FROM atlas/athanalysis:21.2.151
COPY . /analysis
WORKDIR /analysis/build
RUN source ~/release_setup.sh &&  \
    sudo chown -R atlas /analysis && \
    cmake -S ../source -B . && \
    cmake --build . --clean-first --parallel $(($(nproc) - 1))
