import sys
from PyUtils import AthFile

#______________________________________________________________________________
def cleanupLogFile():
   ''' 
   Remove athfile-* files after readnig metadata
   '''
   import os
   from glob import glob
   logfilename = 'athfile-%i-*.log.txt' % (os.getpid())
   for fname in glob(logfilename): os.remove(fname)

#______________________________________________________________________________
def getFileType(af):
   '''
   Get the type of files, can be one of the following:
   data, truth3, truth, fullsum, atlfastII, AOD, or unknown. 

   Args:
         af: an open athena file 
   Returns:
         A string with the file type
   '''

   infos = af.fileinfos
   if 'IS_DATA' in infos['evt_type']: return "data"
   elif 'IS_SIMULATION' in infos['evt_type']:
      af_deflist = ['AtlfastII','ATLFASTII']
      # TRUTH3 is a bit of a wildcard - doesn't have a lot of stuff we'd expect.
      if 'StreamDAOD_TRUTH3' in infos['stream_names']: return "truth3"
      # more generally in pure truth there presumable won't have been an ESDtoAOD step
      if 'AtlasRelease_ESDtoAOD' not in infos['tag_info']: return "truth"
      if 'default' in infos['metadata']['/Simulation/Parameters']['SimulationFlavour']:      return "fullsim"
      if 'FullG4' in infos['metadata']['/Simulation/Parameters']['SimulationFlavour']:       return "fullsim"
      for afstr in af_deflist:
            if afstr in infos['metadata']['/Simulation/Parameters']['SimulationFlavour'] : return "atlfastII"
      if infos['metadata_items'][0][0] == 'EventStreamInfo' and infos['metadata_items'][0][1] =='StreamAOD': return 'AOD'
      error('Impossible to find a simulation type from this sample')
   else: return "unknown"

#______________________________________________________________________________
def isFileEmpty(inFileName):
   '''
   Check if the athena file is emtpy
   if so, return 0 
   '''
   from PyUtils import AthFile
   print 'isFileEmpty({0} -> {1})'.format(type(inFileName), inFileName)
   
   if type(inFileName)==str:
      af = AthFile.fopen(inFileName)
      return af.nentries == 0
   else:
      return inFileName.nentries == 0



#______________________________________________________________________________
def getRunOrChannelNumber(af, isMC = None): # assume exactly one will be non-zero
   '''
   Returns the MC channel number (if running on MC) or the run number (if running on data)
   '''
   if isMC is None:
      isMC = (getFileType(af) != 'data')
   try:
      runOrChannelNumber = af.mc_channel_number[0] if isMC else af.run_number[0]
   except IndexError:
      raise IOError("Failed to access channel/run information. Did you ensure that the file had entries with the isFileEmpty function?")

   return runOrChannelNumber

#______________________________________________________________________________
def getMCShowerType(metadata, inFileName):
   '''
   Decode what the shower type should be from the file name
   This requires the full filename, and parses that string to decode the shower type
   Args:
    metadata: dictionary "tag_info" from athena metadata  
    inFileName: a string with the full path to the filename
   Returns:
    an int corresponding to the correct shower type 
   '''

   showerType = None 

   # extract generator type from athena metadata
   try:
      generators = metadata['generators']
      if 'Sherpa' in generators:
        showerType = 3
      elif 'Pythia8' in generators:
        showerType = 2
      elif 'Herwigpp' in generators:
        showerType = 1
      else:
        showerType = 0
      print 'Found shower type {0} from generator {1}'.format(showerType, generators) 

   except KeyError:
      print 'ERROR: "generators" key not on metadata dictionary, exiting ...'
      print 'Falling back to filename parsing ...' 

      if 'Sherpa' in inFileName:
        showerType = 3
      elif 'Herwigpp' in inFileName:
        showerType = 1
      elif ('Pythia8' in inFileName) or ('MGPy8' in inFileName):
        showerType = 2 # showertype 0 for pythia6
      else:
        showerType = 0
      print 'Parsed filename, found MC shower type: {0}'.format(showerType)

   return showerType 

#______________________________________________________________________________
def getMCCampaign(amiTag):
   '''
   Decode the MC campaign (mc16a/c/d/e) from the AMI r-tags
   Args:
      amiTag: a string of the file's AMI tags
   returns:
      a string corresponding to the MC campaign
   '''

   if "r9364" in amiTag: 
      mcCampaign = "mc16a"
   elif "r9781" in amiTag: 
      mcCampaign = "mc16c"
   elif "r10201" in amiTag: 
      mcCampaign = "mc16d"
   elif "r10724" in amiTag: 
      mcCampaign = "mc16e"
   else:
      cleanupLogFile()
      print 'ERROR: unable to find mc version in metadata, amiTag: ', amiTag
      sys.exit(1)

   return mcCampaign



#______________________________________________________________________________
def getOneFileInfo(inFileName, isEmptyJob = False):
  '''
  Create a dictionary of relevant metadata from the different sources, including the type of file (data/MC), 
  shower type (if MC), and run/channel number 
  '''
  isData    = 0
  isMC      = 0
  isDAOD    = 1
  isAOD     = 0
  isAF2     = 0
  isFullSim = 0
  isTruth   = 0
  hasTruth  = 1
  runOrChannelNumber = -1
  from PyUtils import AthFile
  af = AthFile.fopen(inFileName)

  # Sort file type
  filetype = getFileType(af)
  if filetype == "data":
    isData = 1
    hasTruth = 0
  elif filetype == "fullsim":
    isMC = 1
    isFullSim = 1
    if 'Truth' not in af.fileinfos['metadata']['/Simulation/Parameters']['SimulatedDetectors']: hasTruth = 0
  elif filetype == "atlfastII":
    isMC = 1
    isAF2 = 1
    if 'Truth' not in af.fileinfos['metadata']['/Simulation/Parameters']['SimulatedDetectors']: hasTruth = 0
  elif filetype in ["truth3", "truth"]:
    isTruth = 1
  elif filetype == 'AOD':
    isMC = 1
    isAOD = 1
    isDAOD = 0
  else:
    error('Unable to determine file type! Abort')
    sys.exit(1)


  # add data type to dict
  retDict = {"isData"    : isData,
          "isMC"      : isMC,
          "isDAOD"    : isDAOD,
          "isAOD"     : isAOD,
          "isAF2"     : isAF2,
          "isFullSim" : isFullSim,
          "isTruth"   : isTruth,
          "hasTruth"  : hasTruth}
  toPrint = 'Read information from files: \n \
          \tisData    : {0}\n \
          \tisMC      : {1}\n \
          \tisAF2     : {2}\n \
          \tisFullSim : {3}\n \
          \tisTruth   : {4}\n \
          \thasTruth  : {5}\n'.format(retDict['isData'], retDict['isMC'], retDict['isAF2'], retDict['isFullSim'], retDict['isTruth'], retDict['hasTruth'])
  print toPrint


  # Extract generator type (important for b-tagging SF)
  if isMC:
     showerType = getMCShowerType(af.fileinfos['tag_info'], inFileName)
  else:
     showerType = 0
  retDict['showerType'] = showerType

  # Extract run/MC channel number
  print 'Extracting the dsid ...'
  runOrChannelNumber = getRunOrChannelNumber(af, isMC) if not isEmptyJob else -1
  retDict['number'] = runOrChannelNumber
  print '\tnumber : {0}'.format(runOrChannelNumber)

  # Ami tag and project name
  amiTag = af.fileinfos['tag_info']['AMITag']
  retDict['amiTag'] = amiTag
  retDict['project_name'] = af.fileinfos['tag_info']['project_name']

  # Match r-tag to mc campaign 
  if isMC:
    mcCampaign = getMCCampaign(amiTag) 
    retDict['mcCampaign'] = mcCampaign
    print '\tmcCampaign : {0}'.format(mcCampaign)
  else:
    retDict['mcCampaign'] = 'data' 

  cleanupLogFile()
  return retDict


#______________________________________________________________________________
def getFileInfos(infilelist):
  isData    = 0
  isMC      = 0
  isAF2     = 0
  isFullSim = 0
  isTruth   = 0
  hasTruth  = 1
  runOrChannelNumber = -1
  nEntries = 0
  for ifile in infilelist:
    af = AthFile.fopen(ifile)
    filetype = getFileType(af)
    if filetype == "data":
      isData = 1
      hasTruth = 0
    elif filetype == "fullsim":
      isMC = 1
      isFullSim = 1
      if 'Truth' not in af.fileinfos['metadata']['/Simulation/Parameters']['SimulatedDetectors']: hasTruth = 0
    elif filetype == "atlfastII":
      isMC = 1
      isAF2 = 1
      if 'Truth' not in af.fileinfos['metadata']['/Simulation/Parameters']['SimulatedDetectors']: hasTruth = 0
    elif filetype in ["truth3", "truth"]:
      isTruth = 1
    else:
      error('Unable to determine file type! Abort')
      sys.exit(1)
    number = getRunOrChannelNumber(af, isMC)
    if runOrChannelNumber > 0 and runOrChannelNumber != number:
      error('Mix of run/channel numbers detected: {0} and {1}. Can only run one in each job. Abort'.format(number, runOrChannelNumber) )
      sys.exit(1)
    nEntries += af.nentries
    runOrChannelNumber = number

  if isData + isMC + isTruth> 1:
    error('Mixture of data and mc detected - cannot run more than 1 type. Abort')
    sys.exit(1)
  if isFullSim + isAF2 != isMC:
    error('Mixutre of AFII and fullSim detected - cannot run more than 1 type. Abort')
    sys.exit(1)


  retDict = {"isData"    : isData,
             "isMC"      : isMC,
             "isAF2"     : isAF2,
             "isFullSim" : isFullSim,
             "isTruth"   : isTruth,
             "hasTruth"  : hasTruth,
             "nEntries"  : nEntries}
  print 'Read information from files: \n' \
      '\tisData    : {0}\n' \
      '\tisMC      : {1}\n' \
      '\tisAF2     : {2}\n' \
      '\tisFullSim : {3}\n' \
      '\tisTruth   : {4}\n' \
      '\thasTruth  : {5}\n' \
      '\tnEntries  : {6}'.format(*retDict.items() )
  retDict['number'] = runOrChannelNumber
  print '\tnumber : {0}'.format(runOrChannelNumber)

  if isMC:
    amiTag = af.fileinfos['tag_info']['AMITag']
    mcCampaign = getMCCampaign(amiTag) 
    retDict['mcCampaign'] = mcCampaign
  else:
    retDict['mcCampaign'] = 'data' 
  print '\tmcCampaign : {0}'.format(mcCampaign)

  cleanupLogFile()
  return retDict
