#Skeleton joboption for a simple analysis job

#---- Minimal job options -----

#---- Options you could specify on command line -----
#jps.AthenaCommonFlags.FilesInput = ["/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/CommonInputs/DAOD_PHYSVAL/mc16_13TeV.410501.PowhegPythia8EvtGen_A14_ttbar_hdamp258p75_nonallhad.DAOD_PHYSVAL.e5458_s3126_r9364_r9315_AthDerivation-21.2.1.0.root"]        #set on command-line with: --filesInput=...

print '>>> Starting run of JobOptions! <<<'

try:
   import platform 
   print 'Operating system: {0}'.format(platform.platform())
except:
   print 'Operating system: unable to import platform module'

OUTPUT_STREAM_NAME = "NTUP"

# Parse command-line options
outputName = vars().get("outputFileName", "myfile.root")
if outputName[-5:] != '.root': outputName += '.root'
print "Will save the output to file: ", outputName


doSyst = vars().get("doSyst", 0)
print 'Extracted doSyst=', doSyst
doSyst = bool(doSyst) # default to False
print 'doSyst: {0}'.format(doSyst)
#requestedSystList = vars().get("requestedSystList", "systList_chargeID")
requestedSystList = vars().get("requestedSystList", "systListDataRatio")
#requestedSystList = systListDataRatio
print requestedSystList

if doSyst:
   print 'WARNING: will run systematics!!!!!!!!'
else:
   print 'Note: will not run systematics'

if doSyst:
   print 'You have selected the "{0}" input systematic list'.format(requestedSystList)


# Option: Turn on/off the single lepton triggers
single_lepton_triggers = False
add_ID_and_MS_muons = True
print 'Will add single lepton triggers: {0}'.format(single_lepton_triggers)
print 'Will write ID and MS muons: {0}'.format(add_ID_and_MS_muons)

options=locals()
options.setdefault('EvtMax', -1)

# Number of evenss to process (-1 for all events) 
theApp.EvtMax = EvtMax 

# WJF: move this after FilesInput (may be triggering read files ... causing issues on the grid)  
jps.AthenaCommonFlags.AccessMode = "ClassAccess"              #Choose from TreeAccess,BranchAccess,ClassAccess,AthenaAccess,POOLAccess
#jps.AthenaCommonFlags.TreeName = "MyTree"                    #when using TreeAccess, must specify the input tree name

# register output files like this. MYSTREAM is used in the code
#jps.AthenaCommonFlags.HistOutputs = ["ANALYSIS:myfile_data18.root"]  


# Hack to make metadata extraction "work" for grid submission, can be any file  
# The metadata of this file will be read first, and then the metadata of the "correct" file will be read on the grid 
if svcMgr.EventSelector.InputCollections == []:
   #svcMgr.EventSelector.InputCollections = ['/r03/atlas/emus/dxaod/data/p3704/data15_13TeV.00280673.physics_Main.deriv.DAOD_SUSY2.r9264_p3083_p3704/DAOD_SUSY2.16126988._000012.pool.root.1']
   svcMgr.EventSelector.InputCollections = ['/r04/atlas/emus/dxaod/data/p4190/data18_13TeV.00360129.physics_Main.deriv.DAOD_SUSY2.f969_m2020_p4190/DAOD_SUSY2.21823121._000001.pool.root.1']



# print some info passed to the job 
print 'Command-line arguments:'
input_file_list = jps.AthenaCommonFlags.FilesInput
print 'jps.AthenaCommonFlags.FilesInput: ', input_file_list
print 'svcMgr.EventSelector.InputCollections: ', svcMgr.EventSelector.InputCollections # should be overwritten with pathena
print 'theApp.EvtMax:' , theApp.EvtMax
print 'jps.AthenaCommonFlags.EvtMax(): ', jps.AthenaCommonFlags.EvtMax()
print 'jps.AthenaCommonFlags.HistOutputs:', jps.AthenaCommonFlags.HistOutputs




# Print statistics about which branches the analysis code uses (optional)
svcMgr.EventSelector.PrintPerfStats = False

# Message level
msgLevel = INFO
#msgLevel = DEBUG


MessageSvc.Format = "% F%30W%S%7W%R%T %0W%M" # Incantation for printout column format 
#MessageSvc.infoLimit = 99999999999

'''
svcMgr.AuditorSvc += CfgMgr.ChronoAuditor() #monitor calls to functions in components
theApp.AuditAlgorithms = True #enable auditors for algorithms
'''


# Extract the git revision SHA for metadata 
git_revision_sha = 'not_in_repo'
'''
Remove for panda 
try:
   import git as gitpython
   repo = gitpython.Repo(search_parent_directories=True)
   git_revision_sha = repo.head.object.hexsha
except ImportError:
   import subprocess
   try: 
      git_revision_sha = subprocess.check_output(['git', 'rev-parse', 'HEAD'])
      git_revision_sha = git_revision_sha[:-1] # remove trailing '\n'
   except:
      git_revision_sha = 'not_in_repo'
except:
   git_revision_sha = 'not_in_repo'
print("Git revision sha:", git_revision_sha)
'''


############################
# Extract file metadata
############################
from ntupleGenerator.GetFileInfo import getOneFileInfo, cleanupLogFile, isFileEmpty

# Loop over files until we find one that isn't empty
fileIndex = 0
emptyJob = False
while isFileEmpty(svcMgr.EventSelector.InputCollections[fileIndex]):
  fileIndex += 1
  if fileIndex == len(svcMgr.EventSelector.InputCollections):
    print ("All supplied files are empty! Will output empty files to make JEDI happy")
    emptyJob = True
    break

# Get the relevant file info 
fileInfoDict = getOneFileInfo(svcMgr.EventSelector.InputCollections[fileIndex], isEmptyJob = emptyJob)
isData       = fileInfoDict["isData"]
isMC         = fileInfoDict["isMC"]
isTruth      = fileInfoDict["isTruth"]
isAF2        = fileInfoDict["isAF2"]
isFullSim    = fileInfoDict["isFullSim"]
mcCampaign   = fileInfoDict["mcCampaign"] # set to "data" if isData, otherwise mc16a/d/e  
hasTruth     = fileInfoDict["hasTruth"]
number       = int(fileInfoDict["number"])
showerType   = fileInfoDict["showerType"]
amiTag       = fileInfoDict["amiTag"]
project_name = fileInfoDict['project_name'] # data17_13TeV etc, or IS_SIMULATION for MC

# Sort out data year 
YEAR  = 0
if isData:
   for year in ['15', '16', '17', '18']:
      if year in project_name:
         YEAR = int(year)
         break
   print 'Found year {0} from {1}'.format(YEAR, project_name)





# enum DataType {Data = 0, Full = 1, FastShower = 2, Fast = 3, True = 4} ;
DataType = 0
if isData: DataType = 0
elif isAF2: DataType = 3
else: DataType = 1
print 'Setting DataType to', DataType 


#############################
# Define list of systematics to be run over 
#############################

#if (not doSyst) or isData:
if (not doSyst):
   systListToProcess = ["Nominal"]
else:
#   systListToProcess = [
#   "Nominal",
#  # Electrons
#   "EG_RESOLUTION_ALL__1down",
#   "EG_RESOLUTION_ALL__1up",
#   "EG_SCALE_ALL__1down",
#   "EG_SCALE_ALL__1up",
#   "EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1down",
#   "EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1up",
#   "EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down",
#   "EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up",
#   "EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down",
#   "EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up",
#   "EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down",
#   "EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up",
#   "EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down",
#   "EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up",
#   "EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down",
#   "EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up",
#   'EL_CHARGEID_STAT__1down', 
#   'EL_CHARGEID_STAT__1up', 
#   'EL_CHARGEID_SYStotal__1down', 
#   'EL_CHARGEID_SYStotal__1up', 
#  # 7 JES NPs
#   "JET_EtaIntercalibration_NonClosure_highE__1up",
#   "JET_EtaIntercalibration_NonClosure_highE__1down",
#   "JET_EtaIntercalibration_NonClosure_negEta__1up",
#   "JET_EtaIntercalibration_NonClosure_negEta__1down",
#   "JET_EtaIntercalibration_NonClosure_posEta__1up",
#   "JET_EtaIntercalibration_NonClosure_posEta__1down",
#   "JET_GroupedNP_1__1up",
#   "JET_GroupedNP_1__1down",
#   "JET_GroupedNP_2__1up",
#   "JET_GroupedNP_2__1down",
#   "JET_GroupedNP_3__1up",
#   "JET_GroupedNP_3__1down",
#   "JET_Flavor_Response__1up", # doesn't exist in Scenario2 or 3
#   "JET_Flavor_Response__1down", # doesn't exist in Scenario2 or 3
#  # 8 JER NPs
#   "JET_JER_EffectiveNP_1__1up",
#   "JET_JER_EffectiveNP_2__1up",
#   "JET_JER_EffectiveNP_3__1up",
#   "JET_JER_EffectiveNP_4__1up",
#   "JET_JER_EffectiveNP_5__1up",
#   "JET_JER_EffectiveNP_6__1up",
#   "JET_JER_EffectiveNP_7restTerm__1up",
##   "JET_JER_DataVsMC_MC16__1up",
#   "JET_JvtEfficiency__1down",
#   "JET_JvtEfficiency__1up",
#  # MET
#   "MET_SoftTrk_ResoPara",
#   "MET_SoftTrk_ResoPerp",
#   "MET_SoftTrk_Scale__1down",
#   "MET_SoftTrk_Scale__1up",
#  # Muons
#   "MUON_EFF_BADMUON_SYS__1down",
#   "MUON_EFF_BADMUON_SYS__1up",
#   "MUON_EFF_ISO_STAT__1down",
#   "MUON_EFF_ISO_STAT__1up",
#   "MUON_EFF_ISO_SYS__1down",
#   "MUON_EFF_ISO_SYS__1up",
#   "MUON_EFF_RECO_STAT__1down",
#   "MUON_EFF_RECO_STAT__1up",
#   "MUON_EFF_RECO_SYS__1down",
#   "MUON_EFF_RECO_SYS__1up",
#   "MUON_EFF_TTVA_STAT__1down",
#   "MUON_EFF_TTVA_STAT__1up",
#   "MUON_EFF_TTVA_SYS__1down",
#   "MUON_EFF_TTVA_SYS__1up",
#   "MUON_EFF_TrigStatUncertainty__1down",
#   "MUON_EFF_TrigStatUncertainty__1up",
#   "MUON_EFF_TrigSystUncertainty__1down",
#   "MUON_EFF_TrigSystUncertainty__1up",
#   "MUON_ID__1down",
#   "MUON_ID__1up",
#   "MUON_MS__1down",
#   "MUON_MS__1up",
#   "MUON_SCALE__1down",
#   "MUON_SCALE__1up",
#   'MUON_EFF_RECO_STAT_LOWPT__1down', 
#   'MUON_EFF_RECO_STAT_LOWPT__1up', 
#   'MUON_EFF_RECO_SYS_LOWPT__1down', 
#   'MUON_EFF_RECO_SYS_LOWPT__1up', 
#   "PRW_DATASF__1up",
#   "PRW_DATASF__1down",
#  ## these don't exist for ntuple stage: 
#   ]
#  systListDataRatio
   systListToProcess = [
     "Nominal",
    # Electrons
     "EG_RESOLUTION_ALL__1down",
     "EG_RESOLUTION_ALL__1up",
     "EG_SCALE_ALL__1down",
     "EG_SCALE_ALL__1up",
    # 7 JES NPs
     "JET_EtaIntercalibration_NonClosure_highE__1up",
     "JET_EtaIntercalibration_NonClosure_highE__1down",
     "JET_EtaIntercalibration_NonClosure_negEta__1up",
     "JET_EtaIntercalibration_NonClosure_negEta__1down",
     "JET_EtaIntercalibration_NonClosure_posEta__1up",
     "JET_EtaIntercalibration_NonClosure_posEta__1down",
     "JET_GroupedNP_1__1up",
     "JET_GroupedNP_1__1down",
     "JET_GroupedNP_2__1up",
     "JET_GroupedNP_2__1down",
     "JET_GroupedNP_3__1up",
     "JET_GroupedNP_3__1down",
     "JET_Flavor_Response__1up", # doesn't exist in Scenario2 or 3
     "JET_Flavor_Response__1down", # doesn't exist in Scenario2 or 3
    # 8 JER NPs
     "JET_JER_EffectiveNP_1__1up",
     "JET_JER_EffectiveNP_2__1up",
     "JET_JER_EffectiveNP_3__1up",
     "JET_JER_EffectiveNP_4__1up",
     "JET_JER_EffectiveNP_5__1up",
     "JET_JER_EffectiveNP_6__1up",
     "JET_JER_EffectiveNP_7restTerm__1up",
    # "JET_JER_DataVsMC_MC16__1up"
    # MET
     "MET_SoftTrk_ResoPara",
     "MET_SoftTrk_ResoPerp",
     "MET_SoftTrk_Scale__1down",
     "MET_SoftTrk_Scale__1up",
    # Muons
     "MUON_ID__1down",
     "MUON_ID__1up",
     "MUON_MS__1down",
     "MUON_MS__1up",
     "MUON_SAGITTA_RESBIAS__1down",
     "MUON_SAGITTA_RESBIAS__1up",
     "MUON_SAGITTA_RHO__1down",
     "MUON_SAGITTA_RHO__1up",
     "MUON_SCALE__1down",
     "MUON_SCALE__1up"
    ]
#   syst_dict = SystList.__dict__
   try:
      systListToProcess = systListToProcess#syst_dict[requestedSystList]
   except KeyError:
      print 'FATAL: Input systematic list is not in common/SystList.py'
      sys.exit()
   # Make sure "Nominal" is the first element of the systematic list
   if "Nominal" in systListToProcess:
      if systListToProcess[0] != "Nominal":
         print 'ERROR: it seems that nominal is inside the systList, but it\'s not the first element of the list, and it needs to be the first element of the list!'
         sys.exit()
   else:
      systListToProcess = ["Nominal"] + systListToProcess

#systList = [ 'Nominal', 'EG_RESOLUTION_ALL__1up', 'MUON_SCALE__1up']

from pprint import pprint
print 'List of systematics to consider:'
pprint(systListToProcess)

#############################
# Define Tools
#############################

# xAOD config tool
ToolSvc += CfgMgr.TrigConf__xAODConfigTool( "xAODConfigTool",
      OutputLevel = msgLevel)


# CutFlowTool
ToolSvc += CfgMgr.CutFlowTool("CutFlowTool",
      isMC = isMC, 
      ListOfSystematics = systListToProcess,
      RootStreamName = OUTPUT_STREAM_NAME,
      OutputLevel = msgLevel
      )



# GRL 
# List of up-to-date GRLs can be found on BG forum https://twiki.cern.ch/twiki/bin/view/AtlasProtected/BackgroundStudies
# note GRLs can be found: /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/
# recommendation on GRLs: https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/GoodRunListsForAnalysisRun2
if isData:
   if YEAR==15:
      GoodRunsListVec = ['GoodRunsLists/data15_13TeV/20170619/data15_13TeV.periodAllYear_DetStatus-v89-pro21-02_Unknown_PHYS_StandardGRL_All_Good_25ns.xml']
   elif YEAR==16: 
      GoodRunsListVec = ['GoodRunsLists/data16_13TeV/20180129/data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml']
   elif YEAR==17:
      GoodRunsListVec = ['GoodRunsLists/data17_13TeV/20180619/data17_13TeV.periodAllYear_DetStatus-v99-pro22-01_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml']
   elif YEAR==18:
      #GoodRunsListVec = ['GoodRunsLists/data18_13TeV/20181111/data18_13TeV.periodAllYear_DetStatus-v102-pro22-04_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml']
      GoodRunsListVec = ['GoodRunsLists/data18_13TeV/20190219/data18_13TeV.periodAllYear_DetStatus-v102-pro22-04_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml']
   else:
      print('ERROR, project name set incorrectly: ', project_name, 'unable to set correct GRL')
      sys.exit()
elif isMC:
   # Doesn't matter what we give it for MC, all events pass the GRL -- I THINK (WJF)  
   # Just need to give the tool something otherwise it'll complain!  
   GoodRunsListVec = ['GoodRunsLists/data17_13TeV/20171130/data17_13TeV.periodAllYear_DetStatus-v97-pro21-13_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml']

print 'GRL setting. Year {0} and GRL runs list vector: {1}'.format(YEAR, GoodRunsListVec)



# super cool way of adding lumi metadata to flat ntuples:
# https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AthAnalysis#Copying_luminosity_metadata_to_f
# Will add a metadata TTree (in xAOD format)
if isData:
   print ">>> JOs: Register LumiBlockMetaDataTool"
   svcMgr.MetaDataSvc.MetaDataTools += [ "LumiBlockMetaDataTool" ]
   theApp.CreateSvc += ["xAOD2NtupLumiSvc"]

electron_isolation_WPs = [
   'FCHighPtCaloOnly', 
   'Gradient',
   'FCLoose', 
   'FCTight', 
   #'FCLoose_FixedRad', # not currently supported for electrons 
   #'FCTight_FixedRad', # not currently supported for electrons
    ]

muon_isolation_WPs = [
   #'FixedCutHighPtTrackOnly',
   'HighPtTrackOnly',
   'FCTightTrackOnly',
   'FCLoose',
   'FCTight',
   #'FCLoose_FixedRad',
   #'FCTight_FixedRad',
   'Tight_VarRad',
   ]


   
'''
# configured in c++ because the isolation tool is stupid 
## isolation tool
print ">>> JOs: Register IsolationSelectionTool"
# PhysicsAnalysis/AnalysisCommon/IsolationSelection/
ToolSvc += CfgMgr.CP__IsolationSelectionTool("ManualIsolationSelectionTool",
   #ElectronWP = "Gradient", 
   #MuonWP     = "FCLoose",
   #MuonWP     = "FCTight", # testing 
   #PhotonWP   = "FixedCutTight",
   OutputLevel = VERBOSE
   )
'''


'''
# Already fixed inside SUSYTools ! 
print ">>> JOs: Register MuonSelectionTool"
if not hasattr(ToolSvc,"MuonSelectionTool"):        
   from MuonSelectorTools.MuonSelectorToolsConf import CP__MuonSelectionTool
   ToolSvc += CfgMgr.CP__MuonSelectionTool("MyManualMuonSelectionTool",
         MaxEta = 2.5,
         MuQuality = 4 # high pT working point
         )
'''

# GRL tool
print ">>> JOs: Register GRLTool"
ToolSvc += CfgMgr.GoodRunsListSelectionTool( "GRLTool",
      GoodRunsListVec = GoodRunsListVec, 
      PassThrough=False,
      OutputLevel     = msgLevel)

# https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/PmgEventWeights
# PMG weights tool 
if isMC:
   print ">>> JOs: Register PMGTruthWeightTool"
   ToolSvc += CfgMgr.PMGTools__PMGTruthWeightTool( "PMGTruthWeightTool",
         MetaObjectName="TruthMetaData",
         OutputLevel = msgLevel)

# PMG sherpa tool
if isMC: 

   print ">>> JOs: Register PMGSherpaTool"
   ToolSvc += CfgMgr.PMGTools__PMGSherpaVjetsSysTool( "PMGSherpaTool",
         TruthJetContainer="AntiKt4TruthJets",
         OutputLevel = msgLevel)



# Charge flip tagging tool, note that SUSYTools has this built in, but we want to override with our own to be able to remove ChID requirements later if wanted ... 
# see https://twiki.cern.ch/twiki/bin/view/AtlasProtected/ElectronChargeFlipTaggerTool
print ">>> JOs: Register ElectronChargeIDSelectorTool"
ToolSvc += CfgMgr.AsgElectronChargeIDSelectorTool("ECIDS_loose", # The name must contain one of these following strings: recon, loose, medium, tight
      TrainingFile = 'ElectronPhotonSelectorTools/ChargeID/ECIDS_20180731rel21Summer2018.root'
      #CutOnBDT = -0.288961, # Medium # WJF: remote this, the operating point is picked up from the "medium" or "loose" condition in the name
      )


# Electron Charge Efficiency Correction Tool 
# https://twiki.cern.ch/twiki/bin/view/AtlasProtected/EgammaChargeMisIdentificationTool
# May wish to remove this. Currently the same as the SUSYTools implementation (which will extract the SF corresponding to the "signal" electrons). 
print ">>> JOs: Register ElectronChargeEffCorrectionTool"
ToolSvc += CfgMgr.CP__ElectronChargeEfficiencyCorrectionTool("ElectronChargeEffCorrectionTool",
      CorrectionFileName = 'ElectronEfficiencyCorrection/2015_2017/rel21.2/Consolidation_September2018_v1/charge_misID/chargeEfficiencySF.TightLLH_d0z0_v13_Gradient.root',
      #CorrectionFileName = 'ElectronEfficiencyCorrection/2015_2017/rel21.2/Consolidation_September2018_v2/charge_misID/efficiencySF.ChargeID.TightLLH_d0z0_v13_FCTight_ECIDSloose.root', # WJF: update 16/01/2020
      ForceDataType = DataType
      )

# Electron efficiency correction tool 
# This is also included (along with the above tool) as we may want to extract this scale factor separately from the charge ID SF 
# Although we may want to fall back to the SUSYTools implementation, especially when dealing with systematics 
# WJF: will need to be updated 
print ">>> JOs: Register ElectronEfficiencyCorrectionTool"
#elec_eff_sf_tool_chf_input_file = 'ElectronEfficiencyCorrection/2015_2016/rel20.7/Moriond_February2017_v1/charge_misID/efficiencySF.ChargeID.MediumLLH_d0z0_v11_isolGradient_MediumCFT.root'
elec_eff_sf_tool_chf_input_file = 'ElectronEfficiencyCorrection/2015_2017/rel21.2/Consolidation_September2018_v1/additional/efficiencySF.ChargeID.TightLLH_d0z0_v13_Gradient_ECIDSloose.root'
elec_eff_sf_tool_chf = CfgMgr.AsgElectronEfficiencyCorrectionTool("AsgElectronEfficiencyCorrectionTool_chf",
      CorrectionFileNameList = [elec_eff_sf_tool_chf_input_file],
      #CorrectionFileName = elec_eff_sf_tool_chf_input_file,
      CorrelationModel = 'TOTAL',
      #ForceDataType = DataType 
      )
if isMC: elec_eff_sf_tool_chf.ForceDataType = DataType
ToolSvc += elec_eff_sf_tool_chf




# SUSYTools
susytools_config_file = 'ntupleGenerator/SUSYTools_OSDF.conf'

# TODO make sure all of these are up-to-date

prwConfs = ['ntupleGenerator/pileup_'+mcCampaign+'_EMUS_all.root']

if mcCampaign=="mc16a":
   prwLumiCalcs = [
   #'GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root',
   #'GoodRunsLists/data16_13TeV/20170605/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-008.root'
   'GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root',
   'GoodRunsLists/data16_13TeV/20180129/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-009.root',
   ]   
elif mcCampaign=="mc16d":
   prwLumiCalcs = [
   #'GoodRunsLists/data17_13TeV/20171130/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-001.root'
   'GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root'
   ]
elif mcCampaign=="mc16e":
   prwLumiCalcs = [
   #'GoodRunsLists/data18_13TeV/20180924/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-001.root'
   #'GoodRunsLists/data18_13TeV/20181111/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-001.root'
   #'GoodRunsLists/data18_13TeV/20190219/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-010.root'
   'GoodRunsLists/data18_13TeV/20190318/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-010.root'
   ]


for prefix, eleID in [('Looser_', 'LooseAndBLayerLLH'),  ('Tighter_', 'TightLLH') ]:  
   
   print ">>> JOs: Register "+prefix+"SUSYTools"
   ToolSvc += CfgMgr.ST__SUSYObjDef_xAOD( prefix+"SUSYTools",
                                          ConfigFile              = susytools_config_file, # use config file for now, may want to move away from this in the future  
                                          EleId                   = eleID, 
                                          ShowerType              = showerType,
                                          DebugMode               = msgLevel != INFO,
                                          #JESNuisanceParameterSet = confReader["JESNPSet"], # Only needed for MC?
                                          OutputLevel             = msgLevel)

# Extra SUSYtools configuration for MC only (PRW)
if isMC:

   ToolSvc.Tighter_SUSYTools.PRWConfigFiles = prwConfs
   ToolSvc.Tighter_SUSYTools.PRWLumiCalcFiles = prwLumiCalcs
   ToolSvc.Tighter_SUSYTools.PRWActualMu2017File = 'GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root"' #2017 mc16d
   ToolSvc.Tighter_SUSYTools.PRWActualMu2018File = 'GoodRunsLists/data18_13TeV/20190318/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root' #2018 mc16e
   ToolSvc.Tighter_SUSYTools.AutoconfigurePRWTool = True

   ToolSvc.Looser_SUSYTools.PRWConfigFiles = prwConfs
   ToolSvc.Looser_SUSYTools.PRWLumiCalcFiles = prwLumiCalcs
   ToolSvc.Looser_SUSYTools.PRWActualMu2017File = 'GoodRunsLists/data17_13TeV/20180309/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root' #2017 mc16d
   ToolSvc.Looser_SUSYTools.PRWActualMu2018File = 'GoodRunsLists/data18_13TeV/20180924/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-001.root' #2018 mc16e
   ToolSvc.Looser_SUSYTools.AutoconfigurePRWTool = True


# Tile correction tool
print ">>> JOs: Register JetTileCorrectionTool"
tile_correction_tool = CfgMgr.CP__JetTileCorrectionTool("tilecorr")
ToolSvc += tile_correction_tool


# ConfigTreeTool
print ">>> JOs: Register ConfigTreeTool"
nFiles = len(svcMgr.EventSelector.InputCollections)
ToolSvc += CfgMgr.ConfigTreeTool("ConfigTreeTool", 
      doRejectEventsAfterSyst     = True,
      git_revision_sha            = git_revision_sha,
      outputName                  = outputName, 
      isData                      = isData,
      isMC                        = isMC,
      isTruth                     = isTruth,
      isAF2                       = isAF2,
      isFullSim                   = isFullSim,
      mcCampaign                  = mcCampaign,
      hasTruth                    = hasTruth,
      number                      = number,
      showerType                  = showerType,
      doTileCorrection            = True,
      doPhotonMet                 = False,
      doTauMet                    = False,
      nFiles                      = nFiles,
      nEvents                     = theApp.EvtMax,
      useOldTreeNamingConvention  = False,
      writeTriggerBranches        = True,
      susytools_config_file       = susytools_config_file,
      amiTag                      = amiTag,
      single_lepton_triggers      = single_lepton_triggers,
      RootStreamName              = OUTPUT_STREAM_NAME
      )


##############################################################################
# Now set up the algorithms 
##############################################################################

masterSeq = CfgMgr.AthSequencer("AthAlgSeq")
topseq = CfgMgr.AthSequencer("TopSequence")
masterSeq += topseq


# The "master" algorithm 
print ">>> JOs: Register ntupleGeneratorAlg"
myMasterAlg = CfgMgr.ntupleGeneratorAlg("ntupleGeneratorAlg", 
      SUSYTools_Tighter = ToolSvc.Tighter_SUSYTools,
      SUSYTools_Looser  = ToolSvc.Looser_SUSYTools,
      ConfigTreeTool    = ToolSvc.ConfigTreeTool,
      GRLTool           = ToolSvc.GRLTool,
      single_lepton_triggers = single_lepton_triggers, 
      MyStreamName      = OUTPUT_STREAM_NAME, 
      OutputLevel       = msgLevel
      )
topseq += myMasterAlg


'''
# Algs for common objects (not affected by systematics)
print ">>> JOs: Register CommonWriterAlg"
topseq += CfgMgr.CommonWriter("CommonWriterAlg",
      RootStreamName = OUTPUT_STREAM_NAME,
      OutputLevel = msgLevel)
'''


# Algs for objects affected by systematics
for systName in systListToProcess:
   
   systAlgName = systName.replace('__', '_')
   systseq = CfgMgr.AthSequencer(systAlgName + "Sequence")

   # getter
   print ">>> JOs: Register ObjectGetter_"+systAlgName
   object_getter = CfgMgr.ObjectGetter("ObjGet_"+systAlgName,
         SUSYTools_Tighter      = ToolSvc.Tighter_SUSYTools,
         SUSYTools_Looser       = ToolSvc.Looser_SUSYTools,
         ConfigTreeTool         = ToolSvc.ConfigTreeTool,
         TileCorrectionTool     = tile_correction_tool,
         #MuonSelectionTool      = ToolSvc.MyManualMuonSelectionTool, high pT working point already in fixed in SUSYTools
         #IsolationSelectionTool = ToolSvc.ManualIsolationSelectionTool,
         ElectronIsoWPs         = electron_isolation_WPs,
         MuonIsoWPs             = muon_isolation_WPs,
         ChargeFlipTagTool      = ToolSvc.ECIDS_loose,
         isData                 = isData,
         single_lepton_triggers = single_lepton_triggers,
         SystematicName         = systName,
         OutputLevel = msgLevel)
   if isMC: 
      object_getter.PMGTruthWeightTool = ToolSvc.PMGTruthWeightTool 
      
   
   # writer
   print ">>> JOs: Register ObjectWriter_"+systAlgName
   object_writer = CfgMgr.ObjectWriter("ObjWriter_"+systAlgName,
         RootStreamName           = OUTPUT_STREAM_NAME,
         SystematicName           = systName,
         SUSYTools_Tighter        = ToolSvc.Tighter_SUSYTools,
         ConfigTreeTool           = ToolSvc.ConfigTreeTool,
         CutFlowTool              = ToolSvc.CutFlowTool,
         ElectronIsoWPs           = electron_isolation_WPs,
         MuonIsoWPs               = muon_isolation_WPs,
         single_lepton_triggers   = single_lepton_triggers, 
         add_ID_and_MS_muons      = add_ID_and_MS_muons,   
         #ChargeFlipTagTool        = ToolSvc.ECIDS_loose, # not used 
         ChargeFlipTagEffCorrTool = ToolSvc.AsgElectronEfficiencyCorrectionTool_chf,
         ChargeEffCorrTool        = ToolSvc.ElectronChargeEffCorrectionTool,
         #OutputLevel              = DEBUG)
         OutputLevel              = msgLevel)

   systseq += object_getter
   systseq += object_writer

   topseq += systseq



# systCalib = CfgMgr.
# athAlgSeq += CfgMgr.ntupleGeneratorAlg()                               #adds an instance of your alg to the main alg sequence


if not hasattr(svcMgr, 'THistSvc'): 
   svcMgr += CfgMgr.THistSvc() #only add the histogram service if not already present (will be the case in this jobo)
svcMgr.THistSvc.Output += [OUTPUT_STREAM_NAME+" DATAFILE='" + outputName + "' OPT='RECREATE'"] #add an output root file stream

include("AthAnalysisBaseComps/SuppressLogging.py")  #Optional include to suppress as much athena output as possible. Keep at bottom of joboptions so that it doesn't suppress the logging of the things you have configured above
