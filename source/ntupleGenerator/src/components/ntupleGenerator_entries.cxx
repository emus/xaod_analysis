
#include "GaudiKernel/DeclareFactoryEntries.h"

#include "../ntupleGeneratorAlg.h"

DECLARE_ALGORITHM_FACTORY( ntupleGeneratorAlg )


#include "../ObjectGetter.h"
DECLARE_ALGORITHM_FACTORY( ObjectGetter )


#include "../ObjectWriter.h"
DECLARE_ALGORITHM_FACTORY( ObjectWriter )


#include "../CommonWriter.h"
DECLARE_ALGORITHM_FACTORY( CommonWriter )


#include "../CommonGetter.h"
DECLARE_ALGORITHM_FACTORY( CommonGetter )

#include "ntupleGenerator/ConfigTreeTool.h"
DECLARE_TOOL_FACTORY( ConfigTreeTool ) 

#include "ntupleGenerator/CutFlowTool.h"
DECLARE_TOOL_FACTORY( CutFlowTool ) 

DECLARE_FACTORY_ENTRIES( ntupleGenerator ) 
{
  DECLARE_ALGORITHM( CommonGetter );
  DECLARE_ALGORITHM( CommonWriter );
  DECLARE_ALGORITHM( ObjectWriter );
  DECLARE_ALGORITHM( ObjectGetter );
  DECLARE_TOOL( ConfigTreeTool );
  DECLARE_TOOL( CutFlowTool );
  DECLARE_ALGORITHM( ntupleGeneratorAlg );
}
