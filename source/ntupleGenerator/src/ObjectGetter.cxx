// ntupleGenerator includes
#include "ObjectGetter.h"

#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODTau/TauJetContainer.h"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODMissingET/MissingETAuxContainer.h"

#include "xAODTracking/TrackParticlexAODHelpers.h"


template <typename T, typename TAux>
StatusCode ObjectGetter::recordContainer(T* cont, TAux* auxCont, const std::string& nameBase) {
  std::string outName = c_sgPrefix + nameBase + "_" + c_systName;
  ATH_CHECK(evtStore()->record(cont, outName) );
  ATH_CHECK(evtStore()->record(auxCont, outName+"Aux.") );
  return StatusCode::SUCCESS;
}

template <typename T>
StatusCode ObjectGetter::shallowCopyNominal(T*& cont, xAOD::ShallowAuxContainer*& auxCont, const std::string& nameBase) {
  const T* nominal(0);
  ATH_CHECK(evtStore()->retrieve(nominal, c_nominalPrefix+nameBase+c_nominalSuffix) );
  auto shallowCopy = xAOD::shallowCopyContainer(*nominal);
  cont = shallowCopy.first;
  auxCont = shallowCopy.second;
  return StatusCode::SUCCESS;
}

bool greaterPt(const xAOD::IParticle* part1, const xAOD::IParticle* part2) {
  return part1->pt() > part2->pt();
}

inline bool inVector(std::vector<std::string> &vec, const std::string &val){
  return (std::find(vec.begin(), vec.end(), val) != vec.end());
}


ObjectGetter::ObjectGetter( const std::string& name, ISvcLocator* pSvcLocator ) : AthAnalysisAlgorithm( name, pSvcLocator ){

  //declareProperty( "Property", m_nProperty = 0, "My Example Integer Property" ); //example property declaration
  declareProperty("SUSYTools_Tighter", m_SUSYTools_tighter);
  declareProperty("SUSYTools_Looser", m_SUSYTools_looser);
  declareProperty("ConfigTreeTool", m_configTreeTool);
  declareProperty("TileCorrectionTool", m_tileTool);
  declareProperty("PMGTruthWeightTool", m_weightTool);
  //declareProperty("MuonSelectionTool", m_myMuonSelectionTool); 
  //declareProperty("IsolationSelectionTool", m_manualIsolationTool);
  declareProperty("ElectronIsoWPs", m_eleIsoWPs);
  declareProperty("MuonIsoWPs", m_muonIsoWPs);
  declareProperty("isData", m_isData);
  declareProperty("ChargeFlipTagTool", m_electronChargeIDSelectorTool);
  declareProperty("single_lepton_triggers", c_single_lepton_triggers);

  declareProperty( "SystematicName", c_systName = "Nominal", "Systematic variation with which to run" );
  declareProperty( "SGPrefix", c_sgPrefix = "Cal", "StoreGate prefix for the calibrated objects" );
  declareProperty( "NominalContainerPrefix", c_nominalPrefix = "", "StoreGate prefix for the nominal containers" );
  declareProperty( "NominalContainerSuffix", c_nominalSuffix = "", "StoreGate suffix for the nominal containers" );

}


ObjectGetter::~ObjectGetter() {}


StatusCode ObjectGetter::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");
  //
  //This is called once, before the start of the event loop
  //Retrieves of tools you have configured in the joboptions go here
  //

  //HERE IS AN EXAMPLE
  //We will create a histogram and a ttree and register them to the histsvc
  //Remember to configure the histsvc stream in the joboptions
  //
  //m_myHist = new TH1D("myHist","myHist",100,0,100);
  //CHECK( histSvc()->regHist("/MYSTREAM/myHist", m_myHist) ); //registers histogram to output stream
  //m_myTree = new TTree("myTree","myTree");
  //CHECK( histSvc()->regTree("/MYSTREAM/SubDirectory/myTree", m_myTree) ); //registers tree to output stream inside a sub-directory

  std::cout << "*\n*\nObjectGetter::initialize(" << c_systName << ")\n*\n*" << std::endl;

  std::string SUSY_systName = c_systName;
  std::replace(c_systName.begin(), c_systName.end(), ' ', '_');

  // work out what this systematic does
  if(c_systName == "Nominal"){
    m_systInfo.systset = CP::SystematicSet("Nominal");
    m_affectsKinematics = true;
    m_affectsWeights = true;
    m_affectsElectrons = true;
    m_affectsMuons = true;
    m_affectsJets = true;
    m_affectsPhotons = true;
    m_affectsTaus = true;
//    m_affectsBTag = true;
    m_affectsPRW = true;
  }
  else{
    const auto systInfoList = m_SUSYTools_tighter->getSystInfoList();
    auto systItr = std::find_if(systInfoList.begin(), systInfoList.end(), [SUSY_systName] (const ST::SystInfo& systInfo) -> bool { return systInfo.systset.name() == SUSY_systName; } );
    if (systItr == systInfoList.end() ) {
      ATH_MSG_ERROR( "Unable to find systematic: " << SUSY_systName << "!" );
      return StatusCode::FAILURE;
    }
    m_systInfo = *systItr;
    m_affectsKinematics = m_systInfo.affectsKinematics;
    m_affectsWeights = m_systInfo.affectsWeights;
    m_affectsElectrons = ST::testAffectsObject(xAOD::Type::Electron, m_systInfo.affectsType);
    m_affectsMuons = ST::testAffectsObject(xAOD::Type::Muon, m_systInfo.affectsType);
    m_affectsJets = ST::testAffectsObject(xAOD::Type::Jet, m_systInfo.affectsType);
    m_affectsPhotons = ST::testAffectsObject(xAOD::Type::Photon, m_systInfo.affectsType);
    m_affectsTaus = ST::testAffectsObject(xAOD::Type::Tau, m_systInfo.affectsType);
//    m_affectsBTag = ST::testAffectsObject(xAOD::Type::BTag, m_systInfo.affectsType);
    if (c_systName.find("PRW") != std::string::npos ) m_affectsPRW = true;
    else m_affectsPRW = false;
  }

  // info on what this  systematic affexts
  ATH_MSG_INFO("Systematic " << c_systName << " affects the following:");
  ATH_MSG_INFO("m_affectsKinematics : " << m_affectsKinematics  );
  ATH_MSG_INFO("m_affectsWeights : " << m_affectsWeights  );
  ATH_MSG_INFO("m_affectsElectrons : " << m_affectsElectrons  );
  ATH_MSG_INFO("m_affectsMuons : " << m_affectsMuons  );
  ATH_MSG_INFO("m_affectsJets : " << m_affectsJets  );
  ATH_MSG_INFO("m_affectsPhotons : " << m_affectsPhotons  );
  ATH_MSG_INFO("m_affectsTaus : " << m_affectsTaus  );
//  ATH_MSG_INFO("m_affectsBTag : " << m_affectsBTag  );
  ATH_MSG_INFO("m_affectsPRW : " << m_affectsPRW  );


  if (c_nominalPrefix.empty() ) {
    c_nominalPrefix = c_sgPrefix;
  }
  if (c_nominalSuffix.empty() ) {
    c_nominalSuffix = "_Nominal";
  }

  // Tile Correction Tool
  c_doTileCorrection = m_configTreeTool->getDoTileCorrection();
  if(c_doTileCorrection) ATH_CHECK( m_tileTool.retrieve() );

  // PMG weight tool
  if(!m_isData){
    ATH_CHECK(m_weightTool.retrieve());
  }


  // Configuration from SUSYTools  
  c_elePt    = m_configTreeTool->getConfElePt();
  c_eleEta   = m_configTreeTool->getConfEleEta();
  c_eled0sig = m_configTreeTool->getConfEled0sig();
  c_elez0    = m_configTreeTool->getConfElez0();
  c_muPt     = m_configTreeTool->getConfMuPt();
  c_muEta    = m_configTreeTool->getConfMuEta();
  c_mud0sig  = m_configTreeTool->getConfMud0sig();
  c_muz0     = m_configTreeTool->getConfMuz0();
  ATH_MSG_INFO("Retrieved config from SUSYTools:" 
              << " elePt: " << c_elePt 
              << " eleEta: " << c_eleEta 
              << " eled0sig: " << c_eled0sig 
              << " elez0: " << c_elez0 
              << " muPt: " << c_muPt 
              << " muEta: " << c_muEta 
              << " mud0sig: " << c_mud0sig 
              << " muz0: " << c_muz0 
      );

  ////////////////////////////
  // Initialise Isolation Tool
  ////////////////////////////
  ATH_MSG_INFO( "About to retrieve IsolationTool ... ");
  m_manualIsolationTool.setTypeAndName("CP::IsolationSelectionTool/ManualIsoTool");
  ATH_CHECK( m_manualIsolationTool.retrieve() );
  isoTool = dynamic_cast<CP::IsolationSelectionTool*>(m_manualIsolationTool.get() );

  // muon isolation working points
  for(const std::string muWP : m_muonIsoWPs){
    ATH_MSG_INFO("Adding muon isolation WP: " << muWP);
    ATH_CHECK( isoTool->addMuonWP(muWP) );
  }
    

  // Electron isolation working points. 
  for(const auto eleWP : m_eleIsoWPs){
    ATH_MSG_INFO("Adding election isolation WP: " << eleWP);
    ATH_CHECK( isoTool->addElectronWP(eleWP) );
  }

  ATH_CHECK( isoTool->initialize() );

  // Set bools for isolation WPs 
  m_hasEleIsoGradient = inVector( m_eleIsoWPs, "Gradient" );
  m_hasEleIsoFCHighPtCaloOnly = inVector( m_eleIsoWPs, "FCHighPtCaloOnly" );
  m_hasEleIsoFCLoose = inVector( m_eleIsoWPs, "FCLoose" );
  m_hasEleIsoFCTight = inVector( m_eleIsoWPs, "FCTight" );
  m_hasEleIsoFCLoose_FixedRad = inVector( m_eleIsoWPs, "FCLoose_FixedRad" );
  m_hasEleIsoFCTight_FixedRad = inVector( m_eleIsoWPs, "FCTight_FixedRad" );

  m_hasMuonIsoHighPtTrackOnly = inVector( m_muonIsoWPs, "HighPtTrackOnly" );
  m_hasMuonIsoFCTightTrackOnly = inVector( m_muonIsoWPs, "FCTightTrackOnly" );
  m_hasMuonIsoFCLoose = inVector( m_muonIsoWPs, "FCLoose" );
  m_hasMuonIsoFCTight = inVector( m_muonIsoWPs, "FCTight" );
  m_hasMuonIsoFCLoose_FixedRad = inVector( m_muonIsoWPs, "FCLoose_FixedRad" );
  m_hasMuonIsoFCTight_FixedRad = inVector( m_muonIsoWPs, "FCTight_FixedRad" );
  m_hasMuonIsoTight_VarRad = inVector( m_muonIsoWPs, "Tight_VarRad" );


  ////////////////////////////
  // Electron ChargeID tool
  ////////////////////////////

  ATH_CHECK( m_electronChargeIDSelectorTool.retrieve() );

  ////////////////////////////
  // Muon selection tool
  ////////////////////////////
  
  // Was going to be used for high pT working point, but this is already implemented in SUSYTools
  
  //ATH_MSG_INFO("Attempting to retrieve MuonSelectionTool: " );
  //ATH_CHECK( m_myMuonSelectionTool.retrieve() );
  //ATH_MSG_INFO("Retrieved MuonSelectionTool [" << m_myMuonSelectionTool.name() << "]");
  
  
  ////////////////////////////
  // Triggers 
  ////////////////////////////

  ATH_MSG_INFO("Triggers recieved by this algorithm ... ");
  m_electron_triggers = m_configTreeTool->get_dielectron_triggers();
  m_muon_triggers = m_configTreeTool->get_dimuon_triggers();
  m_emu_triggers = m_configTreeTool->get_emu_triggers();


  ATH_MSG_INFO("Electron triggers: ");
  for(auto &t : m_electron_triggers){
    std::cout << "year : " << t.first << " -> " << t.second << std::endl;
  }
  ATH_MSG_INFO("Muon triggers: ");
  for(auto &t : m_muon_triggers){
    std::cout << "year : " << t.first << " -> " << t.second << std::endl;
  }
  ATH_MSG_INFO("Emu triggers: " );
  for(auto &t : m_emu_triggers){
    std::cout << "year : " << t.first << " -> " << t.second << std::endl;
  }

  // single lepton triggers
  if(c_single_lepton_triggers){
    m_singleelectron_triggers = m_configTreeTool->get_singleelectron_triggers();
    m_singlemuon_triggers = m_configTreeTool->get_singlemuon_triggers();
    ATH_MSG_INFO("Single Electron triggers: ");
    for(auto &t : m_singleelectron_triggers){
      std::cout << "year : " << t.first << " -> " << t.second << std::endl;
    }
    ATH_MSG_INFO("Single Muon triggers: ");
    for(auto &t : m_singlemuon_triggers){
      std::cout << "year : " << t.first << " -> " << t.second << std::endl;
    }
  }

  
  ////////////////////////////
  // IFFTruthClassification 
  ////////////////////////////

  m_TruthClassifier = new IFFTruthClassifier(Form("TruthClassifier")); //_%s",name.c_str()));
  if(m_TruthClassifier->initialize() != StatusCode::SUCCESS){
    ATH_MSG_ERROR("IFFTruthClassifier cannot be initialized"); throw std::runtime_error("Unable to execute initializeTruthClassifier()");
  }
  ATH_MSG_INFO("Initialized IFFTruthClassifier: " << m_TruthClassifier->name());


  return StatusCode::SUCCESS;
}

StatusCode ObjectGetter::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");
  //
  //Things that happen once at the end of the event loop go here
  //



  return StatusCode::SUCCESS;
}

StatusCode ObjectGetter::execute() {  
  ATH_MSG_DEBUG ("Executing " << name() << "...");

  //-----------------------------------------
  // Decarators and accessors  
  //-----------------------------------------

  static SG::AuxElement::Decorator<char> dec_pass_cft("passCFT");
  static SG::AuxElement::Decorator<char> dec_tighterNearlySignal("tighterNearlySignal"); // Leptons satisfying the Tighter selection but without isolation requirements
  static SG::AuxElement::Decorator<char> dec_inclusiveLooserNearlySignal("inclusiveLooserNearlySignal");  // Leptons satisfying the Looser selection but without isolation requirements.
  static SG::AuxElement::Decorator<char> dec_trig_matched("triggerMatched"); // leptons satisfying trigger matching 
  //static SG::AuxElement::Decorator<char> dec_nearlySignal("nearlySignal"); // leptons satisfying trigger matching 

  // object scale factors
  static SG::AuxElement::Decorator<float> dec_tighter_SF("tighter_SF"); 
  static SG::AuxElement::Decorator<float> dec_looser_SF("looser_SF");
  static SG::AuxElement::Decorator<float> dec_iso_SF("iso_SF");
  static SG::AuxElement::Decorator<float> dec_joint_IDSF_CFTSF("joint_IDSF_CFTSF");

  // Other event quantities
  static SG::AuxElement::Decorator<float> dec_metsignificance("metsignificance");
  static SG::AuxElement::Decorator<double> dec_nominalEventWeight("nominalEventWeight");
  static SG::AuxElement::Decorator<std::vector<float>> dec_ScaleWeights("ScaleWeights");

  // isolation WPs
  static SG::AuxElement::Decorator<char> dec_HighPtTrackOnly("HighPtTrackOnly");
  static SG::AuxElement::Decorator<char> dec_FCTightTrackOnly("FCTightTrackOnly");
  static SG::AuxElement::Decorator<char> dec_FCLoose("FCLoose");
  static SG::AuxElement::Decorator<char> dec_FCTight("FCTight");
  static SG::AuxElement::Decorator<char> dec_FCLoose_FixedRad("FCLoose_FixedRad");
  static SG::AuxElement::Decorator<char> dec_FCTight_FixedRad("FCTight_FixedRad");
  static SG::AuxElement::Decorator<char> dec_Tight_VarRad("Tight_VarRad");
  static SG::AuxElement::Decorator<char> dec_FCHighPtCaloOnly("FCHighPtCaloOnly");
  static SG::AuxElement::Decorator<char> dec_Gradient("myIso_Gradient");

  // muons
  static SG::AuxElement::Decorator<char> dec_passHighPtCuts("passHighPtCuts");

  // trigger matching (round 2)
  static SG::AuxElement::Decorator<char> dec_diElectronTrigMatch("diElectronTrigMatch");
  static SG::AuxElement::Decorator<char> dec_diMuonTrigMatch("diMuonTrigMatch");
  static SG::AuxElement::Decorator<char> dec_diEMuTrigMatch("diEMuTrigMatch");
  // single lepton triggers
  static SG::AuxElement::Decorator<char> dec_singleElectronTrigMatch("singleElectronTrigMatch");
  static SG::AuxElement::Decorator<char> dec_singleMuonTrigMatch("singleMuonTrigMatch");

  // trigger SF 
  static SG::AuxElement::Decorator<float> dec_triggerGlobalEfficiencySF("triggerGlobalEfficiencySF");
  static SG::AuxElement::Decorator<float> dec_triggerGlobalEfficiency("triggerGlobalEfficiency");
  static SG::AuxElement::Decorator<float> dec_triggerElectronEfficiencySF("triggerElectronEfficiencySF");
  static SG::AuxElement::Decorator<float> dec_triggerElectronEfficiency("triggerElectronEfficiency");
  static SG::AuxElement::Decorator<float> dec_triggerMuonEfficiencySF("triggerMuonEfficiencySF");
  static SG::AuxElement::Decorator<float> dec_triggerMuonEfficiency("triggerMuonEfficiency");

  static SG::AuxElement::ConstAccessor<char> acc_passOR("passOR");
  static SG::AuxElement::ConstAccessor<char> acc_isol("isol");
//  static SG::AuxElement::ConstAccessor<char> acc_bjet("bjet");
  static SG::AuxElement::ConstAccessor<char> acc_signal("signal");
  static SG::AuxElement::ConstAccessor<char> acc_baseline("baseline");
  static SG::AuxElement::ConstAccessor<char> acc_pass_cft("passCFT");

  //static SG::AuxElement::ConstAccessor<char> acc_nearlySignal("nearlySignal"); 
  static SG::AuxElement::ConstAccessor<char> acc_tighterNearlySignal("tighterNearlySignal"); // Leptons satisfying the Tighter selection but without isolation requirements.
  static SG::AuxElement::ConstAccessor<char> acc_inclusiveLooserNearlySignal("inclusiveLooserNearlySignal");  // Leptons satisfying the Looser selection but without isolation requirements.

  
  static SG::AuxElement::ConstAccessor<char> acc_trig_matched("triggerMatched"); 

  // IFF Truth matching
  static SG::AuxElement::Decorator<int> dec_Class("Classification");

  static SG::AuxElement::ConstAccessor<float> acc_d0sig("d0sig");
  static SG::AuxElement::ConstAccessor<float> acc_z0sinTheta("z0sinTheta");

  //static SG::AuxElement::ConstAccessor<char> acc_eventClean_LooseBad("DFCommonJets_eventClean_LooseBad");


  //-----------------------------------------
  // Event info
  //-----------------------------------------

  const xAOD::EventInfo* eventInfo(0);
  ATH_CHECK(evtStore()->retrieve(eventInfo, "EventInfo") );
  bool isMC = eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION);

  int year = m_SUSYTools_tighter->treatAsYear();
  ATH_MSG_DEBUG("year: " << year);

  //-----------------------------------------
  // Set systematics
  //-----------------------------------------

  if (c_systName == "Nominal") {
    if( m_SUSYTools_tighter->resetSystematics() != CP::SystematicCode::Ok || 
        m_SUSYTools_looser->resetSystematics() != CP::SystematicCode::Ok) {
      ATH_MSG_ERROR( "Failed to reset systematics to nominal!" );
      return StatusCode::FAILURE;
    }
    
  }
  else{
    ATH_MSG_DEBUG( "Apply systematic variation " << c_systName << " ... " );
    if (
        m_SUSYTools_tighter->applySystematicVariation(m_systInfo.systset) != CP::SystematicCode::Ok || 
        m_SUSYTools_looser->applySystematicVariation(m_systInfo.systset) != CP::SystematicCode::Ok
        ) {
      ATH_MSG_ERROR( "Cannot configure SUSYTools for systematic variation " << c_systName);
      return StatusCode::FAILURE;
    }
  }
  
  // Will be "nominal" in the case of nominal 
  if(c_doTileCorrection && m_tileTool->applySystematicVariation(m_systInfo.systset) != CP::SystematicCode::Ok) {
    ATH_MSG_ERROR( "Failed to configure the tileCorrTool for systematic variation " << c_systName  );
    return StatusCode::FAILURE;
  }
  

  ATH_MSG_DEBUG( "Applied systematic variation " << c_systName );


  //-----------------------------------------
  // PRW
  //-----------------------------------------

  // already applied in ntupleGeneratorAlg? 


  //-----------------------------------------
  // Weights
  //-----------------------------------------

  // The "nominal" weight should be the same as the standard m_eventWeight = m_eventInfo->mcEventWeight();
  // Except, this isn't the case for the LQ models because ATLAS  
  double nominalWeight = 1.0; 
  std::vector<float> ScaleWeights;
  if(isMC){
    int dsid = eventInfo->mcChannelNumber();

    if( dsid >= 311841 && dsid <= 311866 ){
      // for the LQ models, the nominal weight is instead stored with in another location
      // Exactly what that location isn't clear, but it's either "45" or " PDF=260000 MemberID=1 ". 
      // STUPID
      //std::string alternative_weight_index_key = " PDF=260000 MemberID=1 "; // yes there are spaces 
      std::string alternative_weight_index_key = "45"; // no spaces. This is definitely the correct index. 
      nominalWeight = m_weightTool->getWeight(alternative_weight_index_key); 
      // note the normal index for the nominal weight is ' nominal ', and yes there are spaces either side of it
    }
    else nominalWeight = eventInfo->mcEventWeights().at(0);

    std::vector<int> scale_samples = {364250, 364253, 364254, 364255, 364288, 364289, 364290, 363356, 363358, 363359, 363360, 363489, 364283, 364284, 364285, 364287};//, 310244, 310245, 310246, 310247, 310248, 310249, 310250, 310251, 310252, 310253, 310254, 310255, 310256, 310257, 310258, 310259, 310260, 310261, 310262, 310263, 310264, 310265, 310266, 310267, 310268, 310269, 310270, 310271, 310272, 310273, 310274, 310275, 310276, 310277, 310278, 310279, 310280, 310281, 310282, 310283, 310284, 310285, 310286, 310287, 310288, 310289, 310290, 310291, 310292, 310293, 310294, 310295, 310296, 310297, 310298, 310299, 310300, 310301, 310302, 310303, 310304, 310305, 311841, 311842, 311843, 311844, 311845, 311846, 311847, 311848, 311849, 311850, 311851, 311852, 311853, 311854, 311855, 311856, 311857, 311858, 311859, 311860, 311861, 311862, 311863, 311864, 311865, 311866};
    bool is_scale_sample = false;
    is_scale_sample = std::find(scale_samples.begin(), scale_samples.end(), dsid) != scale_samples.end();
    if( is_scale_sample ){
      ScaleWeights.push_back( m_weightTool->getWeight("MUR0.5_MUF0.5_PDF261000") );
      ScaleWeights.push_back( m_weightTool->getWeight("MUR0.5_MUF1_PDF261000") );
      ScaleWeights.push_back( m_weightTool->getWeight("MUR1_MUF0.5_PDF261000") );
      ScaleWeights.push_back( m_weightTool->getWeight("MUR2_MUF2_PDF261000") );
      ScaleWeights.push_back( m_weightTool->getWeight("MUR2_MUF1_PDF261000") );
      ScaleWeights.push_back( m_weightTool->getWeight("MUR1_MUF2_PDF261000") );
    }
  }
  dec_nominalEventWeight(*eventInfo) = nominalWeight; 

  dec_ScaleWeights(*eventInfo) = ScaleWeights; 
  /************
  std::cout << "new event" << std::endl;
  std::cout << "Default weight: " << eventInfo->mcEventWeight() << std::endl;
  std::cout << "default weight 0: " << eventInfo->mcEventWeights().at(0) << std::endl;
  for (auto weight : m_weightTool->getWeightNames()) {
    std::cout << "\tweight name: \"" << weight << "\" value: " << m_weightTool->getWeight(weight) << std::endl; 
  }
  ************/




  //-----------------------------------------
  // Retrieve Jets
  //-----------------------------------------

  ATH_MSG_DEBUG( "Getting Jets" );
  xAOD::JetContainer* jets(0);
  xAOD::ShallowAuxContainer* jetsAux(0);
//  if ( (m_affectsJets || m_affectsBTag) && m_affectsKinematics) {
  if ( m_affectsJets && m_affectsKinematics) {
    if (c_systName == "Nominal") {
      ATH_CHECK(m_SUSYTools_tighter->GetJets(jets, jetsAux, false) ); // Setting the third argument to false prevents SUSYTools from writing the container to the StoreGate
    }
    else {
      const xAOD::JetContainer* jetsNominal(0);
      ATH_CHECK(evtStore()->retrieve(jetsNominal, c_nominalPrefix+"Jets"+c_nominalSuffix) );
      ATH_CHECK(m_SUSYTools_tighter->GetJetsSyst(*jetsNominal, jets, jetsAux, false) ); // Setting the third argument to false prevents SUSYTools from writing the container to the StoreGate
    }
  }
  else {
    ATH_CHECK(shallowCopyNominal(jets, jetsAux, "Jets") );
  }
  ATH_CHECK(recordContainer(jets, jetsAux, "Jets") );


  //-----------------------------------------
  // Retrieve Electrons 
  //-----------------------------------------

  xAOD::ElectronContainer* electrons(0);
  xAOD::ShallowAuxContainer* electronsAux(0);
  //std::unique_ptr<xAOD::ElectronContainer> electrons = std::make_unique<xAOD::ElectronContainer>();
  //std::unique_ptr<xAOD::ShallowAuxContainer>  electronsAux = std::make_unique<xAOD::ShallowAuxContainer>();
  if (m_affectsElectrons && m_affectsKinematics) {
    ATH_CHECK( m_SUSYTools_tighter->GetElectrons(electrons,electronsAux, false) ); // Setting the third argument to false prevents SUSYTools from writing the container to the StoreGate
  }
  else{
    ATH_CHECK(shallowCopyNominal(electrons, electronsAux, "Electrons") );
  }
  ATH_CHECK(recordContainer(electrons, electronsAux, "Electrons") );



  //----------------------------------------
  // Retrieve Muons
  //----------------------------------------

  ATH_MSG_DEBUG( "Getting Muons" );
  xAOD::MuonContainer* muons(0);
  xAOD::ShallowAuxContainer* muonsAux(0);
  if (m_affectsMuons && m_affectsKinematics) {
    ATH_CHECK(m_SUSYTools_tighter->GetMuons(muons, muonsAux, false) ); // Setting the third argument to false prevents SUSYTools from writing the container to the StoreGate
  }
  else{
    ATH_CHECK(shallowCopyNominal(muons, muonsAux, "Muons") );
  }
  ATH_CHECK(recordContainer(muons, muonsAux, "Muons") );



  //----------------------------------------
  // Overlap removal
  //----------------------------------------

  ATH_MSG_DEBUG("Doing overlap removal...");
  ATH_CHECK( m_SUSYTools_tighter->OverlapRemoval(electrons, muons, jets)) ;
  ATH_MSG_DEBUG("Done with overlap removal.");




  //-----------------------------------------
  // Add decorations to electrons 
  //-----------------------------------------

  ATH_MSG_VERBOSE( "There are " << electrons->size() << " electrons in this event");
  //std::cout << "There are " << electrons->size() << " electrons in this event" << std::endl;
  ATH_MSG_VERBOSE("IsBaseline, passOR, inclusiveLooserNearlySignal, tighterNearlySignal, CFT, isTrigMatched, isIsolated");
  if(m_affectsElectrons){
    // Note m_affectsElectrons is  equivalent to m_affectsElectrons && (m_affectsKinematics || m_affectsWeights
    for( const xAOD::Electron* el : *electrons ) {

      dec_pass_cft(*el) =  m_electronChargeIDSelectorTool->accept(el);


      const Root::TAccept isIsoSel = isoTool->accept(*el);
      dec_Gradient(*el)         = m_hasEleIsoGradient         ? isIsoSel.getCutResult("Gradient")         : false;
      dec_FCHighPtCaloOnly(*el) = m_hasEleIsoFCHighPtCaloOnly ? isIsoSel.getCutResult("FCHighPtCaloOnly") : false;
      dec_FCLoose(*el)          = m_hasEleIsoFCLoose          ? isIsoSel.getCutResult("FCLoose")          : false;
      dec_FCTight(*el)          = m_hasEleIsoFCTight          ? isIsoSel.getCutResult("FCTight")          : false;
      dec_FCLoose_FixedRad(*el) = m_hasEleIsoFCLoose_FixedRad ? isIsoSel.getCutResult("FCLoose_FixedRad") : false;
      dec_FCTight_FixedRad(*el) = m_hasEleIsoFCTight_FixedRad ? isIsoSel.getCutResult("FCTight_FixedRad") : false;


      // relic from old trigger matching 
      dec_trig_matched(*el) = false;//isMatched;

      // Trigger matching (new)
      bool match_dielectron = m_SUSYTools_tighter->IsTrigMatched(el, m_electron_triggers[year]);
      bool match_emu =  m_SUSYTools_tighter->IsTrigMatched(el, m_emu_triggers[year]);
      ATH_MSG_DEBUG("di-electron trigger: " << m_electron_triggers[year]);
      dec_diEMuTrigMatch(*el) = match_emu; 
      dec_diElectronTrigMatch(*el) = match_dielectron;  

      // Single lepton Trigger matching
      if(c_single_lepton_triggers){
        bool match_singleelectron = m_SUSYTools_tighter->IsTrigMatched(el, m_singleelectron_triggers[year]);
        ATH_MSG_DEBUG("single-electron trigger: " << m_singleelectron_triggers[year]);
        dec_singleElectronTrigMatch(*el) = match_dielectron;  

        // single lepton triggers
        dec_triggerElectronEfficiencySF(*eventInfo) = m_SUSYTools_tighter->GetEleTriggerEfficiencySF(*el, m_singleelectron_triggers[year]);
        dec_triggerElectronEfficiency(*eventInfo)   = m_SUSYTools_tighter->GetEleTriggerEfficiency(*el, m_singleelectron_triggers[year]);
      }

      if(isMC){
        // scale factors
        dec_tighter_SF(*el)       = m_SUSYTools_tighter->GetSignalElecSF(*el, true,  true,  false, false); // Reco and loose ID effs
        dec_looser_SF(*el)        = m_SUSYTools_looser->GetSignalElecSF(*el,  true,  true,  false, false); // Reco and tight ID effs
        dec_iso_SF(*el)           = m_SUSYTools_tighter->GetSignalElecSF(*el, false, false, false, true ); // Isolation SF (for the WP set in the SUSYTools config)

        // Currently a duplicate of what's extracted inside the ObjectWriter
        dec_joint_IDSF_CFTSF(*el) = m_SUSYTools_tighter->GetSignalElecSF(*el, false,false,false,false, "", true ); // joint IDSF and charge flip tagger efficiency |SF


        // IFF Truth classification
        int IFFClassification(-1);
        if(acc_passOR(*el) && acc_baseline(*el)){
          IFFClassification = static_cast<int>(m_TruthClassifier->classify(*el));
          ATH_MSG_DEBUG( Form("Classification: Electron (pt=%.1f): IFFClassification %i", el->pt(), IFFClassification) );
        }
        dec_Class(*el) = IFFClassification;
        if(IFFClassification == 0 && acc_passOR(*el) && acc_baseline(*el)){
          ATH_MSG_WARNING("IFFClassification 'Unknown' for electron:");
          ATH_MSG_WARNING( "event number:"  << eventInfo->eventNumber() << ", run number: " << eventInfo->runNumber() ); 
        }
      }

      // move this here,  since this call may clear other  things
      bool isTighterNearlySignal         = m_SUSYTools_tighter->IsSignalElectron(*el, c_elePt, c_eled0sig, c_elez0, c_eleEta);
      bool isInclusiveLooserNearlySignal = m_SUSYTools_looser->IsSignalElectron( *el, c_elePt, c_eled0sig, c_elez0, c_eleEta);
      dec_inclusiveLooserNearlySignal(*el) = isInclusiveLooserNearlySignal;
      dec_tighterNearlySignal(*el)         = isTighterNearlySignal;


      ATH_MSG_VERBOSE( "\t" 
          << ((int) acc_baseline(*el)) << "," 
          << ((int) acc_passOR(*el)) << ","
          << ((int) acc_inclusiveLooserNearlySignal(*el)) << "," 
          << ((int) acc_tighterNearlySignal(*el)) << ","
          << ((int) acc_pass_cft(*el)) << "," 
          << ((int) acc_trig_matched (*el)) << "," 
          << ((int) acc_isol(*el)) <<  "," 
          << isIsoSel.getCutResult("Gradient") 
          );
    }
  }

  //----------------------------------------
  // Add decorations to Muons
  //----------------------------------------

  ATH_MSG_VERBOSE( "There are " << muons->size() << " muons in this event");
  ATH_MSG_VERBOSE("IsBaseline, passOR, inclusiveLooserNearlySignal, tighterNearlySignal, isTrigMatched, isIsolated ");
  if(m_affectsMuons){
    for(const xAOD::Muon* mu : *muons){


      /***
       * REALLY DON't NEED THESE LINES. 
       * The signal decoration will replace this, since the looser and tigher susytools versions are the same for the muons
       * ALSO, when running on systematics that affect muons, since a shallow container is made 
       * then the dec_d0sig is empty when it is called indisde IsSignalMuon, meaning that this becomes empty instead of inheriting the value set in the original GetMuons function. 
       * THIS IS BAD
       bool isInclusiveLooserNearlySignal = m_SUSYTools_looser->IsSignalMuon( *mu, c_muPt, c_mud0sig, c_muz0, c_muEta);
       bool isTighterNearlySignal         = m_SUSYTools_tighter->IsSignalMuon(*mu, c_muPt, c_mud0sig, c_muz0, c_muEta);
       bool isNearlySignal                = acc_passOR(*mu) && acc_baseline(*mu) && isInclusiveLooserNearlySignal;
       ***/

      const Root::TAccept isIsoSel     = isoTool->accept(*mu);
      dec_HighPtTrackOnly(*mu)         = m_hasMuonIsoHighPtTrackOnly         ? isIsoSel.getCutResult("HighPtTrackOnly")         : false;
      dec_FCTightTrackOnly(*mu)        = m_hasMuonIsoFCTightTrackOnly        ? isIsoSel.getCutResult("FCTightTrackOnly")        : false;
      dec_FCLoose(*mu)                 = m_hasMuonIsoFCLoose                 ? isIsoSel.getCutResult("FCLoose")                 : false;
      dec_FCTight(*mu)                 = m_hasMuonIsoFCTight                 ? isIsoSel.getCutResult("FCTight")                 : false;
      dec_FCLoose_FixedRad(*mu)        = m_hasMuonIsoFCLoose_FixedRad        ? isIsoSel.getCutResult("FCLoose_FixedRad")        : false;
      dec_FCTight_FixedRad(*mu)        = m_hasMuonIsoFCTight_FixedRad        ? isIsoSel.getCutResult("FCTight_FixedRad")        : false;
      dec_Tight_VarRad(*mu)            = m_hasMuonIsoTight_VarRad            ? isIsoSel.getCutResult("Tight_VarRad")            : false;

      //dec_passHighPtCuts(*mu)          = m_SUSYTools_tighter->IsHighPtMuon(*mu);

      // Trigger matching (old)
      dec_trig_matched(*mu) = false;//isMatched;

      // Trigger matching (new)
      bool match_dimuon = m_SUSYTools_tighter->IsTrigMatched(mu, m_muon_triggers[year]);
      bool match_emu_mu    = m_SUSYTools_tighter->IsTrigMatched(mu, m_emu_triggers[year]);
      dec_diMuonTrigMatch(*mu) = match_dimuon;
      dec_diEMuTrigMatch(*mu) = match_emu_mu;
      ATH_MSG_DEBUG("dimuon trigger: " << m_muon_triggers[year]);
      ATH_MSG_DEBUG("emu trigger: " << m_emu_triggers[year]);

      // single lepton Trigger matching
      if(c_single_lepton_triggers){
        bool match_singlemuon = m_SUSYTools_tighter->IsTrigMatched(mu, m_singlemuon_triggers[year]);
        dec_singleMuonTrigMatch(*mu) = match_singlemuon;
        ATH_MSG_DEBUG("single-muon trigger: " << m_singlemuon_triggers[year]);

        //single lepton trigger SFs
        dec_triggerMuonEfficiency(*eventInfo)   = -1.0;//m_SUSYTools_tighter->GetMuonTriggerEfficiency(*mu, m_singlemuon_triggers[year], m_isData);
      }

      if(isMC){

        // scale factors
        dec_tighter_SF(*mu) = m_SUSYTools_tighter->GetSignalMuonSF(*mu, true, false);
        dec_iso_SF(*mu) =  m_SUSYTools_tighter->GetSignalMuonSF(*mu, false, true);

        // IFF Truth Classifier
        int IFFClassification(-1);
        if(acc_passOR(*mu) && acc_baseline(*mu)){
          IFFClassification = static_cast<int>(m_TruthClassifier->classify(*mu));
          ATH_MSG_DEBUG( Form("Classification: Muon (pt=%.1f): IFFClassification %i", mu->pt(), IFFClassification) );
        }
        dec_Class(*mu) = IFFClassification;
      }


      ATH_MSG_VERBOSE( "\t"  
          << ((int) acc_baseline(*mu)) << "," 
          << ((int) acc_passOR(*mu)) << ","
//          << ((int) acc_inclusiveLooserNearlySignal(*mu)) << "," 
//          << ((int) acc_tighterNearlySignal(*mu)) << ","
          << ((int) acc_trig_matched (*mu)) << ","  
          << ((int) acc_isol(*mu)) <<  "," 
          );
    }
  }
  

  // Trigger scale factors
  // WJF: note this uses the "signal" decoration from SUSYTools 
  if(isMC){
    dec_triggerGlobalEfficiencySF(*eventInfo) = m_SUSYTools_tighter->GetTriggerGlobalEfficiencySF(*electrons, *muons, "diLepton");
    dec_triggerGlobalEfficiency(*eventInfo)   = m_SUSYTools_tighter->GetTriggerGlobalEfficiency(*electrons, *muons, "diLepton");
    // single lepton 
    if(c_single_lepton_triggers){
      dec_triggerMuonEfficiencySF(*eventInfo) = -1.0;//m_SUSYTools_tighter->GetTotalMuonTriggerSF(*muons, m_singlemuon_triggers[year]);
    }
  }


  //----------------------------------------
  // MET
  //----------------------------------------

  // TST met (default) 
  double metsignificance(-1.0); 
  if(m_affectsKinematics){
    xAOD::MissingETContainer* met = new xAOD::MissingETContainer();
    xAOD::MissingETAuxContainer* metAux = new xAOD::MissingETAuxContainer();
    met->setStore(metAux);
    ATH_CHECK( m_SUSYTools_tighter->GetMET(*met, jets, electrons, muons ) );
    ATH_CHECK(recordContainer(met, metAux, "METRefFinal") );
    // Get METSig
    ATH_CHECK( m_SUSYTools_tighter->GetMETSig(*met, metsignificance, true, false ) );
  }
  else{
    xAOD::MissingETContainer* met(0);
    xAOD::ShallowAuxContainer* metAux(0);
    ATH_CHECK(shallowCopyNominal(met, metAux, "METRefFinal") );
    ATH_CHECK(recordContainer(met, metAux, "METRefFinal") );
    // Get METSig
    ATH_CHECK( m_SUSYTools_tighter->GetMETSig(*met, metsignificance, true, false ) );
  }

  dec_metsignificance(*eventInfo) = metsignificance;



  return StatusCode::SUCCESS;
}



StatusCode ObjectGetter::beginInputFile() { 
  //
  //This method is called at the start of each input file, even if
  //the input file contains no events. Accumulate metadata information here
  //

  //example of retrieval of CutBookkeepers: (remember you will need to include the necessary header files and use statements in requirements file)
  // const xAOD::CutBookkeeperContainer* bks = 0;
  // CHECK( inputMetaStore()->retrieve(bks, "CutBookkeepers") );

  //example of IOVMetaData retrieval (see https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AthAnalysisBase#How_to_access_file_metadata_in_C)
  //float beamEnergy(0); CHECK( retrieveMetadata("/TagInfo","beam_energy",beamEnergy) );
  //std::vector<float> bunchPattern; CHECK( retrieveMetadata("/Digitiation/Parameters","BeamIntensityPattern",bunchPattern) );

  return StatusCode::SUCCESS;
}
