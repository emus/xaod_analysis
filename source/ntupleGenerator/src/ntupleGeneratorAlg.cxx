// ntupleGenerator includes
#include "ntupleGeneratorAlg.h"

#include "xAODEventInfo/EventInfo.h"
#include "xAODCutFlow/CutBookkeeperContainer.h"
#include "xAODCutFlow/CutBookkeeper.h"

#include "GaudiKernel/IEvtSelector.h" // used to get number of events in a file
#include "AthenaKernel/ICollectionSize.h" // used to get the number of events in a file


ntupleGeneratorAlg::ntupleGeneratorAlg( const std::string& name, ISvcLocator* pSvcLocator ) 
  : AthAnalysisAlgorithm( name, pSvcLocator ),
  m_eventNumber(0),
  m_fileNumber(0)
{

  //declareProperty( "Property", m_nProperty = 0, "My Example Integer Property" ); //example property declaration
  declareProperty("ConfigTreeTool", m_configTreeTool);
  declareProperty("SUSYTools_Tighter", m_SUSYTools_tighter);
  declareProperty("SUSYTools_Looser", m_SUSYTools_looser);
  declareProperty("GRLTool", m_grlTool); 
  declareProperty("single_lepton_triggers", m_single_lepton_triggers);
  declareProperty("MyStreamName", m_streamName);


}


ntupleGeneratorAlg::~ntupleGeneratorAlg() {}


StatusCode ntupleGeneratorAlg::failEvent(std::string reason) {
  ATH_MSG_DEBUG("Failed event because " << reason );
  return StatusCode::SUCCESS;
}


StatusCode ntupleGeneratorAlg::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");
  std::cout << "*\n*\nntupleGeneratorAlg::initialize()\n*\n*" << std::endl;
  /***********************************
  This is called once, before the start of the event loop
  Retrieves of tools you have configured in the joboptions go here
  
  
  Initializes cutflow histograms:
    h_numberOfEvents: stores information from cutbookkeepers
    h_cutflow: stores numbers of events passing each stage of the cutflow
    h_cutflowWeighted: weighted version of above 

  Tools initialized:
    GRL
    Triggers (via SUSYTools)
    ConfigTreeTool 
    CutFlowTool
  
  ************************************/


  //-----------------------------------------
  // Config Tree Tool  
  //-----------------------------------------

  //m_configTreeTool->initialize(); 
  ATH_CHECK( m_configTreeTool.retrieve() ); 
  c_isData = m_configTreeTool->getIsData();
  c_isMC   = m_configTreeTool->getIsMC(); 
  c_nFiles = m_configTreeTool->getNFiles();
  c_nEvents = m_configTreeTool->getNEvents();

  // Cutflow Tool
  ATH_CHECK( m_cutFlowTool.retrieve() );


  // triggers used in the analysis
  m_muon_triggers     = m_configTreeTool->get_dimuon_triggers();
  m_electron_triggers = m_configTreeTool->get_dielectron_triggers();
  m_emu_triggers      = m_configTreeTool->get_emu_triggers();
  // triggers for single lepton trigger study  
  m_singlemuon_triggers     = m_configTreeTool->get_singlemuon_triggers();
  m_singleelectron_triggers = m_configTreeTool->get_singleelectron_triggers();

  // Histogram example
  m_pileupProfile = new TH1D("pileupProfile","pileupProfile",100,0,100);
  CHECK( histSvc()->regHist("/"+m_streamName+"/pileupProfile", m_pileupProfile) ); //registers histogram to output stream

  //-----------------------------------------
  // Cutflows (partly moved to cutflow tool)
  //-----------------------------------------
  // Maybe move to a tool if this needs to access multiple algs? 

  // number of events hist may be redundant ... ? 
  int nBins = 10; 
  h_numberOfEvents = new TH1D("h_numberOfEvents", "Number of Events", nBins, 1, nBins + 1); 
  CHECK( histSvc()->regHist("/"+m_streamName+"/h_numberOfEvents", h_numberOfEvents) );
  h_numberOfEvents->GetXaxis()->SetBinLabel(1,"Nevents Generated");
  h_numberOfEvents->GetXaxis()->SetBinLabel(2,"Sum of weights Generated");
  h_numberOfEvents->GetXaxis()->SetBinLabel(3,"Sum of weights squared Generated");
  h_numberOfEvents->GetXaxis()->SetBinLabel(4,"Nevents in Derivation");
  h_numberOfEvents->GetXaxis()->SetBinLabel(5,"Nevents run over");
  h_numberOfEvents->GetXaxis()->SetBinLabel(6,"Sum of weights run over");
  h_numberOfEvents->GetXaxis()->SetBinLabel(7,"Sum of weights squared run over");
  h_numberOfEvents->GetXaxis()->SetBinLabel(8,"Sum of weights PRW");
  h_numberOfEvents->GetXaxis()->SetBinLabel(9,"muR05,muF05 Sum of weights Generated");
  h_numberOfEvents->GetXaxis()->SetBinLabel(10, "unused");
  h_numberOfEvents->Sumw2();



  m_noCutBin              = m_cutFlowTool->registerCut("NoCut");
  m_grlCutBin             = m_cutFlowTool->registerCut("PassGRL");
  m_triggerCutBin         = m_cutFlowTool->registerCut("Trigger");
  m_LArErrorCutBin        = m_cutFlowTool->registerCut("LArError");
  m_TileErrorCutBin       = m_cutFlowTool->registerCut("TileError");
  m_SCTErrorCutBin        = m_cutFlowTool->registerCut("SCTError");
  m_incompleteEventCutBin = m_cutFlowTool->registerCut("IncompleteEvent");
  m_batmanCutBin          = m_cutFlowTool->registerCut("PassBatmanCleaning");
  m_pvCutBin              = m_cutFlowTool->registerCut("PrimaryVertex");


  //-----------------------------------------
  // SUSYTools 
  //-----------------------------------------

  ATH_MSG_INFO("Attempting to retrieve Tighter SUSYTools tool: " );
  ATH_CHECK( m_SUSYTools_tighter.retrieve() );
  ATH_MSG_INFO("Retrieved Tighter SUSYTools tool: [" << m_SUSYTools_tighter.name() <<"]");

  ATH_MSG_INFO("Attempting to retrieve Looser SUSYTools tool: " );
  ATH_CHECK( m_SUSYTools_looser.retrieve() );
  ATH_MSG_INFO("Retrieved Looser SUSYTools tool: [" << m_SUSYTools_looser.name() <<"]");
  
/*  // Get list of Experimental Systematics:
  std::vector<ST::SystInfo> systInfoList = m_SUSYTools_tighter->getSystInfoList();
  for (const auto& sysInfo : systInfoList) {
    const CP::SystematicSet& sys = sysInfo.systset;
    ATH_MSG_INFO("SYST CALLED: "<<sys.name());
  }
*/

  //-----------------------------------------
  // GRL
  //-----------------------------------------
  ATH_MSG_INFO("About to retrieve GRL Tool: [" << m_grlTool.name() <<"]");
  ATH_CHECK( m_grlTool.retrieve() );
  ATH_MSG_INFO( "Retrieved GRL Tool" );


  //-----------------------------------------
  // Number of events  
  //-----------------------------------------

  ServiceHandle<IEvtSelector> evtSelector("EventSelector","");
  m_jobNumberOfEvents = dynamic_cast<ICollectionSize&>(*evtSelector).size(); // number of events this job will run over
  if (c_nEvents > 0 && c_nEvents < m_jobNumberOfEvents) m_jobNumberOfEvents = c_nEvents;



  return StatusCode::SUCCESS;
}

StatusCode ntupleGeneratorAlg::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");
  //
  //Things that happen once at the end of the event loop go here
  //


  return StatusCode::SUCCESS;
}

StatusCode ntupleGeneratorAlg::execute() {  
  ATH_MSG_DEBUG ("Executing " << name() << "...");
  setFilterPassed(false); //optional: start with algorithm not passed

  //
  //Your main analysis code goes here
  //If you will use this algorithm to perform event skimming, you
  //should ensure the setFilterPassed method is called
  //If never called, the algorithm is assumed to have 'passed' by default
  //

  //-----------------------------------------
  // Stopwatch and monitoring  
  //-----------------------------------------

  if (m_fileEventNumber % 1000 == 1 || (m_eventNumber < 1000 && m_eventNumber % 100 == 1) ) {
    double pcComplete = 100.0*static_cast<float>(m_eventNumber) / static_cast<float>(m_jobNumberOfEvents);

    ATH_MSG_INFO( "Processing event " << m_eventNumber << " of " << m_jobNumberOfEvents << " (" << pcComplete << "%). " << m_fileEventNumber << "/" << m_nFileEvents << ", " << m_fileNumber-1 << "/" << c_nFiles << " (events processed in file/events in file), (files processed/number of files)");
  }
  ++m_eventNumber;
  ++m_fileEventNumber;


  //-----------------------------------------
  // Decorators 
  //-----------------------------------------
  
  static SG::AuxElement::ConstAccessor<char> dec_isBadBatman("DFCommonJets_isBadBatman");
  static SG::AuxElement::Decorator<unsigned int> dec_pvIndex("pvIndex");

  // Triggers 
  static SG::AuxElement::Decorator<char> dec_passSingleelectronTrigger("passSingleelectronTrigger");
  static SG::AuxElement::Decorator<char> dec_passSingleMuonTrigger("passSinglemuonTrigger");
  static SG::AuxElement::Decorator<char> dec_passDielectronTrigger("passDielectronTrigger");
  static SG::AuxElement::Decorator<char> dec_passDiMuonTrigger("passDimuonTrigger");
  static SG::AuxElement::Decorator<char> dec_passEMuTrigger("passEMuTrigger");


  //-----------------------------------------
  // Event info, pileup, weights 
  //-----------------------------------------

  // Event info
  m_eventInfo = 0;
  CHECK( evtStore()->retrieve( m_eventInfo , "EventInfo" ) );
  m_pileupProfile->Fill( m_eventInfo->averageInteractionsPerCrossing() ); //fill mu into histogram


  // Weights
  m_eventWeight = 1.0;
  m_pileupWeight = 1.0; 
  if(c_isMC){
    m_eventWeight = m_eventInfo->mcEventWeight();
    ATH_CHECK( m_SUSYTools_tighter->ApplyPRWTool() ); // TODO: check if looser tool needs PRW? 
    m_pileupWeight = m_SUSYTools_tighter->GetPileupWeight();
    ATH_MSG_DEBUG("Extracted pileup weight: " << m_pileupWeight); 
  }
  m_totalWeight = m_eventWeight * m_pileupWeight;

  ATH_MSG_VERBOSE("event weight " << m_eventWeight);
  ATH_MSG_VERBOSE("pileup weight " << m_pileupWeight);


  // Year 
  // work out what year this data or MC represents 
  const int year = m_SUSYTools_tighter->treatAsYear();

  // Histograms 
  h_numberOfEvents->Fill(5, 1.);
  h_numberOfEvents->Fill(6, m_eventWeight);
  h_numberOfEvents->Fill(7, m_eventWeight*m_eventWeight);

  // Cut 0: number of events executed
  m_cutFlowTool->incrementCut(m_noCutBin);


  //-----------------------------------------
  // GRL
  //-----------------------------------------

  // Cut 1: GRL 
  if(c_isData){
    int passGRL = m_grlTool->passRunLB(*m_eventInfo); 
    if(!passGRL) return failEvent("GRL"); 
  }
  m_cutFlowTool->incrementCut(m_grlCutBin); // passes for MC, or if not failed for data 


  //-----------------------------------------
  // Trigger (new) 
  //-----------------------------------------


  // normal dilepton trigger stuff
  bool pass_dielectron_triggger = m_SUSYTools_tighter->IsTrigPassed( m_electron_triggers[year] );
  bool pass_dimuon_trigger      = m_SUSYTools_tighter->IsTrigPassed( m_muon_triggers[year] );
  bool pass_emu_trigger         = m_SUSYTools_tighter->IsTrigPassed( m_emu_triggers[year] );

  dec_passDielectronTrigger(*m_eventInfo) = pass_dielectron_triggger;
  dec_passDiMuonTrigger(*m_eventInfo)     = pass_dimuon_trigger;
  dec_passEMuTrigger(*m_eventInfo)        = pass_emu_trigger;

  bool passDileptonTrigger = pass_dielectron_triggger || pass_dimuon_trigger || pass_emu_trigger;

  // The trigger decision 
  bool passTrigger = passDileptonTrigger; 

  // single lepton trigger stuff
  bool passSingleLeptonTrigger = false;
  if(m_single_lepton_triggers){
    bool pass_singleelectron_triggger = m_SUSYTools_tighter->IsTrigPassed( m_singleelectron_triggers[year] );
    bool pass_singlemuon_trigger      = m_SUSYTools_tighter->IsTrigPassed( m_singlemuon_triggers[year] );
    dec_passSingleelectronTrigger(*m_eventInfo) = pass_singleelectron_triggger;
    dec_passSingleMuonTrigger(*m_eventInfo)     = pass_singlemuon_trigger;
    passSingleLeptonTrigger = pass_singleelectron_triggger;//|| pass_singlemuon_trigger;

    // update the trigger decision
//    passTrigger = passTrigger || passSingleLeptonTrigger; 
//    passTrigger = passSingleLeptonTrigger; 
  }

  // bool passTrigger = pass_singleelectron_triggger || pass_singlemuon_trigger; // only need if we are applying JUST single lepton


  // Cut 2: Trigger
  if(!passTrigger) return failEvent("Trigger");
  m_cutFlowTool->incrementCut(m_triggerCutBin);


  //-----------------------------------------
  // Event cleaning 
  //-----------------------------------------

  // Cut 3: Cleaning cuts

  if(c_isMC){
    // All of these pass for MC 
    m_cutFlowTool->incrementCut(m_LArErrorCutBin);
    m_cutFlowTool->incrementCut(m_TileErrorCutBin);
    m_cutFlowTool->incrementCut(m_SCTErrorCutBin);
    m_cutFlowTool->incrementCut(m_incompleteEventCutBin);
    m_cutFlowTool->incrementCut(m_batmanCutBin); 
  }
  else{

    // Cut 3a: LAr
    if (m_eventInfo->errorState(xAOD::EventInfo::LAr)==xAOD::EventInfo::Error) return failEvent("LAr");
    m_cutFlowTool->incrementCut(m_LArErrorCutBin);

    // Cut 3b: Tile
    if (m_eventInfo->errorState(xAOD::EventInfo::Tile) == xAOD::EventInfo::Error) return failEvent("Tile");
    m_cutFlowTool->incrementCut(m_TileErrorCutBin);

    // Cut 3c: SCT
    if (m_eventInfo->errorState(xAOD::EventInfo::SCT) == xAOD::EventInfo::Error) return failEvent("SCT");
    m_cutFlowTool->incrementCut(m_SCTErrorCutBin);

    // Cut 3d: incomplete event 
    // WJF: two ways of doing this, presumably they are identical
    if (
        m_eventInfo->isEventFlagBitSet(xAOD::EventInfo::Core, 18) || 
        m_eventInfo->eventFlags(xAOD::EventInfo::Core) & 0x40000
        ){
      ATH_MSG_WARNING("This event is incompletely built. Skipping.");
      return failEvent("Incomplete Event");
    }
    m_cutFlowTool->incrementCut(m_incompleteEventCutBin); 

    // Cut 3e: Batman cleaning is applied only to 2015+2016 data
    if (c_isData && m_eventInfo->runNumber() >= 276262 && m_eventInfo->runNumber() <= 340453) {  // https://twiki.cern.ch/twiki/bin/view/AtlasProtected/HowToCleanJets2017#IsBadBatMan_Event_Flag_and_EMEC 
      if ( dec_isBadBatman(*m_eventInfo) ) return failEvent("Fail Batman cleaning");
    }
    m_cutFlowTool->incrementCut(m_batmanCutBin);
  }

  // Cut 4: Primary vertex
  ATH_MSG_DEBUG("Primary Vertex");
  const xAOD::Vertex* priVtx = m_SUSYTools_tighter->GetPrimVtx();
  if (!priVtx || priVtx->nTrackParticles() < 2) return failEvent("Primary Vertex"); // TODO check that 2 tracks are required 
  m_cutFlowTool->incrementCut(m_pvCutBin);
  dec_pvIndex(*m_eventInfo) = priVtx->index();



  


  setFilterPassed(true); //if got here, assume that means algorithm passed (useful for skimming) 
  return StatusCode::SUCCESS;
}

StatusCode ntupleGeneratorAlg::beginInputFile() { 
  //
  //This method is called at the start of each input file, even if
  //the input file contains no events. Accumulate metadata information here
  //

  std::cout << "*\n*\nntupleGeneratorAlg::beginInputFile()\n*\n*" << std::endl;

  //example of IOVMetaData retrieval (see https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AthAnalysisBase#How_to_access_file_metadata_in_C)
  //float beamEnergy(0); CHECK( retrieveMetadata("/TagInfo","beam_energy",beamEnergy) );
  //std::vector<float> bunchPattern; CHECK( retrieveMetadata("/Digitiation/Parameters","BeamIntensityPattern",bunchPattern) );




    // timer and file info 
    m_fileStartTime = std::chrono::system_clock::now();
    m_fileEventNumber = 0;
    ++m_fileNumber;

    // Access cutbookkeepers 
    std::cout << "*\n*\n*\nAttempting to access CutBookkeepers to retrieve sum of event weights.\n*\n*\n*" << std::endl;
    ATH_MSG_DEBUG("Attempting to access CutBookkeepers to retrieve sum of event weights.");
    if( inputMetaStore()->contains<xAOD::CutBookkeeperContainer>("CutBookkeepers") ) { //only found in MC15
      const xAOD::CutBookkeeperContainer* CBKCont(0);
      CHECK( inputMetaStore()->retrieve(CBKCont, "CutBookkeepers") );
      const xAOD::CutBookkeeper* allEventsCBK = 0; 
      const xAOD::CutBookkeeper* allEventsCBK_muR05_muF05 = 0; 
      const xAOD::CutBookkeeper* latestCBK = 0; 

      int maxAllCycle = -1;
      int maxCycle = -1; //need to find the max cycle where input stream is StreamAOD and the name is AllExecutedEvents
      int maxCycle_muR05_muF05 = -1; //need to find the max cycle where input stream is StreamAOD and the name is AllExecutedEvents

      for(const xAOD::CutBookkeeper* cbk : *CBKCont){
        if(cbk->inputStream()=="StreamAOD" && cbk->name()=="AllExecutedEvents" && cbk->cycle()>maxAllCycle){
          maxAllCycle=cbk->cycle();
          allEventsCBK = cbk;
        } 
        else if (cbk->name()=="LHE3Weight_muR=05,muF=05" && cbk->inputStream() == "StreamAOD" && cbk->cycle() > maxCycle_muR05_muF05){
          ATH_MSG_INFO("Getting ISR weight weight");
          maxCycle_muR05_muF05 = cbk->cycle();
          allEventsCBK_muR05_muF05 = cbk;
        }
        if(cbk->cycle() > maxCycle && cbk->name() == "AllExecutedEvents") {
          latestCBK = cbk;
          maxCycle = cbk->cycle();
        }
      }

      ATH_MSG_INFO("Done Looping");
      if(!allEventsCBK && !allEventsCBK_muR05_muF05){
        ATH_MSG_ERROR("Unable to find the all events CutBookkeeper object!" );
        return StatusCode::FAILURE;
      }

      // Extract info for histograms
      uint64_t nEventsAOD = allEventsCBK->nAcceptedEvents();
      double sumWeightsAOD = allEventsCBK->sumOfEventWeights();
      double sumWeightsAOD_muR05_muF05 = allEventsCBK_muR05_muF05!=0 ? allEventsCBK_muR05_muF05->sumOfEventWeights() : 0.0;
      double sumWeightsSquaredAOD = allEventsCBK->sumOfEventWeightsSquared();
      m_nFileEvents = latestCBK->nAcceptedEvents();

      ATH_MSG_INFO( "CutBookkeepers AOD information:" );
      ATH_MSG_INFO( "  Number of events       : " << nEventsAOD );
      ATH_MSG_INFO( "  Sum of weights         : " << sumWeightsAOD );
      ATH_MSG_INFO( "  Sum of weights muR=0.5,muF=0.5        : " << sumWeightsAOD_muR05_muF05 );
      ATH_MSG_INFO( "  Sum of weights squared : " << sumWeightsSquaredAOD );

      // Fill histograms TODO: fix binning 
      h_numberOfEvents->Fill(1, nEventsAOD);
      h_numberOfEvents->Fill(2, sumWeightsAOD);
      h_numberOfEvents->Fill(3, sumWeightsSquaredAOD);
      h_numberOfEvents->Fill(4, m_nFileEvents);
      h_numberOfEvents->Fill(9, sumWeightsAOD_muR05_muF05);

      
      m_cutFlowTool->incrementCutFlowBin(1, nEventsAOD);
      m_cutFlowTool->incrementCutFlowWeightedBin(1, sumWeightsAOD);

    } 
    else {
      ATH_MSG_ERROR("Unable to access CutBookkeepers. Weight histogram will not be filled!");
    }






  return StatusCode::SUCCESS;
}

StatusCode ntupleGeneratorAlg::endInputFile() {
  std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
  std::chrono::milliseconds diffTime = std::chrono::duration_cast<std::chrono::milliseconds>(now-m_fileStartTime);
  float diffTimeSeconds = diffTime.count() * 0.001;
  float rate = 0.0;
  //if (diffTime.count() > 0) rate = m_fileNumberOfEvents / (diffTime.count() * 0.001);
  if (diffTimeSeconds > 0) rate = m_nFileEvents / diffTimeSeconds;
  ATH_MSG_INFO("Timing: File complete, took " << std::fixed << std::setprecision(3) << diffTimeSeconds << " seconds for " << m_nFileEvents << " events ===> " << std::setprecision(3) << rate << " Hz");
  return StatusCode::SUCCESS;
}


//--------------------------------------------
// WJF: functions taken from old XAODAnalysisTool code
// seem to be largely from SUSYTools, may be able to remove completely?
// TODO
//--------------------------------------------

// Function taken from original XAODAnalysisTool
bool ntupleGeneratorAlg::pass_trigger_OR( const std::vector<std::string>& triggers ) const {
  if (triggers.size()==0) {
    ATH_MSG_WARNING("Taking OR of an empty list of triggers.");
    return false;
  }
  bool passed = false;
  for (auto& trig : triggers) {
    passed |= m_passedTriggers.at(trig);
    if (passed) break;
  }
  return passed;
}

// Function taken from original XAODAnalysisTool. Taken from SUSYTools.
std::vector<std::string> ntupleGeneratorAlg::GetTriggerOR(std::string trigExpr) const {
  //Split a ORed trigger string up
  static std::string delOR = "_OR_";
  std::vector<std::string> trigchains = {};

  size_t pos = 0;
  while ((pos = trigExpr.find(delOR)) != std::string::npos) {
    trigchains.push_back( "HLT_"+trigExpr.substr(0, pos) );
    trigExpr.erase(0, pos + delOR.length());
  }
  if(pos==std::string::npos) trigchains.push_back("HLT_"+trigExpr);
  return trigchains;
}

// Function taken from original XAODAnalysisTool (from old version of code), which itself was taken from SUSYTools 
void ntupleGeneratorAlg::GetTriggerTokens(std::string trigExpr, std::string& tokens15, std::string& tokens16, std::string& tokens17, std::string& tokens18) {
  static std::string del15 = "_2015_";
  static std::string del16 = "_2016_";
  static std::string del17 = "2017_";

  size_t pos = 0;
  if ( (pos = trigExpr.find(del15)) != std::string::npos) {
    trigExpr.erase(0, pos + del15.length());

    pos = 0;
    if ((pos = trigExpr.find(del16+del17)) != std::string::npos) {
      tokens15 = trigExpr.substr(0, pos);
      tokens16 = trigExpr.erase(0, pos + del16.length() + del17.length() );
      tokens17 = tokens16;
      tokens18 = tokens17;
      std::cout<<"tokens15: "<<tokens15<<" tokens16: "<<tokens16<<" tokens17: "<<tokens17<<" tokens18: "<<tokens18<<std::endl;
    }
    else if ((pos = trigExpr.find(del16)) != std::string::npos) {
      tokens15 = trigExpr.substr(0,pos);
      trigExpr.erase(0,pos+del16.length());
      pos = 0;
      while ((pos = trigExpr.find("_"+del17)) != std::string::npos){
        tokens16 = trigExpr.substr(0,pos);
        tokens17 = trigExpr.erase(0, pos+del17.length()+1);	
        tokens18 = tokens17;
        std::cout<<"tokens15: "<<tokens15<<" tokens16: "<<tokens16<<" tokens17: "<<tokens17<<" tokens18: "<<tokens18<<std::endl;
      }
    }
  }

  // For some reason the electron trigger SF tool uses a different naming convention for dilepton triggers.
  // The SUSYTools config file follows this, but I don't like it.
  if (tokens15 == "e12_lhloose_L1EM10VH") tokens15 = "2e12_lhloose_L12EM10VH";
  else if (tokens15 == "e17_lhvloose_nod0") tokens15 =  "2e17_lhvloose_nod0";
  if (tokens16 == "e12_lhloose_L1EM10VH") tokens16 = "2e12_lhloose_L12EM10VH";
  else if (tokens16 == "e17_lhvloose_nod0") tokens16 =  "2e17_lhvloose_nod0";
  if (tokens17 == "e24_lhloose_nod0") tokens17 = "2e24_lhloose_nod0";
  else if (tokens17 == "e24_lhvloose_nod0_L1EM20VH") tokens17 =  "2e24_lhvloose_nod0";
  if (tokens18 == "e24_lhloose_nod0") tokens18 = "2e24_lhloose_nod0";
  else if (tokens18 == "e24_lhvloose_nod0_L1EM20VH") tokens18 =  "2e24_lhvloose_nod0";
  std::cout<<"tokens15: "<<tokens15<<" tokens16: "<<tokens16<<" tokens17: "<<tokens17<<" tokens18: "<<tokens18<<std::endl;
}
