#ifndef NTUPLEGENERATOR_COMMONWRITER_H
#define NTUPLEGENERATOR_COMMONWRITER_H 1

#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"

//Example ROOT Includes
//#include "TTree.h"
//#include "TH1D.h"



class CommonWriter: public ::AthAnalysisAlgorithm { 
 public: 
  CommonWriter( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~CommonWriter(); 

  ///uncomment and implement methods as required

                                        //IS EXECUTED:
  virtual StatusCode  initialize();     //once, before any input is loaded
  virtual StatusCode  beginInputFile(); //start of each input file, only metadata loaded
  //virtual StatusCode  firstExecute();   //once, after first eventdata is loaded (not per file)
  virtual StatusCode  execute();        //per event
  //virtual StatusCode  endInputFile();   //end of each input file
  //virtual StatusCode  metaDataStop();   //when outputMetaStore is populated by MetaDataTools
  virtual StatusCode  finalize();       //once, after all events processed
  

  ///Other useful methods provided by base class are:
  ///evtStore()        : ServiceHandle to main event data storegate
  ///inputMetaStore()  : ServiceHandle to input metadata storegate
  ///outputMetaStore() : ServiceHandle to output metadata storegate
  ///histSvc()         : ServiceHandle to output ROOT service (writing TObjects)
  ///currentFile()     : TFile* to the currently open input file
  ///retrieveMetadata(...): See twiki.cern.ch/twiki/bin/view/AtlasProtected/AthAnalysisBase#ReadingMetaDataInCpp


 private: 

   TTree* m_commonTree = 0;

   // output branches
   ULong64_t EventNumber;
   Int_t     RunNumber;
   uint32_t  bcid;
   Int_t     LumiBlock; 


}; 

#endif //> !NTUPLEGENERATOR_COMMONWRITER_H
