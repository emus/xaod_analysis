#ifndef NTUPLEGENERATOR_NTUPLEGENERATORALG_H
#define NTUPLEGENERATOR_NTUPLEGENERATORALG_H 1

// Histogramming 
#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"

// SUSYTools 
#include "SUSYTools/ISUSYObjDef_xAODTool.h"

// Tool includes
#include "AsgTools/AnaToolHandle.h"
#include "AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h"

#include "ntupleGenerator/IConfigTreeTool.h"
#include "ntupleGenerator/CutFlowTool.h"



class IConfigTreeTool; 
//class IGoodRunsListSelectionTool;



class ntupleGeneratorAlg: public ::AthAnalysisAlgorithm { 
  public: 
    ntupleGeneratorAlg( const std::string& name, ISvcLocator* pSvcLocator );
    virtual ~ntupleGeneratorAlg(); 

    ///uncomment and implement methods as required

    //IS EXECUTED:
    virtual StatusCode  initialize();     //once, before any input is loaded
    virtual StatusCode  beginInputFile(); //start of each input file, only metadata loaded
    //virtual StatusCode  firstExecute();   //once, after first eventdata is loaded (not per file)
    virtual StatusCode  execute();        //per event
    virtual StatusCode  endInputFile();   //end of each input file
    //virtual StatusCode  metaDataStop();   //when outputMetaStore is populated by MetaDataTools
    virtual StatusCode  finalize();       //once, after all events processed


    ///Other useful methods provided by base class are:
    ///evtStore()        : ServiceHandle to main event data storegate
    ///inputMetaStore()  : ServiceHandle to input metadata storegate
    ///outputMetaStore() : ServiceHandle to output metadata storegate
    ///histSvc()         : ServiceHandle to output ROOT service (writing TObjects)
    ///currentFile()     : TFile* to the currently open input file
    ///retrieveMetadata(...): See twiki.cern.ch/twiki/bin/view/AtlasProtected/AthAnalysisBase#ReadingMetaDataInCpp


  private: 

    //Example algorithm property, see constructor for declaration:
    //int m_nProperty = 0;

    const xAOD::EventInfo* m_eventInfo;

    //Example histogram, see initialize method for registration to output histSvc
    TH1D* m_pileupProfile = 0;

    inline StatusCode failEvent(std::string);
    //int registerCut(std::string cutName);
    //void incrementCut(int cutBin);

    int m_noCutBin;
    int m_grlCutBin;
    int m_triggerCutBin;
    int m_LArErrorCutBin;
    int m_TileErrorCutBin;
    int m_SCTErrorCutBin;
    int m_incompleteEventCutBin;
    int m_batmanCutBin;
    int m_pvCutBin;
    std::string m_streamName;

    // config
    bool m_single_lepton_triggers;


    // scale factors/weights
    double m_eventWeight;
    double m_pileupWeight;
    double m_totalWeight;


    // GRL
    ToolHandle<IGoodRunsListSelectionTool>       m_grlTool;


    // SUSYTools 
    ToolHandle<ST::ISUSYObjDef_xAODTool> m_SUSYTools_tighter;
    ToolHandle<ST::ISUSYObjDef_xAODTool> m_SUSYTools_looser;


    // Monitoring
    TH1* h_numberOfEvents;
    std::chrono::system_clock::time_point m_fileStartTime;

    bool m_firstFile;
    uint64_t m_eventNumber; 
    uint64_t m_fileNumber;
    uint64_t m_nFileEvents;
    uint64_t m_fileEventNumber;
    uint64_t m_jobNumberOfEvents;

    // cutflow tool
    ToolHandle<CutFlowTool> m_cutFlowTool;

    // config
    ToolHandle<IConfigTreeTool> m_configTreeTool; 
    int c_isData; 
    int c_isMC; 
    int c_nFiles;
    int c_nEvents;

    // Trigger 
    std::vector<std::string> GetTriggerOR(std::string trigExpr) const;
    void GetTriggerTokens(std::string trigExpr, std::string& tokens15, std::string& tokens16, std::string& tokens17, std::string& tokens18);
    std::vector<std::string> m_triggers;
    bool pass_trigger_OR(const std::vector<std::string>& triggers) const;

    std::map<std::string,bool> m_passedTriggers;
    std::map<std::string, std::vector<std::string>> m_specialTriggers; // map from e.g. "single electron" to "HLT_e60_lhmedium"
    std::map<std::string, std::vector<std::string>> m_specialTriggers_2015; // map from e.g. "single electron" to "HLT_e60_lhmedium
    std::map<std::string, std::vector<std::string>> m_specialTriggers_2016; // map from e.g. "single electron" to "HLT_e60_lhmedium"
    std::map<std::string, std::vector<std::string>> m_specialTriggers_2017; // map from e.g. "single electron" to "HLT_e60_lhmedium"
    std::map<std::string, std::vector<std::string>> m_specialTriggers_2018; // map from e.g. "single electron" to "HLT_e60_lhmedium"
    std::map<std::string, std::string> m_triggerExprs;
    std::map<std::string, std::string> m_triggerExprs_2015;
    std::map<std::string, std::string> m_triggerExprs_2016;
    std::map<std::string, std::string> m_triggerExprs_2017;
    std::map<std::string, std::string> m_triggerExprs_2018;

    // trigger
    std::map<int, std::string > m_muon_triggers;
    std::map<int, std::string > m_electron_triggers;
    std::map<int, std::string > m_emu_triggers;
    // single lepton triggers
    std::map<int, std::string > m_singlemuon_triggers;
    std::map<int, std::string > m_singleelectron_triggers;


}; 

#endif //> !NTUPLEGENERATOR_NTUPLEGENERATORALG_H
