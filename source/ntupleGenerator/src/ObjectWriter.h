#ifndef NTUPLEGENERATOR_OBJECTWRITER_H
#define NTUPLEGENERATOR_OBJECTWRITER_H 1

#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"

// include needed for SystInfo
#include "SUSYTools/ISUSYObjDef_xAODTool.h"

#include "ElectronPhotonSelectorTools/AsgElectronChargeIDSelectorTool.h"
#include "ElectronEfficiencyCorrection/AsgElectronEfficiencyCorrectionTool.h"
#include "EgammaAnalysisInterfaces/IAsgElectronEfficiencyCorrectionTool.h"

#include "xAODTruth/xAODTruthHelpers.h"
#include "ntupleGenerator/IConfigTreeTool.h"
#include "ntupleGenerator/CutFlowTool.h"


class ObjectWriter: public ::AthAnalysisAlgorithm { 
 public: 
  ObjectWriter( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~ObjectWriter(); 

  ///uncomment and implement methods as required

                                        //IS EXECUTED:
  virtual StatusCode  initialize();     //once, before any input is loaded
  virtual StatusCode  beginInputFile(); //start of each input file, only metadata loaded
  //virtual StatusCode  firstExecute();   //once, after first eventdata is loaded (not per file)
  virtual StatusCode  execute();        //per event
  //virtual StatusCode  endInputFile();   //end of each input file
  //virtual StatusCode  metaDataStop();   //when outputMetaStore is populated by MetaDataTools
  virtual StatusCode  finalize();       //once, after all events processed
  

  ///Other useful methods provided by base class are:
  ///evtStore()        : ServiceHandle to main event data storegate
  ///inputMetaStore()  : ServiceHandle to input metadata storegate
  ///outputMetaStore() : ServiceHandle to output metadata storegate
  ///histSvc()         : ServiceHandle to output ROOT service (writing TObjects)
  ///currentFile()     : TFile* to the currently open input file
  ///retrieveMetadata(...): See twiki.cern.ch/twiki/bin/view/AtlasProtected/AthAnalysisBase#ReadingMetaDataInCpp


 private: 

  // Algorithm properties 
  std::string c_systName;
  std::string c_sgPrefix;
  std::string c_nominalPrefix;
  std::string c_nominalSuffix;
  ToolHandle<ST::ISUSYObjDef_xAODTool> m_SUSYTools_tighter;
  ToolHandle<IConfigTreeTool> m_configTreeTool; 
  ToolHandle<CutFlowTool> m_cutFlowTool;

  std::vector< std::string > m_eleIsoWPs;
  std::vector< std::string > m_muonIsoWPs;

  // Charge Flip Tool
  ToolHandle<IAsgElectronEfficiencyCorrectionTool> m_elecEfficiencySFTool_chf;
  ToolHandle<IAsgElectronEfficiencyCorrectionTool> m_elecChargeEffCorrTool;

  double get_charge_flip_sf(const xAOD::Electron* electron);
  double get_charge_id_sf(const xAOD::Electron* electron);

  TTree* m_systTree = 0;
  bool c_isMC;
  std::string m_tree_name;

  // config
  bool m_single_lepton_triggers;
  bool m_add_ID_and_MS_muons;
  bool m_RUN_FOR_FAKES;

  // Cutflow
  int m_executeCounterBin;
  int m_badMuonCutBin;
  int m_badJetCutBin;
  int m_cosmicMuonCutBin;
  int m_twoLeptonsCutBin;
  int m_twoDFLeptonsCutBin;
  int m_oppositeChargeLepCutBin;
  int m_sameChargeLepCutBin;

  // Weights
  bool m_is_PDF4LHC_sample;
  bool m_is_ISR_sample;
  bool m_is_scale_sample;
  
  ULong64_t EventNumber;
  /*******
  // For debugging
   Int_t     RunNumber;
   uint32_t  bcid;
   Int_t     LumiBlock; 
   *********/

  // Event values (could be moved to a common tree?)
  /*Char_t hasDiLeptonMatchedTrigger;*/
  int averageInteractionsPerCrossing;
  int dsid;
  Float_t mc_event_weight;
  Float_t m_pileup_weight;
  int mc_channel_number;
  std::vector<Float_t> mc_scale_weights;
  std::vector<Float_t> mc_pdf_weights;
  Float_t mc_isr_mur05muf05_weight;
  Float_t mc_isr_mur20muf20_weight;
  Float_t mc_isr_var3cup_weight;
  Float_t mc_isr_var3cdown_weight;

  // Trigger decisions
  Char_t m_passDiElTrig;
  Char_t m_passDiMuTrig;
  Char_t m_passElMuTrig;
  // single lepton triggers
  Char_t m_passSingleElTrig;
  Char_t m_passSingleMuTrig;

  // Jet values
  std::vector<Float_t>  jetPt;
  std::vector<Float_t>  jetEta;
  std::vector<Float_t>  jetPhi;
  std::vector<Float_t>  jetMass;
  std::vector<Char_t>   jetIsSignal;
//  std::vector<Char_t>   jetIsBtagged;
  std::vector<Char_t>   jetIsBaseline;
  std::vector<Char_t>   jetIsBad;
  std::vector<Char_t>   jetPassOR;


  // Electrons 
  std::vector<Float_t> elePt;
  std::vector<Float_t> eleEta;
  std::vector<Float_t> elePhi;
  std::vector<Float_t> eleE;
  std::vector<Int_t> eleCharge;
  std::vector<Float_t> eleTruthCharge;
  std::vector<Int_t> eleTruthType;
  std::vector<Int_t> eleTruthOrigin;
  std::vector<Char_t> eleIsIsolated;
  std::vector<Char_t> eleIsLonely;
  std::vector<Char_t> elePassCFT;
  std::vector<Char_t> eleIsBaseline;
  std::vector<Char_t> eleIsSingleTriggerMatched;
  std::vector<Char_t> eleIsDiElectronTriggerMatched;
  std::vector<Char_t> eleIsEMuTriggerMatched;
  std::vector<Char_t> eleIsTighterNearlySignal;
  std::vector<Char_t> eleIsInclusiveLooserNearlySignal;
  std::vector<Char_t> eleIsPerfect;
  std::vector<Int_t> eleIFFTruthClassification;
  std::vector<Char_t> elePassOR; // is this needed? 
  std::vector<Float_t> eleSF;
  std::vector<Float_t> eleLooseSF;
  std::vector<Float_t> eleIsoSF;
  std::vector<Float_t> eleChargeIDSF;
  std::vector<Float_t> eleChargeFlipTaggerSF;
  std::vector<Float_t> eleJointISDF_CFTSF; // duplicate (product of the above two)
  std::vector<Float_t> eleTrigSF;
  std::vector<Float_t> eled0sig;
  std::vector<Float_t> elez0sinTheta;
  /*std::vector<Float_t> eletopoetcone20;*/
  /*std::vector<Float_t> eleptvarcone30;*/
 
  std::vector<Char_t> eleIsFCHighPtCaloOnly;
  std::vector<Char_t> eleIsGradient;
  std::vector<Char_t> eleIsFCLoose;
  std::vector<Char_t> eleIsFCTight;
  std::vector<Char_t> eleIsFCLoose_FixedRad;
  std::vector<Char_t> eleIsFCTight_FixedRad;

  // Muons
  std::vector<Float_t> muPt;
  std::vector<Float_t> muEta;
  std::vector<Float_t> muPhi;
  std::vector<Float_t> muIDPt;
  std::vector<Float_t> muMSPt;
  /*std::vector<Float_t> muIDEta;*/
  /*std::vector<Float_t> muMSEta;*/
  /*std::vector<Float_t> muIDPhi;*/
  /*std::vector<Float_t> muMSPhi;*/
  std::vector<Float_t> muE; // is this needed? 
  std::vector<Int_t> muCharge;
  std::vector<Char_t> muIsIsolated;
  std::vector<Char_t> muIsLonely;
  std::vector<Char_t> muIsBad;
  std::vector<Char_t> muIsBaseline;
  std::vector<Char_t> muIsCosmic;
  std::vector<Char_t> muIsPerfect; // is this needed?
  /*std::vector<Char_t> muIsTighterNearlySignal;*/
  /*std::vector<Char_t> muIsInclusiveLooserNearlySignal;*/
  std::vector<Char_t> muIsSignal;
  std::vector<Char_t> muIsSingleTriggerMatched;
  std::vector<Char_t> muIsDiMuonTriggerMatched;
  std::vector<Char_t> muIsEMuTriggerMatched;
  std::vector<Char_t> muPassHighPtCuts;
  std::vector<Char_t> muIsBadHighPt;
  /*std::vector<int> muIsBadHighPt;*/
  std::vector<Char_t> muPassOR;
  std::vector<Float_t> muTruthCharge;
  std::vector<Int_t> muTruthType;
  std::vector<Int_t> muTruthOrigin;
  std::vector<Float_t> muSF;
  std::vector<Float_t> muIsoSF;
  std::vector<Int_t> muIFFTruthClassification;

  std::vector<Float_t> mud0sig;
  std::vector<Float_t> muz0sinTheta;
  /*std::vector<Float_t> mutopoetcone20;*/
  /*std::vector<Float_t> muptvarcone30;*/
  std::vector<Char_t> muIsHighPtTrackOnly;
  std::vector<Char_t> muIsFCTightTrackOnly;
  std::vector<Char_t> muIsFCLoose;
  std::vector<Char_t> muIsFCTight;
  std::vector<Char_t> muIsFCLoose_FixedRad;
  std::vector<Char_t> muIsFCTight_FixedRad;
  std::vector<Char_t> muIsTight_VarRad;

  // Global trigger SF
  float triggerGlobalEfficiencySF;
  float triggerGlobalEfficiency;
  // single lepton triggers
  float triggerElectronEfficiencySF;
  float triggerElectronEfficiency;
  float triggerMuonEfficiencySF;
  float triggerMuonEfficiency;


  // MET
  float EtMiss;
  float EtMissPhi;
  float EtMissEle;
  float EtMissElePhi;
  float EtMissMu;
  float EtMissMuPhi;
  float EtMissJet;
  float EtMissJetPhi;
  float SumEtJet;
  float EtMissSignificance;
  float EtMissPhoton;
  float EtMissPhotonPhi;
  float EtMissTau;
  float EtMissTauPhi;

  // GenFilt
  float GenFiltHT;
  float GenFiltMET;

  // systematics 
  ST::SystInfo m_systInfo; 
  bool m_affectsKinematics;
  bool m_affectsWeights;
  bool m_affectsElectrons;
  bool m_affectsMuons;
  bool m_affectsJets;
  bool m_affectsPhotons;
  bool m_affectsTaus;
//  bool m_affectsBTag;
  bool m_affectsPRW;

  // isolation config
  bool m_hasEleIsoGradient;
  bool m_hasEleIsoFCHighPtCaloOnly;
  bool m_hasEleIsoFCLoose;
  bool m_hasEleIsoFCTight;
  bool m_hasEleIsoFCLoose_FixedRad;
  bool m_hasEleIsoFCTight_FixedRad;
  bool m_hasMuonIsoHighPtTrackOnly;
  bool m_hasMuonIsoFCTightTrackOnly;
  bool m_hasMuonIsoFCLoose;
  bool m_hasMuonIsoFCTight;
  bool m_hasMuonIsoFCLoose_FixedRad;
  bool m_hasMuonIsoFCTight_FixedRad;
  bool m_hasMuonIsoTight_VarRad;

  float get_truth_lepton_charge(const xAOD::IParticle& lepton);
  StatusCode getMETComponent(const xAOD::MissingETContainer* cont, const xAOD::MissingET*& ele, const std::string& name) const;
  StatusCode failEvent(std::string);


}; 

#endif //> !NTUPLEGENERATOR_OBJECTWRITER_H
