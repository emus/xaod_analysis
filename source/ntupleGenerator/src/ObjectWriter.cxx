// ntupleGenerator includes
#include "ObjectWriter.h"

#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODMuon/MuonContainer.h"
//#include "xAODTau/TauJetContainer.h"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODMissingET/MissingETAuxContainer.h"

#include "MCTruthClassifier/MCTruthClassifierDefs.h"


inline bool inVector(std::vector<std::string> &vec, const std::string &val){
  return (std::find(vec.begin(), vec.end(), val) != vec.end());
}

ObjectWriter::ObjectWriter( const std::string& name, ISvcLocator* pSvcLocator ) : AthAnalysisAlgorithm( name, pSvcLocator ){

  declareProperty( "SystematicName", c_systName = "Nominal", "Systematic variation with which to run" );
  declareProperty( "SGPrefix", c_sgPrefix = "Cal", "StoreGate prefix for the calibrated objects" );
  declareProperty("ConfigTreeTool", m_configTreeTool);
  declareProperty("CutFlowTool", m_cutFlowTool);
  declareProperty("SUSYTools_Tighter", m_SUSYTools_tighter);
  declareProperty("ChargeFlipTagEffCorrTool", m_elecEfficiencySFTool_chf);
  declareProperty("ChargeEffCorrTool", m_elecChargeEffCorrTool); 
  declareProperty("ElectronIsoWPs", m_eleIsoWPs);
  declareProperty("single_lepton_triggers", m_single_lepton_triggers);
  declareProperty("add_ID_and_MS_muons", m_add_ID_and_MS_muons);
  declareProperty("MuonIsoWPs", m_muonIsoWPs);

}


ObjectWriter::~ObjectWriter() {}


StatusCode ObjectWriter::failEvent(std::string reason) {
  ATH_MSG_DEBUG("Failed event because " << reason );
  return StatusCode::SUCCESS;
}


StatusCode ObjectWriter::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");
  //
  //This is called once, before the start of the event loop
  //Retrieves of tools you have configured in the joboptions go here
  //

  std::cout << "*\n*\nObjectWriter::initialize(" << c_systName << ")\n*\n*" << std::endl;

  //-----------------------------------------
  // Retrieve tools 
  //-----------------------------------------
  ATH_CHECK( m_SUSYTools_tighter.retrieve() );
  ATH_CHECK( m_configTreeTool.retrieve() );
  ATH_CHECK( m_cutFlowTool.retrieve() );
  ATH_CHECK( m_elecChargeEffCorrTool.retrieve() );

  c_isMC = m_configTreeTool->getIsMC();

  //-----------------------------------------
  // Set bools for isolation WPs 
  //-----------------------------------------
  m_hasEleIsoGradient = inVector( m_eleIsoWPs, "Gradient" );
  m_hasEleIsoFCHighPtCaloOnly = inVector( m_eleIsoWPs, "FCHighPtCaloOnly" );
  m_hasEleIsoFCLoose = inVector( m_eleIsoWPs, "FCLoose" );
  m_hasEleIsoFCTight = inVector( m_eleIsoWPs, "FCTight" );
  m_hasEleIsoFCLoose_FixedRad = inVector( m_eleIsoWPs, "FCLoose_FixedRad" );
  m_hasEleIsoFCTight_FixedRad = inVector( m_eleIsoWPs, "FCTight_FixedRad" );

  m_hasMuonIsoHighPtTrackOnly = inVector( m_muonIsoWPs, "HighPtTrackOnly" );
  m_hasMuonIsoFCTightTrackOnly = inVector( m_muonIsoWPs, "FCTightTrackOnly" );
  m_hasMuonIsoFCLoose = inVector( m_muonIsoWPs, "FCLoose" );
  m_hasMuonIsoFCTight = inVector( m_muonIsoWPs, "FCTight" );
  m_hasMuonIsoFCLoose_FixedRad = inVector( m_muonIsoWPs, "FCLoose_FixedRad" );
  m_hasMuonIsoFCTight_FixedRad = inVector( m_muonIsoWPs, "FCTight_FixedRad" );
  m_hasMuonIsoTight_VarRad = inVector( m_muonIsoWPs, "Tight_VarRad" );


  //-----------------------------------------
  // Deal with systematics  
  //-----------------------------------------

  std::string SUSY_systName = c_systName;
  ST::SystInfo systInfo;
  // Deal with this systematic outside of SUSYTools 
  if(c_systName.substr(0, 24) == "JET_TILECORR_Uncertainty"){
    // TODO: deal with this ... 
    ATH_MSG_ERROR("ERROR: you need to deal with JET_TILECORR_Uncertainty, you don't want to ask SUSYTools about this one");
    return StatusCode::FAILURE;
  }

  // work out what this systematic does
  if(c_systName == "Nominal"){
    m_systInfo.systset = CP::SystematicSet("Nominal");
    m_affectsKinematics = true;
    m_affectsWeights = true;
    m_affectsElectrons = true;
    m_affectsMuons = true;
    m_affectsJets = true;
    m_affectsPhotons = true;
    m_affectsTaus = true;
//    m_affectsBTag = true;
    m_affectsPRW = true;
  }
  else{
    const auto systInfoList = m_SUSYTools_tighter->getSystInfoList();
    auto systItr = std::find_if(systInfoList.begin(), systInfoList.end(), [SUSY_systName] (const ST::SystInfo& systInfo) -> bool { return systInfo.systset.name() == SUSY_systName; } );
    if (systItr == systInfoList.end() ) {
      ATH_MSG_ERROR( "Unable to find systematic: " << SUSY_systName << "!" );
      return StatusCode::FAILURE;
    }
    m_systInfo = *systItr;
    m_affectsKinematics = m_systInfo.affectsKinematics;
    m_affectsWeights = m_systInfo.affectsWeights;
    m_affectsElectrons = ST::testAffectsObject(xAOD::Type::Electron, m_systInfo.affectsType);
    m_affectsMuons = ST::testAffectsObject(xAOD::Type::Muon, m_systInfo.affectsType);
    m_affectsJets = ST::testAffectsObject(xAOD::Type::Jet, m_systInfo.affectsType);
    m_affectsPhotons = ST::testAffectsObject(xAOD::Type::Photon, m_systInfo.affectsType);
    m_affectsTaus = ST::testAffectsObject(xAOD::Type::Tau, m_systInfo.affectsType);
//    m_affectsBTag = ST::testAffectsObject(xAOD::Type::BTag, m_systInfo.affectsType);
    if (c_systName.find("PRW") != std::string::npos ) m_affectsPRW = true;
    else m_affectsPRW = false;
  }


  //-----------------------------------------
  // Create systematics tree
  //-----------------------------------------

  // Naming convention from old code (previously m_tree_name = c_systName)
  if( m_configTreeTool->useOldTreeNamingConvention() ){
    m_tree_name = m_configTreeTool->getTreeBaseName(); // e.g. "ttbar" or "data"
    if(c_systName == "Nominal"){
      m_tree_name = m_tree_name + "_NONE";
    }
    else{
      m_tree_name = m_tree_name + "_" + c_systName;
    }
  }
  else{
    m_tree_name = c_systName;
  }
  ATH_MSG_INFO("Setting tree name: " << m_tree_name);

  // create the TTree 
  ATH_CHECK( book( TTree(m_tree_name.c_str(), m_tree_name.c_str()) ));
  m_systTree = tree(m_tree_name);



  m_RUN_FOR_FAKES = false; // Turn on/off running for fakes!!!!!!!!!!!!!!!!!!!!!!!!
  if(m_RUN_FOR_FAKES) ATH_MSG_WARNING("WILL RUN FOR FAKES"); 

  //-----------------------------------------
  // Initialize cuts 
  //-----------------------------------------
  //if(m_configTreeTool->rejectEventAfterSyst()){
  m_executeCounterBin = m_cutFlowTool->registerSystCut("ExecuteCounter", c_systName);
  m_badMuonCutBin    = m_cutFlowTool->registerSystCut("PassBadMuon", c_systName);
  m_badJetCutBin     = m_cutFlowTool->registerSystCut("PassBadJet", c_systName);
  m_twoLeptonsCutBin = m_cutFlowTool->registerSystCut("ExactlyTwoLeptons", c_systName);
  
  if(!m_RUN_FOR_FAKES){
    m_twoDFLeptonsCutBin = m_cutFlowTool->registerSystCut("TwoDFLeptons", c_systName);
    m_oppositeChargeLepCutBin = m_cutFlowTool->registerSystCut("OppositeChargeLeptons", c_systName);
  }

  //m_sameChargeLepCutBin = m_cutFlowTool->registerSystCut("SameChargeLeptons", c_systName);
  //m_cosmicMuonCutBin = m_cutFlowTool->registerSystCut("PassCosmicMuon", c_systName);
  //}


  //-----------------------------------------
  // Stuff for weights
  //-----------------------------------------
  std::vector<int> PDF4LHC_samples = {407341, 407342, 407343, 407344, 407345, 407346, 410470, 410472, 101232, 142857, 410644, 410645, 410646, 410647, 410658, 410659, 410648, 410649};
  m_is_PDF4LHC_sample = false;
  int this_dsid = m_configTreeTool->getDSID();
  m_is_PDF4LHC_sample = std::find(PDF4LHC_samples.begin(), PDF4LHC_samples.end(), this_dsid) != PDF4LHC_samples.end();

  std::vector<int> ISR_samples = {410472, 410482};
  m_is_ISR_sample = false;
  m_is_ISR_sample = std::find(ISR_samples.begin(), ISR_samples.end(), this_dsid) != ISR_samples.end();

  std::vector<int> scale_samples = {364250, 364253, 364254, 364255, 364288, 364289, 364290, 363356, 363358, 363359, 363360, 363489, 364283, 364284, 364285, 364287};//, 310244, 310245, 310246, 310247, 310248, 310249, 310250, 310251, 310252, 310253, 310254, 310255, 310256, 310257, 310258, 310259, 310260, 310261, 310262, 310263, 310264, 310265, 310266, 310267, 310268, 310269, 310270, 310271, 310272, 310273, 310274, 310275, 310276, 310277, 310278, 310279, 310280, 310281, 310282, 310283, 310284, 310285, 310286, 310287, 310288, 310289, 310290, 310291, 310292, 310293, 310294, 310295, 310296, 310297, 310298, 310299, 310300, 310301, 310302, 310303, 310304, 310305, 311841, 311842, 311843, 311844, 311845, 311846, 311847, 311848, 311849, 311850, 311851, 311852, 311853, 311854, 311855, 311856, 311857, 311858, 311859, 311860, 311861, 311862, 311863, 311864, 311865, 311866};
  m_is_scale_sample = false;
  m_is_scale_sample = std::find(scale_samples.begin(), scale_samples.end(), this_dsid) != scale_samples.end();

  

  //-----------------------------------------
  // Initialize branches
  //-----------------------------------------

  // TODO: implement cases for systematics !!!!!!!!!!!!!!!!!!!!!!!!! 
  
  // Jets
  m_systTree->Branch("jetPt"        , &jetPt        );
  m_systTree->Branch("jetEta"       , &jetEta       );
  m_systTree->Branch("jetPhi"       , &jetPhi       );
  m_systTree->Branch("jetMass"      , &jetMass      );
//  m_systTree->Branch("jetIsBtagged"  , &jetIsBtagged  );
  m_systTree->Branch("jetIsSignal"  , &jetIsSignal  );
  if(!m_configTreeTool->rejectEventAfterSyst()){
    m_systTree->Branch("jetIsBaseline", &jetIsBaseline);
    m_systTree->Branch("jetIsBad", &jetIsBad);
    m_systTree->Branch("jetPassOR"  , &jetPassOR  );
  }

  // Electrons 
  m_systTree->Branch("elePt", &elePt);
  m_systTree->Branch("eleEta", &eleEta);
  m_systTree->Branch("elePhi", &elePhi);
  m_systTree->Branch("eleE", &eleE);
  m_systTree->Branch("eleCharge", &eleCharge);
  m_systTree->Branch("eleIsIsolated", &eleIsIsolated);
  m_systTree->Branch("elePassCFT", &elePassCFT);
  m_systTree->Branch("eleIsTighterNearlySignal", &eleIsTighterNearlySignal);
  m_systTree->Branch("eleIsInclusiveLooserNearlySignal", &eleIsInclusiveLooserNearlySignal);
  m_systTree->Branch("eleIsSingleTriggerMatched", &eleIsSingleTriggerMatched);
  m_systTree->Branch("eleIsDiElectronTriggerMatched", &eleIsDiElectronTriggerMatched);
  m_systTree->Branch("eleIsEMuTriggerMatched", &eleIsEMuTriggerMatched);
  m_systTree->Branch("eled0sig", &eled0sig);
  m_systTree->Branch("elez0sinTheta", &elez0sinTheta);
  //m_systTree->Branch("eletopoetcone20", &eletopoetcone20);
  //m_systTree->Branch("eleptvarcone30", &eleptvarcone30);
  // electron isolation 
  if(m_hasEleIsoGradient) m_systTree->Branch("eleIsGradient", &eleIsGradient);
  if(m_hasEleIsoFCLoose) m_systTree->Branch("eleIsFCLoose", &eleIsFCLoose);
  if(m_hasEleIsoFCTight) m_systTree->Branch("eleIsFCTight", &eleIsFCTight);
  if(m_hasEleIsoFCLoose_FixedRad) m_systTree->Branch("eleIsFCLoose_FixedRad", &eleIsFCLoose_FixedRad);
  if(m_hasEleIsoFCTight_FixedRad) m_systTree->Branch("eleIsFCTight_FixedRad ", &eleIsFCTight_FixedRad );
  if(m_hasEleIsoFCHighPtCaloOnly) m_systTree->Branch("eleIsFCHighPtCaloOnly", &eleIsFCHighPtCaloOnly);

  if(!m_configTreeTool->rejectEventAfterSyst()){
    m_systTree->Branch("eleIsBaseline", &eleIsBaseline);
    m_systTree->Branch("elePassOR", &elePassOR);
  }
  //m_systTree->Branch("eleIsPerfect", &eleIsPerfect);

  if(c_isMC){
    m_systTree->Branch("eleTruthCharge", &eleTruthCharge);
    m_systTree->Branch("eleTruthType", &eleTruthType);
    m_systTree->Branch("eleTruthOrigin", &eleTruthOrigin);
    m_systTree->Branch("eleSF", &eleSF);
    m_systTree->Branch("eleLooseSF", &eleLooseSF);
    m_systTree->Branch("eleIsoSF", &eleIsoSF);
    m_systTree->Branch("eleChargeIDSF", &eleChargeIDSF);
    m_systTree->Branch("eleChargeFlipTaggerSF", &eleChargeFlipTaggerSF);
    m_systTree->Branch("eleJointISDF_CFTSF", &eleJointISDF_CFTSF);
    //m_systTree->Branch("eleTrigSF", &eleTrigSF); // no applied since there's a global trigger SF
    m_systTree->Branch("eleIFFTruthClassification", &eleIFFTruthClassification);
  }

  // Muons
  m_systTree->Branch("muPt", &muPt);
  m_systTree->Branch("muEta", &muEta);
  m_systTree->Branch("muPhi", &muPhi);
  if(m_add_ID_and_MS_muons){
    m_systTree->Branch("muIDPt", &muIDPt);
    m_systTree->Branch("muMSPt", &muMSPt);
    //m_systTree->Branch("muIDEta", &muIDEta);
    //m_systTree->Branch("muMSEta", &muMSEta);
    //m_systTree->Branch("muIDPhi", &muIDPhi);
    //m_systTree->Branch("muMSPhi", &muMSPhi);
  }
  m_systTree->Branch("muE", &muE);
  m_systTree->Branch("muCharge", &muCharge);
  m_systTree->Branch("muIsIsolated", &muIsIsolated);
  m_systTree->Branch("mud0sig", &mud0sig);
  m_systTree->Branch("muz0sinTheta", &muz0sinTheta);
  //m_systTree->Branch("mutopoetcone20", &mutopoetcone20);
  //m_systTree->Branch("muptvarcone30", &muptvarcone30);
  // isolation WPs
  if(m_hasMuonIsoHighPtTrackOnly)         m_systTree->Branch("muIsHighPtTrackOnly", &muIsHighPtTrackOnly);
  if(m_hasMuonIsoFCTightTrackOnly)        m_systTree->Branch("muIsFCTightTrackOnly", &muIsFCTightTrackOnly);
  if(m_hasMuonIsoFCLoose)                 m_systTree->Branch("muIsFCLoose", &muIsFCLoose);
  if(m_hasMuonIsoFCTight)                 m_systTree->Branch("muIsFCTight", &muIsFCTight);
  if(m_hasMuonIsoFCLoose_FixedRad)        m_systTree->Branch("muIsFCLoose_FixedRad", &muIsFCLoose_FixedRad);
  if(m_hasMuonIsoFCTight_FixedRad)        m_systTree->Branch("muIsFCTight_FixedRad", &muIsFCTight_FixedRad);
  if(m_hasMuonIsoTight_VarRad)            m_systTree->Branch("muIsTight_VarRad", &muIsTight_VarRad);
  // 
  if(!m_configTreeTool->rejectEventAfterSyst() ){
    m_systTree->Branch("muIsBad", &muIsBad);
    m_systTree->Branch("muIsBaseline", &muIsBaseline);
    //m_systTree->Branch("muIsCosmic", &muIsCosmic);
    m_systTree->Branch("muPassOR", &muPassOR);
  }
  //m_systTree->Branch("muIsPerfect", &muIsPerfect);
  //m_systTree->Branch("muIsTighterNearlySignal", &muIsTighterNearlySignal);
  //m_systTree->Branch("muIsInclusiveLooserNearlySignal", &muIsInclusiveLooserNearlySignal);
  m_systTree->Branch("muIsSignal", &muIsSignal);
  m_systTree->Branch("muIsSingleTriggerMatched", &muIsSingleTriggerMatched);
  m_systTree->Branch("muIsDiMuonTriggerMatched", &muIsDiMuonTriggerMatched);
  m_systTree->Branch("muIsEMuTriggerMatched", &muIsEMuTriggerMatched);
  m_systTree->Branch("muPassHighPtCuts", &muPassHighPtCuts);
  m_systTree->Branch("muIsBadHighPt", &muIsBadHighPt);

  if(c_isMC){
    m_systTree->Branch("muTruthCharge", &muTruthCharge);
    m_systTree->Branch("muTruthType", &muTruthType);
    m_systTree->Branch("muTruthOrigin", &muTruthOrigin);
    m_systTree->Branch("muSF", &muSF);
    m_systTree->Branch("muIsoSF", &muIsoSF);
    m_systTree->Branch("muIFFTruthClassification", &muIFFTruthClassification);
  }

  // Trigger SF
  if(c_isMC){
    m_systTree->Branch("triggerGlobalEfficiencySF", &triggerGlobalEfficiencySF);
    m_systTree->Branch("triggerGlobalEfficiency", &triggerGlobalEfficiency);
    if(m_single_lepton_triggers){
      m_systTree->Branch("triggerElectronEfficiencySF", &triggerElectronEfficiencySF);
      m_systTree->Branch("triggerElectronEfficiency", &triggerElectronEfficiency);
      m_systTree->Branch("triggerMuonEfficiencySF", &triggerMuonEfficiencySF);
      m_systTree->Branch("triggerMuonEfficiency", &triggerMuonEfficiency);
    }
  }

  // MET
  m_systTree->Branch("EtMiss", &EtMiss);
  m_systTree->Branch("EtMissPhi", &EtMissPhi);
  /***********************
   * WJF: remove extra MET branches
  m_systTree->Branch("EtMissEle", &EtMissEle);
  m_systTree->Branch("EtMissElePhi", &EtMissElePhi);
  m_systTree->Branch("EtMissMu", &EtMissMu);
  m_systTree->Branch("EtMissMuPhi", &EtMissMuPhi);
  m_systTree->Branch("EtMissJet", &EtMissJet);
  m_systTree->Branch("EtMissJetPhi", &EtMissJetPhi);
  m_systTree->Branch("SumEtJet", &SumEtJet);
  ****************************/
  m_systTree->Branch("EtMissSignificance", &EtMissSignificance);
  if(m_configTreeTool->getDoPhotonMet()){
    m_systTree->Branch("EtMissPhoton", &EtMissPhoton);
    m_systTree->Branch("EtMissPhotonPhi", &EtMissPhotonPhi);
  }
  if(m_configTreeTool->getDoTauMet() ){
    m_systTree->Branch("EtMissTau", &EtMissTau);
    m_systTree->Branch("EtMissTauPhi", &EtMissTauPhi);
  }

  // Event values
  m_systTree->Branch("dsid", &dsid);
  m_systTree->Branch("averageInteractionsPerXing", &averageInteractionsPerCrossing);
  if(c_isMC){
    m_systTree->Branch("GenFiltMET", &GenFiltMET);
    m_systTree->Branch("GenFiltHT", &GenFiltHT);
    m_systTree->Branch("mc_event_weight", &mc_event_weight);
    m_systTree->Branch("mc_scale_weights", &mc_scale_weights);
    m_systTree->Branch("mc_pdf_weights", &mc_pdf_weights);
    m_systTree->Branch("mc_isr_mur05muf05_weight", &mc_isr_mur05muf05_weight);
    m_systTree->Branch("mc_isr_mur20muf20_weight", &mc_isr_mur20muf20_weight);
    m_systTree->Branch("mc_isr_var3cup_weight", &mc_isr_var3cup_weight);
    m_systTree->Branch("mc_isr_var3cdown_weight", &mc_isr_var3cdown_weight);
    m_systTree->Branch("mcid", &mc_channel_number); // different from DSID
    m_systTree->Branch("pileup_weight", &m_pileup_weight);
  }
  if(m_configTreeTool->writeTriggerBranches() ){
    if(m_single_lepton_triggers){
      m_systTree->Branch("passSingleElTrig", &m_passSingleElTrig);
      m_systTree->Branch("passSingleMuTrig", &m_passSingleMuTrig);
    }
    m_systTree->Branch("passDielectronTrigger", &m_passDiElTrig);
    m_systTree->Branch("passDiMuonTrigger", &m_passDiMuTrig);
    m_systTree->Branch("passEMuTrigger", &m_passElMuTrig);
  }
  
  m_systTree->Branch("EventNumber", &EventNumber);
  /******************
  // Temp branches for debugging
  m_systTree->Branch("RunNumber", &RunNumber);
  m_systTree->Branch("LumiBlock", &LumiBlock);
  m_systTree->Branch("bcid", &bcid);
  *******************/


  return StatusCode::SUCCESS;
}

StatusCode ObjectWriter::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");
  //
  //Things that happen once at the end of the event loop go here
  //
  

  std::cout << "*\n*\nObjectWriter::finalize(" << c_systName << ")\n*\n*" << std::endl;

  return StatusCode::SUCCESS;
}

StatusCode ObjectWriter::execute() {  
  ATH_MSG_DEBUG ("Executing " << name() << "...");

  // useful for scaling momenta 
  float GeV(0.001);

  const xAOD::EventInfo* eventInfo(0);
  ATH_CHECK(evtStore()->retrieve(eventInfo, "EventInfo" ) );

  //-----------------------------------------
  // Define the accessors 
  //-----------------------------------------
  
  static SG::AuxElement::ConstAccessor<char> acc_passOR("passOR");
  static SG::AuxElement::ConstAccessor<char> acc_signal("signal");
//  static SG::AuxElement::ConstAccessor<char> acc_bjet("bjet");
  static SG::AuxElement::ConstAccessor<char> acc_isol("isol");
  static SG::AuxElement::ConstAccessor<char> acc_pass_cft("passCFT");
  static SG::AuxElement::ConstAccessor<char> acc_trig_matched("triggerMatched");

  // Object scale factors
  static SG::AuxElement::ConstAccessor<float> acc_tighter_SF("tighter_SF");
  static SG::AuxElement::ConstAccessor<float> acc_looser_SF("looser_SF");
  static SG::AuxElement::ConstAccessor<float> acc_iso_SF("iso_SF");
  static SG::AuxElement::ConstAccessor<float> acc_joint_IDSF_CFTSF("joint_IDSF_CFTSF");

  // Isolation WPs 
  static SG::AuxElement::ConstAccessor<char> acc_HighPtTrackOnly("HighPtTrackOnly");
  static SG::AuxElement::ConstAccessor<char> acc_FCTightTrackOnly("FCTightTrackOnly");
  static SG::AuxElement::ConstAccessor<char> acc_FCLoose("FCLoose");
  static SG::AuxElement::ConstAccessor<char> acc_FCTight("FCTight");
  static SG::AuxElement::ConstAccessor<char> acc_FCLoose_FixedRad("FCLoose_FixedRad");
  static SG::AuxElement::ConstAccessor<char> acc_FCTight_FixedRad("FCTight_FixedRad");
  static SG::AuxElement::ConstAccessor<char> acc_Tight_VarRad("Tight_VarRad");
  static SG::AuxElement::ConstAccessor<char> acc_FCHighPtCaloOnly("FCHighPtCaloOnly");
  static SG::AuxElement::ConstAccessor<char> acc_Gradient("myIso_Gradient");

  // muons
  //const static SG::AuxElement::ConstAccessor<char>  acc_passHighPtCuts("passHighPtCuts");
  const static SG::AuxElement::ConstAccessor<char>  acc_passedHighPtCuts("passedHighPtCuts"); // susytools
  const static SG::AuxElement::ConstAccessor<char>  acc_bad_highPt("bad_highPt"); // susytools

  // Triggers decisions
  static SG::AuxElement::ConstAccessor<char> acc_passDiElTrig("passDielectronTrigger");
  static SG::AuxElement::ConstAccessor<char> acc_passDiMuTrig("passDimuonTrigger");
  static SG::AuxElement::ConstAccessor<char> acc_passElMuTrig("passEMuTrigger");
  // single lepton triggers
  static SG::AuxElement::ConstAccessor<char> acc_passSingleElTrig("passSingleelectronTrigger");
  static SG::AuxElement::ConstAccessor<char> acc_passSingleMuTrig("passSinglemuonTrigger");
  
  // trigger matching 
  static SG::AuxElement::ConstAccessor<char> acc_dielectron_trigger_matched("diElectronTrigMatch");
  static SG::AuxElement::ConstAccessor<char> acc_dimuon_trigger_matched("diMuonTrigMatch");
  static SG::AuxElement::ConstAccessor<char> acc_emu_trigger_matched("diEMuTrigMatch");
  // single lepton triggers
  static SG::AuxElement::ConstAccessor<char> acc_singleelectron_trigger_matched("singleElectronTrigMatch");
  static SG::AuxElement::ConstAccessor<char> acc_singlemuon_trigger_matched("singleMuonTrigMatch");

  // trigger SF
  static SG::AuxElement::ConstAccessor<float> acc_triggerGlobalEfficiencySF("triggerGlobalEfficiencySF");
  static SG::AuxElement::ConstAccessor<float> acc_triggerGlobalEfficiency("triggerGlobalEfficiency");
  static SG::AuxElement::ConstAccessor<float> acc_triggerElectronEfficiencySF("triggerElectronEfficiencySF");
  static SG::AuxElement::ConstAccessor<float> acc_triggerElectronEfficiency("triggerElectronEfficiency");
  static SG::AuxElement::ConstAccessor<float> acc_triggerMuonEfficiencySF("triggerMuonEfficiencySF");
  static SG::AuxElement::ConstAccessor<float> acc_triggerMuonEfficiency("triggerMuonEfficiency");

  // isolation 
  static SG::AuxElement::ConstAccessor<float> acc_d0sig("d0sig");
  static SG::AuxElement::ConstAccessor<float> acc_z0sinTheta("z0sinTheta");
  //static SG::AuxElement::ConstAccessor<float> acc_topoetcone20("topoetcone20");
  //static SG::AuxElement::ConstAccessor<float> acc_ptvarcone30("ptvarcone30");


  static SG::AuxElement::ConstAccessor<char> acc_tighterNearlySignal("tighterNearlySignal"); // Leptons satisfying the Tighter selection but without isolation requirements.
  static SG::AuxElement::ConstAccessor<char> acc_inclusiveLooserNearlySignal("inclusiveLooserNearlySignal");  // Leptons satisfying the Looser

  static SG::AuxElement::ConstAccessor<char> acc_bad("bad"); // Is set by IsGoodJet or IsBadJet and IsBadMuon in SUSYTools
  //static SG::AuxElement::ConstAccessor<char> acc_cosmic("cosmic"); // Is set by IsCosmicMuon in SUSYTools
  static SG::AuxElement::ConstAccessor<char> acc_baseline("baseline");

  // for matrix method
  static SG::AuxElement::ConstAccessor<int> acc_truthOrigin("truthOrigin");
  static SG::AuxElement::ConstAccessor<int> acc_truthType("truthType");


  // for GenFilt variables 
  static SG::AuxElement::ConstAccessor<float> cacc_genFiltHT("GenFiltHT");
  static SG::AuxElement::ConstAccessor<float> cacc_genFiltMET("GenFiltMET");

  static SG::AuxElement::ConstAccessor<float> acc_metsignificance("metsignificance");

  // Other event quantities
  static SG::AuxElement::ConstAccessor<double> acc_nominalEventWeight("nominalEventWeight");
  static SG::AuxElement::ConstAccessor<std::vector<float>> acc_ScaleWeights("ScaleWeights");

  // IFF Truth classifier
  static SG::AuxElement::Accessor<int> acc_Class("Classification");


  // function to build the SG names
  auto nameGen = [this] (const std::string& contName) {return c_sgPrefix + contName + "_" + c_systName;};



  // Make sure systematic is applied to SUSYTools
  if (c_systName == "Nominal") {
    if( m_SUSYTools_tighter->resetSystematics() != CP::SystematicCode::Ok ) {
      ATH_MSG_ERROR( "Failed to reset systematics to nominal!" );
      return StatusCode::FAILURE;
    }
  }
  else{
    ATH_MSG_DEBUG( "Apply systematic variation " << c_systName << " ... " );
    if ( m_SUSYTools_tighter->applySystematicVariation(m_systInfo.systset) != CP::SystematicCode::Ok ) {
      ATH_MSG_ERROR( "Cannot configure SUSYTools for systematic variation " << c_systName);
      return StatusCode::FAILURE;
    }
  }
  
  // Make sure systematics are applied to other tools 
  if( m_elecChargeEffCorrTool->applySystematicVariation(m_systInfo.systset) != CP::SystematicCode::Ok){
    ATH_MSG_ERROR("Cannot configure elecChargeEffCorrTool for systematic variation" << c_systName);
    return StatusCode::FAILURE;
  }
  if( m_elecEfficiencySFTool_chf->applySystematicVariation(m_systInfo.systset) != CP::SystematicCode::Ok){
    ATH_MSG_ERROR("Cannot configure elecEfficiencySFTool_chf for systematic variation" << c_systName);
    return StatusCode::FAILURE;
  }



  EventNumber = eventInfo->eventNumber();
  /*********
  // For debugging
  RunNumber = eventInfo->runNumber();
  LumiBlock = eventInfo->lumiBlock();
  bcid = eventInfo->bcid();
  *********/


  //-----------------------------------------
  // Clear the vectors of values from previous event
  //-----------------------------------------

  // Jet values
  jetPt.clear();
  jetEta.clear();
  jetPhi.clear();
  jetMass.clear();
  jetIsSignal.clear();
//  jetIsBtagged.clear();
  // May want to only store signal jets? Could then have a bool that the event has a bad jet? 
  jetIsBaseline.clear();
  jetIsBad.clear();
  jetPassOR.clear();

  // Electron values
  elePt.clear();
  eleEta.clear();
  elePhi.clear();
  eleE.clear();
  eleCharge.clear();
  eleIsIsolated.clear();
  elePassCFT.clear();
  eleIsTighterNearlySignal.clear();
  eleIsInclusiveLooserNearlySignal.clear();
  eleIsSingleTriggerMatched.clear();
  eleIsDiElectronTriggerMatched.clear();
  eleIsEMuTriggerMatched.clear();
  eleIsPerfect.clear();
  elePassOR.clear();
  eleIsBaseline.clear();
  // isolation WPs
  eleIsFCHighPtCaloOnly.clear();
  eleIsGradient.clear();
  eleIsFCLoose.clear();
  eleIsFCTight.clear();
  eleIsFCLoose_FixedRad.clear();
  eleIsFCTight_FixedRad .clear();
  // other isolation
  eled0sig.clear();
  elez0sinTheta.clear();
  //eletopoetcone20.clear();
  //eleptvarcone30.clear();

  if(c_isMC){
    eleTruthCharge.clear();
    eleTruthType.clear();
    eleTruthOrigin.clear();
    eleSF.clear();
    eleLooseSF.clear();
    eleIsoSF.clear();
    eleChargeIDSF.clear();
    eleChargeFlipTaggerSF.clear();
    eleJointISDF_CFTSF.clear();
    eleTrigSF.clear();
    eleIFFTruthClassification.clear();
  }

  // Muon values 
  muPt.clear();
  muEta.clear();
  muPhi.clear();
  if(m_add_ID_and_MS_muons){
    muIDPt.clear();
    muMSPt.clear();
    //muIDEta.clear();
    //muMSEta.clear();
    //muIDPhi.clear();
    //muMSPhi.clear();
  }
  muE.clear();
  muCharge.clear();
  muIsIsolated.clear();
  // isolation WPs
  muIsHighPtTrackOnly.clear();
  muIsFCTightTrackOnly.clear();
  muIsFCLoose.clear();
  muIsFCTight.clear();
  muIsFCLoose_FixedRad.clear();
  muIsFCTight_FixedRad.clear();
  muIsTight_VarRad.clear();
  muIsBad.clear();
  muIsBaseline.clear();
  //muIsCosmic.clear();
  muIsPerfect.clear();
  //muIsTighterNearlySignal.clear();
  //muIsInclusiveLooserNearlySignal.clear();
  muIsSignal.clear();
  muIsSingleTriggerMatched.clear(); // old
  muIsDiMuonTriggerMatched.clear(); // new
  muIsEMuTriggerMatched.clear(); // new
  muPassHighPtCuts.clear();
  muIsBadHighPt.clear();
  muPassOR.clear();
  mud0sig.clear();
  muz0sinTheta.clear();
  //mutopoetcone20.clear();
  //muptvarcone30.clear();
  if(c_isMC){
    muTruthCharge.clear();
    muTruthType.clear();
    muTruthOrigin.clear();
    muSF.clear();
    muIsoSF.clear();
    muIFFTruthClassification.clear();
  }

  // Trigger SF
  if(c_isMC){
    triggerGlobalEfficiencySF = -1.0;
    triggerGlobalEfficiency = -1.0;
    triggerElectronEfficiencySF = -1.0;
    triggerElectronEfficiency = -1.0;
    triggerMuonEfficiencySF = -1.0;
    triggerMuonEfficiency = -1.0;
  }

  // MET (initialize to -10, will be out of -pi -- pi range for phi) 
  EtMiss = -10;
  EtMissPhi = -10;
  EtMissEle = -10;
  EtMissElePhi = -10;
  EtMissMu = -10;
  EtMissMuPhi = -10;
  EtMissJet = -10;
  EtMissJetPhi = -10;
  SumEtJet = -10;
  EtMissPhoton = -10;
  EtMissPhotonPhi = -10;
  EtMissTau = -10;
  EtMissTauPhi = -10;
  EtMissSignificance = -1.0;

  // GenFilt
  GenFiltHT = 0;
  GenFiltMET = 0;

  // event weight
  mc_event_weight = 1.0;
  m_pileup_weight = 1.0;
  mc_channel_number = -1;
  mc_scale_weights.clear();
  mc_pdf_weights.clear();
//  mc_pdf_weights = vect;
  mc_isr_mur05muf05_weight = -1.0;
  mc_isr_mur20muf20_weight = -1.0;
  mc_isr_var3cup_weight = -1.0;
  mc_isr_var3cdown_weight = -1.0;

  
  //-----------------------------------------
  // Store any event-level variables 
  // (some of these could be moved to the common tree,
  // but haven't been because of the possiblity of adding
  // cuts in this algorithm)
  //-----------------------------------------
  ATH_MSG_DEBUG("About to store event-level information"); 
  if(c_isMC){
    dsid = eventInfo->mcChannelNumber();
  }
  else{
    dsid = eventInfo->runNumber();
  }
  averageInteractionsPerCrossing = eventInfo->averageInteractionsPerCrossing();  

  // Extract GenFilt variables
  if(c_isMC){
    if(cacc_genFiltHT.isAvailable(*eventInfo)) GenFiltHT = cacc_genFiltHT(*eventInfo) * GeV;
    else ATH_MSG_WARNING("Unavailable GenFiltHT accessor.");
    if(cacc_genFiltMET.isAvailable(*eventInfo)) GenFiltMET = cacc_genFiltMET(*eventInfo) * GeV;
    else ATH_MSG_WARNING("Unavailable GenFiltMET accessor.");

    // WJF 2019-07-17 Bug in event weights for LQ models ... 
    // This literally took my entire morning
    //mc_event_weight   = eventInfo->mcEventWeights().at(0);
    mc_event_weight   = acc_nominalEventWeight(*eventInfo);
    mc_channel_number = eventInfo->mcChannelNumber();
    m_pileup_weight   = m_SUSYTools_tighter->GetPileupWeight(); // note this doesn't modify the eventInfo
  }

  // get ISR weights for ttbar ISR samples
  if (c_isMC && m_is_ISR_sample){
    ATH_MSG_DEBUG("getting ISR weights");
    mc_isr_mur05muf05_weight = eventInfo->mcEventWeights().at(5); 
    mc_isr_mur20muf20_weight = eventInfo->mcEventWeights().at(6); 
    mc_isr_var3cup_weight = eventInfo->mcEventWeights().at(193); 
    mc_isr_var3cdown_weight = eventInfo->mcEventWeights().at(194); 
    ATH_MSG_DEBUG("got ISR weights: "<<mc_isr_mur05muf05_weight<<", "<<mc_isr_mur20muf20_weight<<", "<<mc_isr_var3cup_weight<<", "<<mc_isr_var3cdown_weight);
  }

  // get Scale weights for samples
  if (c_isMC && m_is_scale_sample){
    ATH_MSG_DEBUG("getting scale weights");
    mc_scale_weights = acc_ScaleWeights(*eventInfo); 
    ATH_MSG_DEBUG("got Scale weights");
  }

  // get PDF4LHC error set for the ttbar and single top samples
  if(c_isMC && m_is_PDF4LHC_sample ){
    ATH_MSG_DEBUG("a quest for pdf weights begins");
    for (int p=112; p<142; p++){
      ATH_MSG_DEBUG(p);
      mc_pdf_weights.push_back(eventInfo->mcEventWeights().at(p)); 
      ATH_MSG_DEBUG(mc_pdf_weights[p-112]);
    }
    ATH_MSG_DEBUG("a quest for pdf weights ends");
  }
  else {
    std::vector<Float_t> vect(30, 1.0); // 112-141 inclusive
    mc_pdf_weights = vect;
//    for (int d=112; d<142; d++){
//      ATH_MSG_DEBUG(d);
//      mc_pdf_weights.push_back(1.0);//eventInfo->mcEventWeights().at(p); 
//      ATH_MSG_DEBUG(mc_pdf_weights[d-112]);
//    }
  }

  // branches for trigger variables
  ATH_MSG_DEBUG("About to access pass-trigger decorations");
  if(m_single_lepton_triggers){
    m_passSingleElTrig = acc_passSingleElTrig(*eventInfo); 
    m_passSingleMuTrig = acc_passSingleMuTrig(*eventInfo); 
  }
  m_passDiElTrig = acc_passDiElTrig(*eventInfo); 
  m_passDiMuTrig = acc_passDiMuTrig(*eventInfo); 
  m_passElMuTrig = acc_passElMuTrig(*eventInfo); 



  //-----------------------------------------
  // Add the new object values to the vectors
  //-----------------------------------------

  // Jets 
  ATH_MSG_DEBUG("Storing jet information ...");
  const xAOD::JetContainer* jets(0);
  ATH_CHECK( evtStore()->retrieve(jets, nameGen("Jets") ) );
  bool eventHasBadJet(false);

  for (const xAOD::Jet* jet : *jets) {

    if(acc_passOR(*jet) && acc_bad(*jet)) eventHasBadJet = true;

    if(m_configTreeTool->rejectEventAfterSyst()){
      // If the event is rejected then we don't want any bad muons
      if (!acc_passOR(*jet) ) continue;
      if (!acc_baseline(*jet) ) continue; 
      if (acc_bad(*jet)) continue;
    }
    else{
      jetIsBaseline.push_back(acc_baseline(*jet) );
      jetIsBad.push_back(acc_bad(*jet));
      jetPassOR.push_back( acc_passOR(*jet));
    }

    jetPt.push_back(jet->pt() * GeV);
    jetEta.push_back(jet->eta() );
    jetPhi.push_back(jet->phi() );
    jetMass.push_back(jet->m() * GeV);
    jetIsSignal.push_back(acc_signal(*jet) );
//    jetIsBtagged.push_back(acc_bjet(*jet) );

  }


  // Electrons 
  ATH_MSG_DEBUG("Storing electron information ...");
  const xAOD::ElectronContainer* electrons(0);
  ATH_CHECK( evtStore()->retrieve(electrons, nameGen("Electrons")));
  ATH_MSG_DEBUG("There are " << electrons->size() << " electrons");

  for(const xAOD::Electron* electron : *electrons){

    // WJF: TODO: Check we want to skip these ... what about scale factors?  
    // >>>  Must be at the top! otherwise continue statement won't have the desired effect!!!  <<< 
    if(m_configTreeTool->rejectEventAfterSyst() ){
      if(!acc_baseline(*electron)) continue;
      if(!acc_passOR(*electron)) continue;
    }
    else{
      eleIsBaseline.push_back(acc_baseline(*electron));
      elePassOR.push_back(acc_passOR(*electron));
    }

    elePt.push_back(electron->pt() * GeV);
    eleEta.push_back(electron->eta() );
    elePhi.push_back(electron->phi() );
    eleE.push_back(electron->e() * GeV ); 
    eleCharge.push_back( electron->charge() );
    eleIsIsolated.push_back( acc_isol(*electron) );
    elePassCFT.push_back( acc_pass_cft(*electron) );
    eleIsTighterNearlySignal.push_back( acc_tighterNearlySignal(*electron) );
    eleIsInclusiveLooserNearlySignal.push_back( acc_inclusiveLooserNearlySignal(*electron) );
    if(m_single_lepton_triggers) {
      eleIsSingleTriggerMatched.push_back( acc_singleelectron_trigger_matched(*electron) );
    }
    eled0sig.push_back( acc_d0sig(*electron) );    
    elez0sinTheta.push_back( acc_z0sinTheta(*electron) );
    //eletopoetcone20.push_back( acc_topoetcone20(*electron) );
    //eleptvarcone30.push_back( acc_ptvarcone30(*electron) );


    eleIsDiElectronTriggerMatched.push_back( acc_dielectron_trigger_matched(*electron));
    eleIsEMuTriggerMatched.push_back( acc_emu_trigger_matched(*electron));
    //eleIsPerfect.push_back( acc_isol(*electron) && acc_tighterNearlySignal(*electron) );
    
    // electron isolation
    if(m_hasEleIsoGradient) eleIsGradient.push_back( acc_Gradient(*electron) );
    if(m_hasEleIsoFCLoose) eleIsFCLoose.push_back( acc_FCLoose(*electron) );
    if(m_hasEleIsoFCTight) eleIsFCTight.push_back( acc_FCTight(*electron) );
    if(m_hasEleIsoFCLoose_FixedRad) eleIsFCLoose_FixedRad.push_back( acc_FCLoose_FixedRad(*electron) );
    if(m_hasEleIsoFCTight_FixedRad) eleIsFCTight_FixedRad .push_back( acc_FCTight_FixedRad (*electron) );
    if(m_hasEleIsoFCHighPtCaloOnly) eleIsFCHighPtCaloOnly.push_back( acc_FCHighPtCaloOnly(*electron) );


    if(c_isMC){
      
      // Scale factors
      eleSF.push_back( acc_tighter_SF(*electron) );
      eleLooseSF.push_back( acc_looser_SF(*electron) );
      eleIsoSF.push_back( acc_iso_SF(*electron) );
      eleChargeIDSF.push_back( get_charge_id_sf(electron) );  
      eleChargeFlipTaggerSF.push_back( get_charge_flip_sf(electron) );
      eleJointISDF_CFTSF.push_back( acc_joint_IDSF_CFTSF(*electron) ); // SUSYTools product of the above two

      // 
      //eleTrigSF.push_back( m_SUSYTools_tighter->GetSignalElecSF(*electron, false,false,true,false, "diLepton")); // changed to di-lepton 17-aug-2019 
      // Don't need to apply this, since we extract the global trigger scale factor! 

      eleIFFTruthClassification.push_back( acc_Class(*electron) );

      // Truth information
      const xAOD::TruthParticle* truth_electron = xAOD::TruthHelpers::getTruthParticle( *electron );
      if(truth_electron){
        //eleTruthCharge.push_back( get_truth_lepton_charge(*electron) );
        eleTruthCharge.push_back( truth_electron->charge() );
        eleTruthOrigin.push_back( acc_truthOrigin( *electron));
        eleTruthType.push_back( acc_truthType( *electron ));
      }
      else{
        eleTruthCharge.push_back(0.0); 
        eleTruthOrigin.push_back(0);
        eleTruthType.push_back(0);
      }
    }
  }


  // Muons
  ATH_MSG_DEBUG("Storing muon information ...");
  const xAOD::MuonContainer* muons(0);
  ATH_CHECK( evtStore()->retrieve(muons, nameGen("Muons")));

  bool eventHasBadMuon(false);
  //bool eventHasCosmicMuon(false);
  for(const xAOD::Muon* muon : *muons){

    if(acc_baseline(*muon) && acc_bad(*muon)) eventHasBadMuon = true;
    //if(acc_passOR(*muon) && acc_cosmic(*muon)) eventHasCosmicMuon = true; // WFJ: cosmic muon veto no longer needed

    // >>> must be at the top, otherwise continue statements won't have the desired effect!!!! <<< 
    if(m_configTreeTool->rejectEventAfterSyst() ){
      // Only skip these muons if we're going to reject the event if it has a bad muon or cosmic muon.
      // If we removed muons that don't pass baseline requirements or OR then we wouldn't know if there was a bad or cosmic muon in the event 
      // and then we wouldn't be able to reject it later
      if(!acc_passOR(*muon) ) continue;
      if(!acc_baseline(*muon) ) continue;
      //if(acc_bad(*muon)) continue;
      //if(eventHasCosmicMuon) continue; 
      if(eventHasBadMuon) continue;
    }
    else{
      // info to reject events
      muIsBad.push_back( acc_bad(*muon) );
      //muIsCosmic.push_back( acc_cosmic(*muon) ); // WJF: cosmic muon veto no longer needed
      // info to keep track of muons we don't want (rejected anyway) 
      muPassOR.push_back( acc_passOR(*muon) );
      muIsBaseline.push_back( acc_baseline(*muon) );
    }


    muPt.push_back( muon->pt() * GeV );
    muEta.push_back( muon->eta() );
    muPhi.push_back( muon->phi() );
    if(m_add_ID_and_MS_muons){
      muIDPt.push_back( muon->auxdata<float>("InnerDetectorPt") * GeV);
      muMSPt.push_back( muon->auxdata<float>("MuonSpectrometerPt") * GeV);
      //muIDEta.push_back( muon->auxdata<float>("InnerDetectorEta"));
      //muMSEta.push_back( muon->auxdata<float>("MuonSpectrometerEta"));
      //muIDPhi.push_back( muon->auxdata<float>("InnerDetectorPhi"));
      //muMSPhi.push_back( muon->auxdata<float>("MuonSpectrometerPhi"));
    }
    muE.push_back( muon->e() * GeV ); // do we need this?
    muCharge.push_back( muon->charge() );
    muIsIsolated.push_back( acc_isol(*muon) );
    mud0sig.push_back( acc_d0sig(*muon) );    
    muz0sinTheta.push_back( acc_z0sinTheta(*muon) );
    //mutopoetcone20.push_back( acc_topoetcone20(*muon) );
    //muptvarcone30.push_back( acc_ptvarcone30(*muon) );
    // isolation WPs 
    if( m_hasMuonIsoFCTightTrackOnly) muIsFCTightTrackOnly.push_back( acc_FCTightTrackOnly(*muon) );
    if( m_hasMuonIsoFCLoose) muIsFCLoose.push_back( acc_FCLoose(*muon) );
    if( m_hasMuonIsoFCTight) muIsFCTight.push_back( acc_FCTight(*muon) );
    if( m_hasMuonIsoFCLoose_FixedRad) muIsFCLoose_FixedRad.push_back( acc_FCLoose_FixedRad(*muon) );
    if( m_hasMuonIsoFCTight_FixedRad) muIsFCTight_FixedRad.push_back( acc_FCTight_FixedRad(*muon) );
    if( m_hasMuonIsoTight_VarRad) muIsTight_VarRad.push_back( acc_Tight_VarRad(*muon) );
    if( m_hasMuonIsoHighPtTrackOnly) muIsHighPtTrackOnly.push_back( acc_HighPtTrackOnly(*muon) );
    //muIsPerfect.push_back(
    
    
    //muIsTighterNearlySignal.push_back( acc_tighterNearlySignal(*muon) );
    //muIsInclusiveLooserNearlySignal.push_back( acc_inclusiveLooserNearlySignal(*muon) );
    muIsSignal.push_back( acc_signal(*muon) );

    if(m_single_lepton_triggers){
      muIsSingleTriggerMatched.push_back( acc_singlemuon_trigger_matched(*muon) );
    }
    muIsDiMuonTriggerMatched.push_back( acc_dimuon_trigger_matched(*muon) );
    muIsEMuTriggerMatched.push_back( acc_emu_trigger_matched(*muon) );
    muPassHighPtCuts.push_back( acc_passedHighPtCuts(*muon) ); // the high pT "working point" , decorator from SUSYTools, called inside IsSignalMuon 
    muIsBadHighPt.push_back( acc_bad_highPt(*muon) ); // "bad" from the high pT "working point", decorator from SUSYTools 


    if(c_isMC){

      muSF.push_back( acc_tighter_SF(*muon) );
      muIsoSF.push_back( acc_iso_SF(*muon) );

      muIFFTruthClassification.push_back(acc_Class(*muon));

      // Truth information
      const xAOD::TruthParticle* truth_muon = xAOD::TruthHelpers::getTruthParticle( *muon );
      if(truth_muon){
        muTruthCharge.push_back( truth_muon->charge() );
        muTruthOrigin.push_back( acc_truthType(*muon) );
        muTruthType.push_back( acc_truthOrigin(*muon) );
      }
      else{
        muTruthCharge.push_back(0.0);
        muTruthOrigin.push_back(0);
        muTruthType.push_back(0);
      }
    }
  }


  // Global trigger scale factors
  if(c_isMC){
    triggerGlobalEfficiencySF = acc_triggerGlobalEfficiencySF(*eventInfo);
    triggerGlobalEfficiency = acc_triggerGlobalEfficiency(*eventInfo);
    if(m_single_lepton_triggers){
      triggerElectronEfficiencySF = acc_triggerElectronEfficiencySF(*eventInfo);
      triggerElectronEfficiency = acc_triggerElectronEfficiency(*eventInfo);
      triggerMuonEfficiencySF = acc_triggerMuonEfficiencySF(*eventInfo);
      triggerMuonEfficiency = acc_triggerMuonEfficiency(*eventInfo);
    }
  }

  

  


  // MET
  ATH_MSG_DEBUG("Storing MET information ...");
  const xAOD::MissingETContainer* met(0);
  ATH_CHECK( evtStore()->retrieve(met, nameGen("METRefFinal") ) );

  const xAOD::MissingET* component(0);

  ATH_CHECK( getMETComponent(met, component, "Final") );
  EtMiss = component->met() * GeV;
  EtMissPhi = component->phi();

  ATH_CHECK( getMETComponent(met, component, "RefEle") );
  EtMissEle = component->met() * GeV;
  EtMissElePhi = component->phi();

  ATH_CHECK( getMETComponent(met, component, "Muons") );
  EtMissMu = component->met() * GeV;
  EtMissMuPhi = component->phi();

  ATH_CHECK( getMETComponent(met, component, "RefJet") );
  EtMissJet = component->met() * GeV;
  EtMissJetPhi = component->phi();
  SumEtJet = component->sumet() * GeV;

  EtMissSignificance = acc_metsignificance(*eventInfo);


  if ( m_configTreeTool->getDoPhotonMet() ) {
    ATH_CHECK( getMETComponent(met, component, "RefGamma") );
    EtMissPhoton = component->met() * GeV;
    EtMissPhotonPhi = component->phi();
  }
  if ( m_configTreeTool->getDoTauMet() ) {
    ATH_CHECK( getMETComponent(met, component, "RefTau") );
    EtMissTau = component->met() * GeV;
    EtMissTauPhi = component->phi();
  }



  //-----------------------------------------
  // Fill the tree
  // Note some cuts are applied here! 
  //-----------------------------------------
  
  // reject events with bad muon/bad jet or cosmic muon, only if the flag is set in JOs 
  // (default to True)
  m_cutFlowTool->incrementSystCut(m_executeCounterBin, c_systName);
  if(m_configTreeTool->rejectEventAfterSyst()){
    
    // bad muon cut
    if(!eventHasBadMuon){
      m_cutFlowTool->incrementSystCut(m_badMuonCutBin, c_systName);
    }
    else return failEvent("Bad muon");


    // bad jet cut
    if(!eventHasBadJet){
      m_cutFlowTool->incrementSystCut(m_badJetCutBin, c_systName);
    }
    else return failEvent("Bad jet");


    // Two leptons "is inclusive loose" leptons (i.e. "loose")
    // "is inclusive loose" taken from the ReadTree definition is = passOR && baseline && isLoose;
    // Leptons in the vectors passOR and baseline already.
    // isLoose varies for electrons and muons
    // for electrons: ele.isLoose = ele.passCFT && eleIsInclusiveLooserNearlySignal->at(i)
    // for muons:     mu.isLoose = muIsInclusiveLooserNearlySignal->at(i)
    
    // Note, if we want to make cuts at the ntuple level, these should be 'looser' cuts. 
    // i.e. requiring two baseline leptons is a harsher cut than requiring two loose leptons, since loose is a subset of baseline. 
    // and there could be a case where we have 2 loose leptons and 1 baseline-not-loose lepton. 
   
    // find the number of loose muons
    std::vector<int> looseMuonIndex;
    int nMuons=muPt.size();
    for(int imu=0; imu<nMuons; ++imu){
      //if( muIsInclusiveLooserNearlySignal.at(imu) ) {
      if( muIsSignal.at(imu) ){
        looseMuonIndex.push_back(imu);
      }
    }
    std::vector<int> looseElectronIndex;
    int nElectrons=elePt.size();
    for(int iele=0; iele<nElectrons; ++iele){
      if( eleIsInclusiveLooserNearlySignal.at(iele) && elePassCFT.at(iele) ){
        looseElectronIndex.push_back(iele);
      }
    }

    // Exactly two loose leptons 
    if(looseElectronIndex.size() + looseMuonIndex.size() == 2){
      //if(true){
      m_cutFlowTool->incrementSystCut(m_twoLeptonsCutBin, c_systName);
    }
    else return failEvent("Two Leptons");


    if(!m_RUN_FOR_FAKES){
      // These cuts are only enabled if you *aren't* running for fakes

      // One electron and one muon 
      if(looseMuonIndex.size() == 1 && looseElectronIndex.size() == 1){
        m_cutFlowTool->incrementSystCut(m_twoDFLeptonsCutBin, c_systName);
      }
      else return failEvent("DF Leptons");

      // two oppositely charged leptons
      if( muCharge.at(looseMuonIndex.at(0)) != eleCharge.at(looseElectronIndex.at(0)) ){
        m_cutFlowTool->incrementSystCut(m_oppositeChargeLepCutBin, c_systName);
      }
      else return failEvent("OppositeChargeLeptons");

    }

    
    /********
    // two same-charge leptons
    if( muCharge.at(looseMuonIndex.at(0)) == eleCharge.at(looseElectronIndex.at(0)) ){
      m_cutFlowTool->incrementSystCut(m_sameChargeLepCutBin, c_systName);
    }
    else return failEvent("SameChargeLeptons");
    *********/

  }
  else{
    // all pass 
    m_cutFlowTool->incrementSystCut(m_badMuonCutBin, c_systName);
    m_cutFlowTool->incrementSystCut(m_badJetCutBin, c_systName);
    //m_cutFlowTool->incrementSystCut(m_cosmicMuonCutBin, c_systName);
    m_cutFlowTool->incrementSystCut(m_twoLeptonsCutBin, c_systName);
    if(!m_RUN_FOR_FAKES){
      m_cutFlowTool->incrementSystCut(m_twoDFLeptonsCutBin, c_systName);
      m_cutFlowTool->incrementSystCut(m_oppositeChargeLepCutBin, c_systName);
    }
    //m_cutFlowTool->incrementSystCut(m_sameChargeLepCutBin, c_systName);
  }

  tree(m_tree_name)->Fill();



  return StatusCode::SUCCESS;
}

StatusCode ObjectWriter::beginInputFile() { 
  //
  //This method is called at the start of each input file, even if
  //the input file contains no events. Accumulate metadata information here
  //

  //example of retrieval of CutBookkeepers: (remember you will need to include the necessary header files and use statements in requirements file)
  // const xAOD::CutBookkeeperContainer* bks = 0;
  // CHECK( inputMetaStore()->retrieve(bks, "CutBookkeepers") );

  //example of IOVMetaData retrieval (see https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AthAnalysisBase#How_to_access_file_metadata_in_C)
  //float beamEnergy(0); CHECK( retrieveMetadata("/TagInfo","beam_energy",beamEnergy) );
  //std::vector<float> bunchPattern; CHECK( retrieveMetadata("/Digitiation/Parameters","BeamIntensityPattern",bunchPattern) );

  return StatusCode::SUCCESS;
}

float ObjectWriter::get_truth_lepton_charge(const xAOD::IParticle& lepton) {
  float charge = 0.;

  const xAOD::TruthParticle* truthp = xAOD::TruthHelpers::getTruthParticle( lepton );
  if (truthp) charge = truthp->charge();

  return charge;
}


double ObjectWriter::get_charge_flip_sf(const xAOD::Electron* electron){
  /************************
   * Returns the scale factor from the AsgElectronEfficiencyCorrectionTool  
   * Args:
   *  electron: a pointer to an  xAOD::Electron
   * Returns:
   *  double: the charge-flip scale factor
   *  TODO: move to object getter
   *  ********************************/

  double charge_flip_tagger_sf = 1.;
  CP::CorrectionCode result = m_elecEfficiencySFTool_chf->getEfficiencyScaleFactor(*electron, charge_flip_tagger_sf);
  switch (result) {
    case CP::CorrectionCode::Ok: 
      break;
    case CP::CorrectionCode::Error:
      ATH_MSG_ERROR( "Failed to retrieve signal electron charge-flip SF"); 
      break;
    case CP::CorrectionCode::OutOfValidityRange:
      ATH_MSG_VERBOSE( "OutOfValidityRange found for signal electron charge-flip SF"); 
      break;
    default:
      ATH_MSG_WARNING( "Don't know what to do for signal electron charge-flip SF");
  }
  ATH_MSG_DEBUG("Charge Flip Tagger SF for electron of pt = " << electron->pt()/1000. << " : " << charge_flip_tagger_sf);

  return charge_flip_tagger_sf;

}

double ObjectWriter::get_charge_id_sf(const xAOD::Electron* electron){
  /************************************
   * Returns the scale factor corresponding to charge-ID
   * Args:
   *  electron: a pointer to an xAOD::Electron
   * Returns:
   *  double: the charge-ID scale factor
   *  TODO: move to object getter 
   *  TODO: move to using the SUSYTools implementation (and remove the dependence on the tool alltogether) 
   *************************************/

  double charge_id_sf(1);
  //  double charge_id_sf = m_SUSYTools_tighter->GetSignalElecSF(*electron, false,false,false,false, "", true ); // commented out as there's an annoying error message in SUSYTools
  //  We will want to move to the SUSYTools version as we want to make sure the scale factor is calculated for the same electrons that are our "signal" electrons. 
  //  Will leave the "get_charge_id_sf" code here though, if we ever want to extract different scale factors for different electrons. 
  CP::CorrectionCode result = m_elecChargeEffCorrTool->getEfficiencyScaleFactor(*electron, charge_id_sf);

  switch (result) {
    case CP::CorrectionCode::Ok: 
      break;
    case CP::CorrectionCode::Error:
      ATH_MSG_ERROR( "Failed to retrieve signal electron charge efficiency correction SF"); 
      break;
    case CP::CorrectionCode::OutOfValidityRange:
//      ATH_MSG_WARNING( "OutOfValidityRange found for signal electron charge efficiency correction SF");
      ATH_MSG_WARNING( "ChargeEff tool demands some truth information that isn't available for some samples. SF will be set to 1.");
      charge_id_sf = 1.;
      break;
    default:
      ATH_MSG_WARNING( "Don't know what to do for signal electron charge efficiency correction SF");
  }

  ATH_MSG_DEBUG("Charge SF for electron of pt = " << electron->pt()/1000. << " : " << charge_id_sf);

  return charge_id_sf; 

}


StatusCode ObjectWriter::getMETComponent(const xAOD::MissingETContainer* cont, const xAOD::MissingET*& ele, const std::string& name) const
{
  /***********************************
   * Extract a specific component of MET 
   *
   *
   * *******************************/
  ATH_MSG_DEBUG( "Looking for " << name );
  auto itr = cont->find(name);
  if (itr == cont->end() ) {
    ATH_MSG_ERROR( "Element " << name << " not present in container" );
    return StatusCode::FAILURE;
  }
  ele = *itr;
  return StatusCode::SUCCESS;
}


