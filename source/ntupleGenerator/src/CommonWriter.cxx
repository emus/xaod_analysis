// ntupleGenerator includes
#include "CommonWriter.h"

#include "xAODEventInfo/EventInfo.h"




CommonWriter::CommonWriter( const std::string& name, ISvcLocator* pSvcLocator ) : AthAnalysisAlgorithm( name, pSvcLocator ){

  //declareProperty( "Property", m_nProperty = 0, "My Example Integer Property" ); //example property declaration

}


CommonWriter::~CommonWriter() {}


StatusCode CommonWriter::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");
  //
  //This is called once, before the start of the event loop
  //Retrieves of tools you have configured in the joboptions go here
  //

  // create the common tree
  ATH_CHECK( book( TTree("commonValues", "commonValues") ));
  m_commonTree = tree("commonValues");

  // Add branches to the common tree 
  m_commonTree->Branch("EventNumber", &EventNumber);
  m_commonTree->Branch("RunNumber", &RunNumber);
  m_commonTree->Branch("LumiBlock", &LumiBlock);
  m_commonTree->Branch("bcid", &bcid);



  return StatusCode::SUCCESS;
}

StatusCode CommonWriter::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");
  //
  //Things that happen once at the end of the event loop go here
  //


  return StatusCode::SUCCESS;
}

StatusCode CommonWriter::execute() {  
  ATH_MSG_DEBUG ("Executing " << name() << "...");


  const xAOD::EventInfo* eventInfo = 0;
  CHECK( evtStore()->retrieve( eventInfo , "EventInfo" ) );
  ATH_MSG_DEBUG("eventNumber=" << eventInfo->eventNumber() );


  // Assign branch values 
  EventNumber = eventInfo->eventNumber();
  RunNumber = eventInfo->runNumber();
  LumiBlock = eventInfo->lumiBlock();
  bcid = eventInfo->bcid();

  tree("commonValues")->Fill();

  return StatusCode::SUCCESS;
}

StatusCode CommonWriter::beginInputFile() { 
  //
  //This method is called at the start of each input file, even if
  //the input file contains no events. Accumulate metadata information here
  //

  //example of retrieval of CutBookkeepers: (remember you will need to include the necessary header files and use statements in requirements file)
  // const xAOD::CutBookkeeperContainer* bks = 0;
  // CHECK( inputMetaStore()->retrieve(bks, "CutBookkeepers") );

  //example of IOVMetaData retrieval (see https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AthAnalysisBase#How_to_access_file_metadata_in_C)
  //float beamEnergy(0); CHECK( retrieveMetadata("/TagInfo","beam_energy",beamEnergy) );
  //std::vector<float> bunchPattern; CHECK( retrieveMetadata("/Digitiation/Parameters","BeamIntensityPattern",bunchPattern) );



  return StatusCode::SUCCESS;
}


