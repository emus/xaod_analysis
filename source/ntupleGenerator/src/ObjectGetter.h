#ifndef NTUPLEGENERATOR_OBJECTGETTER_H
#define NTUPLEGENERATOR_OBJECTGETTER_H 1

#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"


#include "xAODCore/ShallowCopy.h"

// include needed for SystInfo
#include "SUSYTools/ISUSYObjDef_xAODTool.h"

// isolation tool
/*#include "IsolationSelection/IIsolationSelectionTool.h"*/
#include "IsolationSelection/IsolationSelectionTool.h"

// MuonSelectionTool
//#include "MuonSelectorTools/IMuonSelectionTool.h"
//#include "MuonAnalysisInterfaces/IMuonSelectionTool.h" // already in SUSYTools

#include "ntupleGenerator/IConfigTreeTool.h"
#include "PMGTools/PMGTruthWeightTool.h"

// Electron efficiency tool 
#include "ElectronPhotonSelectorTools/AsgElectronChargeIDSelectorTool.h"
#include "ElectronEfficiencyCorrection/AsgElectronEfficiencyCorrectionTool.h"
#include "EgammaAnalysisInterfaces/IAsgElectronEfficiencyCorrectionTool.h"

// Truth classification
#include "IFFTruthClassifier/IFFTruthClassifier.h"
#include "MCTruthClassifier/MCTruthClassifierDefs.h"

#include "JetCPInterfaces/IJetTileCorrectionTool.h"
namespace CP {
    class IJetTileCorrectionTool;
}


class ObjectGetter: public ::AthAnalysisAlgorithm { 
 public: 
  ObjectGetter( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~ObjectGetter(); 

  ///uncomment and implement methods as required

                                        //IS EXECUTED:
  virtual StatusCode  initialize();     //once, before any input is loaded
  virtual StatusCode  beginInputFile(); //start of each input file, only metadata loaded
  //virtual StatusCode  firstExecute();   //once, after first eventdata is loaded (not per file)
  virtual StatusCode  execute();        //per event
  //virtual StatusCode  endInputFile();   //end of each input file
  //virtual StatusCode  metaDataStop();   //when outputMetaStore is populated by MetaDataTools
  virtual StatusCode  finalize();       //once, after all events processed
  

  ///Other useful methods provided by base class are:
  ///evtStore()        : ServiceHandle to main event data storegate
  ///inputMetaStore()  : ServiceHandle to input metadata storegate
  ///outputMetaStore() : ServiceHandle to output metadata storegate
  ///histSvc()         : ServiceHandle to output ROOT service (writing TObjects)
  ///currentFile()     : TFile* to the currently open input file
  ///retrieveMetadata(...): See twiki.cern.ch/twiki/bin/view/AtlasProtected/AthAnalysisBase#ReadingMetaDataInCpp

  template <typename T, typename TAux>
    StatusCode recordContainer(T* cont, TAux* auxCont, const std::string& nameBase);

  template <typename T>
    StatusCode shallowCopyNominal(T*& cont, xAOD::ShallowAuxContainer*& auxCont, const std::string& nameBase);

 private: 

  // configuration variables
  bool c_single_lepton_triggers;
  std::string c_systName;
  std::string c_sgPrefix;
  std::string c_nominalPrefix; // Just in case these aren't what you'd expect from above (e.g. c_sgPrefix)
  std::string c_nominalSuffix; // Just in case these aren't what you'd expect from above (e.g. "_Nominal")
  bool c_doTileCorrection;

  ToolHandle<ST::ISUSYObjDef_xAODTool> m_SUSYTools_tighter;
  ToolHandle<ST::ISUSYObjDef_xAODTool> m_SUSYTools_looser;
  ToolHandle<IConfigTreeTool> m_configTreeTool; 
  ToolHandle<PMGTools::IPMGTruthWeightTool> m_weightTool;
  ToolHandle<CP::IJetTileCorrectionTool> m_tileTool;

  // isolation tool (already in SUSYTools, repeated here because we want to be careful)
  ToolHandle<CP::IsolationSelectionTool> m_manualIsolationTool; 

  CP::IsolationSelectionTool* isoTool;
  /*ToolHandle<CP::IMuonSelectionTool> m_myMuonSelectionTool;*/

  // IFF Truth classifier
  IFFTruthClassifier *m_TruthClassifier; //!

  // charge flip tagger
  ToolHandle<IAsgElectronLikelihoodTool> m_electronChargeIDSelectorTool;

  bool m_isData;

  std::vector< std::string > m_eleIsoWPs;
  std::vector< std::string > m_muonIsoWPs;

  // systematics 
  ST::SystInfo m_systInfo; 
  bool m_affectsKinematics;
  bool m_affectsWeights;
  bool m_affectsElectrons;
  bool m_affectsMuons;
  bool m_affectsJets;
  bool m_affectsPhotons;
  bool m_affectsTaus;
//  bool m_affectsBTag;
  bool m_affectsPRW;

  // Values from SUSYTools configuration
  double c_elePt;
  double c_eleEta;
  double c_eled0sig;
  double c_elez0;
  double c_muPt;
  double c_muEta;
  double c_mud0sig;
  double c_muz0;

  // isolation config
  bool m_hasEleIsoGradient;
  bool m_hasEleIsoFCHighPtCaloOnly;
  bool m_hasEleIsoFCLoose;
  bool m_hasEleIsoFCTight;
  bool m_hasEleIsoFCLoose_FixedRad;
  bool m_hasEleIsoFCTight_FixedRad;
  bool m_hasMuonIsoHighPtTrackOnly;
  bool m_hasMuonIsoFCTightTrackOnly;
  bool m_hasMuonIsoFCLoose;
  bool m_hasMuonIsoFCTight;
  bool m_hasMuonIsoFCLoose_FixedRad;
  bool m_hasMuonIsoFCTight_FixedRad;
  bool m_hasMuonIsoTight_VarRad;

  // trigger
  std::map<int, std::string > m_muon_triggers;
  std::map<int, std::string > m_electron_triggers;
  std::map<int, std::string > m_emu_triggers;
  // single lepton triggers
  std::map<int, std::string > m_singlemuon_triggers;
  std::map<int, std::string > m_singleelectron_triggers;

}; 

#endif //> !NTUPLEGENERATOR_OBJECTGETTER_H
