#ifndef NTUPLEGENERATOR_CUTFLOWTOOL_H
#define NTUPLEGENERATOR_CUTFLOWTOOL_H 1

#include "AsgTools/AsgMetadataTool.h"
#include "AthenaBaseComps/AthHistogramming.h"

#include "xAODEventInfo/EventInfo.h"


class CutFlowTool : public asg::AsgMetadataTool, public AthHistogramming {
  public: 

    ASG_TOOL_CLASS0( CutFlowTool)
    //ASG_TOOL_CLASS0( CutFlowTool )  // WJF: if you wanted to remove the ICutFlowTool then use this line, and replace the inheritance from ICutFlowTool with IAsgTool 

    CutFlowTool(const std::string& name); 

    // Initialize is required by AsgTool base class
    virtual StatusCode initialize(); 

    // From AthHistogramming 
    virtual StatusCode beginInputFile();
    virtual StatusCode endInputFile();
    virtual StatusCode beginEvent();

    // 
    virtual StatusCode readConfig();

    virtual ~CutFlowTool() {}; 

    void incrementBin(TH1* hist, int binNumber, double incrementBy);
    void incrementCutFlowBin(int binNumber, double incrementBy);
    void incrementCutFlowWeightedBin(int binNumber, double incrementBy);
    void incrementCut(int cutBin);
    int registerCut(std::string cutName);
     
    // functions for cuts applied during systematics 
    int registerSystCut(std::string cutName, std::string systName);
    void incrementSystCut(int cutBin, std::string systName);


  private:

    // Configurables for AthHistogramming
    /// Name of the ROOT output stream (file)
    std::string m_prefix;

    /// Name of the ROOT directory
    std::string m_rootDir;

    /// The prefix for the histogram THx name
    std::string m_histNamePrefix;

    /// The postfix for the histogram THx name
    std::string m_histNamePostfix;

    /// The prefix for the histogram THx title
    std::string m_histTitlePrefix;

    /// The postfix for the histogram THx title
    std::string m_histTitlePostfix;

    // configuration variables
    std::string c_git_revision_sha;

    // Cutflow
    TH1* h_cutflow;
    TH1* h_weighted_cutflow;

    std::map<std::string, TH1*> syst_cutflows;
    std::map<std::string, TH1*> syst_weighted_cutflows;

    TH1* AddHist1D(std::string, std::string); 

    // config 
    bool c_isMC;
    std::vector<std::string> c_systList;

    // event
    const xAOD::EventInfo* m_eventInfo;


};

#endif //> NTUPLEGENERATOR_CUTFLOWTOOL_H
