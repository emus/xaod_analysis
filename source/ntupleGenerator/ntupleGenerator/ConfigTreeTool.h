#ifndef NTUPLEGENERATOR_CONFIGTREETOOL_H
#define NTUPLEGENERATOR_CONFIGTREETOOL_H 1

#include "AsgTools/AsgMetadataTool.h"
#include "AthenaBaseComps/AthHistogramming.h"

#include "ntupleGenerator/IConfigTreeTool.h"

#include "xAODEventInfo/EventInfo.h"

// readConfig
#include "TEnv.h"



class ConfigTreeTool : public asg::AsgMetadataTool, public IConfigTreeTool, public AthHistogramming {
  public: 

    ASG_TOOL_CLASS( ConfigTreeTool, IConfigTreeTool)
    //ASG_TOOL_CLASS0( ConfigTreeTool )  // WJF: if you wanted to remove the IConfigTreeTool then use this line, and replace the inheritance from IConfigTreeTool with IAsgTool 

    ConfigTreeTool(const std::string& name); 

    // Initialize is required by AsgTool base class
    virtual StatusCode initialize() override; 

    // From AthHistogramming 
    virtual StatusCode beginInputFile() override;
    virtual StatusCode endInputFile() override;
    virtual StatusCode beginEvent() override;

    // 
    virtual StatusCode readConfig() override;

    virtual ~ConfigTreeTool() {}; 

    virtual bool getIsData() const override {return c_isData;}
    virtual bool getIsMC() const override {return c_isMC;}
    virtual bool getDoTileCorrection() const override {return c_doTileCorrection;}
    virtual int getDSID() const override {return c_dsid;}

    virtual std::string getSTConfig() const override {return c_susytools_config_file;}
    /*virtual std::string getSTConfigExtra() const override {return c_susytools_extra_config_file;}*/


    // (new) const override;
    virtual std::map<int, std::string> get_dielectron_triggers() const override;
    virtual std::map<int, std::string> get_dimuon_triggers() const override;
    virtual std::map<int, std::string> get_emu_triggers() const override;
    // single lepton triggers
    virtual std::map<int, std::string> get_singleelectron_triggers() const override;
    virtual std::map<int, std::string> get_singlemuon_triggers() const override;


    // from SUSYTools config
    virtual std::string getElectronTriggerSFStringSingle() const override {return c_electronTriggerSFStringSingle;}

    virtual double getConfElePt() const override {return c_elePt;}
    virtual double getConfEleEta() const override {return c_eleEta;}
    virtual double getConfEled0sig() const override {return c_eled0sig;}
    virtual double getConfElez0() const override {return c_elez0;}
    virtual double getConfMuPt() const override {return c_muPt;}
    virtual double getConfMuEta() const override {return c_muEta;}
    virtual double getConfMud0sig() const override {return c_mud0sig;}
    virtual double getConfMuz0() const override {return c_muz0;}

    virtual bool getDoPhotonMet() const override {return c_doPhotonMET;}
    virtual bool getDoTauMet() const override {return c_doTauMET;}

    virtual int getNFiles() const override {return c_nFiles;}
    virtual int getNEvents() const override {return c_nEvents;}

    virtual bool rejectEventAfterSyst() const override {return c_doRejectEventsAfterSyst;}
    virtual bool useOldTreeNamingConvention() const override {return c_useOldTreeNamingConvention;}
    virtual bool writeTriggerBranches() const override {return c_writeTriggerBranches;}


    virtual std::string getTreeBaseName() const override {return m_treeBaseName;}


    /*virtual void setSpecialTriggers(std::map<std::string, std::vector<std::string>> trigs) override { m_specialTriggers = trigs;  }*/
    /*virtual std::map<std::string, std::vector<std::string>> getSpecialTriggers() const override {return m_specialTriggers;}*/

    virtual void setPassedTriggers(std::map<std::string,bool> passedTrigs) override {
      m_passedTriggers = passedTrigs;
    }
    virtual std::map<std::string,bool> getPassedTrigs() const override {return m_passedTriggers;}







  private:
    uint64_t m_eventNumber; // Putting monitoring here
    bool m_firstFile;
    uint64_t m_fileNumber;

    // Configurables for AthHistogramming
    /// Name of the ROOT output stream (file)
    std::string m_prefix;

    /// Name of the ROOT directory
    std::string m_rootDir;

    /// The prefix for the histogram THx name
    std::string m_histNamePrefix;

    /// The postfix for the histogram THx name
    std::string m_histNamePostfix;

    /// The prefix for the histogram THx title
    std::string m_histTitlePrefix;

    /// The postfix for the histogram THx title
    std::string m_histTitlePostfix;

    // configuration variables
    std::string c_git_revision_sha;

    // functions to help read configuration 
    void configFromFile(double& property, const std::string& propname, TEnv& rEnv, double defaultValue);
    void configFromFile(std::string& property, const std::string& propname, TEnv& rEnv, const std::string& defaultValue);

    // Set of triggers for trigger matching
    std::map<std::string, std::vector<std::string>> m_specialTriggers; 
    std::map<std::string,bool> m_passedTriggers;

    /// Configuration variables
    bool c_doRejectEventsAfterSyst;
    int c_isData;
    std::string c_outputName;
    int c_isMC;
    int c_isTruth;
    int c_isAF2;
    int c_isFullSim;
    std::string c_mcCampaign;
    int c_nFiles;
    bool c_useOldTreeNamingConvention;
    bool c_writeTriggerBranches;
    int c_nEvents;
    int c_hasTruth;
    int c_dsid;
    int c_showerType;
    bool c_doTileCorrection;
    std::string c_susytools_config_file;
    /*std::string c_susytools_extra_config_file;*/
    std::string c_amiTag;
    bool c_single_lepton_triggers;

    bool c_doTauMET;
    bool c_doPhotonMET;

    // TTree naming
    std::string mcid_to_sample_category(unsigned int mcid) const;
    std::string m_treeBaseName; 

    // from SUSYTools config file
    double c_elePt;
    double c_eleEta;
    double c_eled0sig;
    double c_elez0;
    double c_muPt;
    double c_muEta;
    double c_mud0sig;
    double c_muz0;

    std::string c_electronTriggerSFStringSingle;

    // trigger
    std::map<int, std::string > m_muon_triggers;
    std::map<int, std::string > m_electron_triggers;
    std::map<int, std::string > m_emu_triggers;
    // single lepton triggers
    std::map<int, std::string > m_singlemuon_triggers;
    std::map<int, std::string > m_singleelectron_triggers;


};

#endif //> NTUPLEGENERATOR_CONFIGTREETOOL_H
