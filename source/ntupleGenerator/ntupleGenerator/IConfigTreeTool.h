#ifndef NTUPLEGENERATOR_ICONFIGTREETOOL_H
#define NTUPLEGENERATOR_ICONFIGTREETOOL_H 1

#include "AsgTools/IAsgTool.h"


class IConfigTreeTool : public virtual asg::IAsgTool {
    ASG_TOOL_INTERFACE( IConfigTreeTool ); // declares the interface to athena
  public:


    virtual StatusCode readConfig() = 0;
    virtual ~IConfigTreeTool(){};

    virtual bool getIsData() const = 0;
    virtual bool getIsMC() const = 0;

    virtual std::string getSTConfig() const = 0;
    virtual bool getDoTileCorrection() const = 0;
    virtual int getDSID() const = 0;


    // (new) functions for Trigger strings
    virtual std::map<int, std::string> get_singleelectron_triggers() const = 0;
    virtual std::map<int, std::string> get_singlemuon_triggers() const = 0;
    virtual std::map<int, std::string> get_dielectron_triggers() const = 0;
    virtual std::map<int, std::string> get_dimuon_triggers() const = 0;
    virtual std::map<int, std::string> get_emu_triggers() const = 0;


    // from SUSYTools config
    virtual std::string getElectronTriggerSFStringSingle() const = 0;

    virtual double getConfElePt() const = 0;
    virtual double getConfEleEta() const = 0;
    virtual double getConfEled0sig() const = 0;
    virtual double getConfElez0() const = 0;
    virtual double getConfMuPt() const = 0;
    virtual double getConfMuEta() const = 0;
    virtual double getConfMud0sig() const = 0;
    virtual double getConfMuz0() const = 0;

    virtual bool getDoPhotonMet() const = 0;
    virtual bool getDoTauMet() const = 0;
    virtual int getNFiles() const = 0;
    virtual int getNEvents() const = 0;

    virtual bool rejectEventAfterSyst() const = 0;
    virtual bool useOldTreeNamingConvention() const = 0;
    virtual bool writeTriggerBranches() const = 0;
    
    virtual std::string getTreeBaseName() const = 0;

    /*virtual void setSpecialTriggers(std::map<std::string, std::vector<std::string>> trigs) = 0;*/
    /*virtual std::map<std::string, std::vector<std::string>> getSpecialTriggers() const = 0;*/

    virtual void setPassedTriggers(std::map<std::string,bool> passedTrigs) = 0;
    virtual std::map<std::string,bool> getPassedTrigs() const =0;

};

#endif // !NTUPLEGENERATOR_ICONFIGTREETOOL_H

