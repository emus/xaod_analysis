#include "ntupleGenerator/CutFlowTool.h"

// readConfig 
#include "PathResolver/PathResolver.h"

int CutFlowTool::registerCut(std::string cutName) {
  int binNum = h_cutflow->Fill(cutName.c_str(), 0.);
  h_weighted_cutflow->Fill(cutName.c_str(), 0.);
  h_cutflow->LabelsDeflate("X");
  h_weighted_cutflow->LabelsDeflate("X");
  return binNum;
}

int CutFlowTool::registerSystCut(std::string cutName, std::string systName){
  int binNum = syst_cutflows.at(systName)->Fill(cutName.c_str(), 0.);
  syst_weighted_cutflows.at(systName)->Fill(cutName.c_str(), 0.);
  syst_cutflows.at(systName)->LabelsDeflate("X");
  syst_weighted_cutflows.at(systName)->LabelsDeflate("X");
  return binNum;
}


void CutFlowTool::incrementCutFlowBin(int binNumber, double incrementBy) {
  // increment bin for cutflow histogram
  h_cutflow->SetBinContent(binNumber, h_cutflow->GetBinContent(binNumber) + incrementBy);
}

void CutFlowTool::incrementCutFlowWeightedBin(int binNumber, double incrementBy) {
  // increment bin for weighted cutflow histogram
  h_weighted_cutflow->SetBinContent(binNumber, h_weighted_cutflow->GetBinContent(binNumber) + incrementBy);
}

void CutFlowTool::incrementBin(TH1* hist, int binNumber, double incrementBy) {
  // increment bin for specified histogram
  hist->SetBinContent(binNumber, hist->GetBinContent(binNumber) + incrementBy);
}

void CutFlowTool::incrementCut(int cutBin) {
  // used to increment a cut bin for the nominal histograms
  incrementBin(h_cutflow, cutBin, 1.);
  incrementBin(h_weighted_cutflow, cutBin, c_isMC ? m_eventInfo->mcEventWeight() : 1.);
}

void CutFlowTool::incrementSystCut(int cutBin, std::string systName){
  // used to increment a cut bin for the systematics histograms (cuts applied during the systematics loop)
  incrementBin(syst_cutflows[systName], cutBin, 1.);
  incrementBin(syst_weighted_cutflows[systName], cutBin, c_isMC ? m_eventInfo->mcEventWeight() : 1.);

}

CutFlowTool::CutFlowTool ( const std::string& name) : asg::AsgMetadataTool( name ), AthHistogramming( name )
{
  declareProperty("isMC", c_isMC);
  declareProperty("ListOfSystematics", c_systList);

  // AthHistogramming properties
  declareProperty("RootStreamName",    m_prefix  = "/", "Name of the output ROOT stream (file) that the THistSvc uses");
  declareProperty("RootDirName",       m_rootDir = "",  "Name of the ROOT directory inside the ROOT file where the histograms will go");
  declareProperty( "HistNamePrefix",   m_histNamePrefix  = "", "The prefix for the histogram THx name" );
  declareProperty( "HistNamePostfix",  m_histNamePostfix = "", "The postfix for the histogram THx name" );
  declareProperty( "HistTitlePrefix",  m_histTitlePrefix  = "", "The prefix for the histogram THx title" );
  declareProperty( "HistTitlePostfix", m_histTitlePostfix = "", "The postfix for the histogram THx title" );
}


StatusCode CutFlowTool::initialize() {
  ATH_MSG_INFO("Initializing " << name() << "...");

  // Make sure AthHistogramming collects our settings for things like the stream name  
  ATH_CHECK( AthHistogramming::configAthHistogramming( histSvc(), m_prefix, m_rootDir, m_histNamePrefix, m_histNamePostfix, m_histTitlePrefix, m_histTitlePostfix) );



  h_cutflow = new TH1D("cutflow_common", "Cutflow", 1, 1, 2);
  h_cutflow->Sumw2();
  CHECK( histSvc()->regHist("/"+m_prefix+"/common_cutflow", h_cutflow) );
  h_cutflow->Fill("Nevts in AOD from CBK", 0.);

  h_weighted_cutflow = new TH1D("weighted_cutflow_common", "Weighted Cutflow", 1, 1, 2);
  h_weighted_cutflow->Sumw2(); 
  CHECK( histSvc()->regHist("/"+m_prefix+"/weighted_cutflow_common", h_weighted_cutflow) );
  h_weighted_cutflow->Fill("Sum of weights from CBK", 0.);

  // Fill map of additional cutflows for extra systematics
  for(std::string syst : c_systList){

    // unweighted
    std::string temp_name = "cutflow_"+syst;
    std::string temp_title = syst+" cutflow";
    syst_cutflows[syst] = AddHist1D( "cutflow_"+syst, "cutflow "+syst); 

    // weighted 
    std::string temp_name_weighted = "weighted_cutflow_"+syst;
    std::string temp_title_weighted = syst+" weighted cutflow";
    syst_weighted_cutflows[syst] = AddHist1D(temp_name_weighted, temp_title_weighted);
  }



  return StatusCode::SUCCESS;
}

TH1* CutFlowTool::AddHist1D(std::string name, std::string title) {
  ATH_MSG_DEBUG("Creating histogram: " << name); 
  TH1D* hist = new TH1D(name.c_str(), title.c_str(), 1, 2, 2);
   histSvc()->regHist("/"+m_prefix+"/"+name, hist) ; // maybe should be inside ATH_CHECK 
  return hist;
}



StatusCode CutFlowTool::beginInputFile() {
  return StatusCode::SUCCESS;
}

StatusCode CutFlowTool::beginEvent() {

  ATH_CHECK( evtStore()->retrieve(m_eventInfo, "EventInfo") );
  return StatusCode::SUCCESS;
}

StatusCode CutFlowTool::endInputFile() {
  return StatusCode::SUCCESS;
}

StatusCode CutFlowTool::readConfig() {
  return StatusCode::SUCCESS;
}
