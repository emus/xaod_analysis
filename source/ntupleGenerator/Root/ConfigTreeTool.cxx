#include "ntupleGenerator/ConfigTreeTool.h"

#include "EventInfo/EventStreamInfo.h"
// readConfig 
#include "PathResolver/PathResolver.h"

ConfigTreeTool::ConfigTreeTool ( const std::string& name)
  : asg::AsgMetadataTool( name ),
    AthHistogramming( name ),
    m_eventNumber(0),
    m_firstFile(false),
    m_fileNumber(0)
{

  // Configuration properties 
  declareProperty("doRejectEventsAfterSyst", c_doRejectEventsAfterSyst);
  declareProperty("git_revision_sha", c_git_revision_sha);
  declareProperty("outputName", c_outputName); 
  declareProperty("isData", c_isData);
  declareProperty("isMC", c_isMC);
  declareProperty("isTruth", c_isTruth);
  declareProperty("isAF2", c_isAF2);
  declareProperty("isFullSim", c_isFullSim);
  declareProperty("mcCampaign", c_mcCampaign);
  declareProperty("hasTruth", c_hasTruth);
  declareProperty("number", c_dsid);
  declareProperty("showerType", c_showerType);
  declareProperty("doTileCorrection", c_doTileCorrection);
  declareProperty("doTauMet", c_doTauMET);
  declareProperty("doPhotonMet", c_doPhotonMET);
  declareProperty("nFiles", c_nFiles);
  declareProperty("nEvents", c_nEvents);
  declareProperty("useOldTreeNamingConvention", c_useOldTreeNamingConvention);
  declareProperty("writeTriggerBranches", c_writeTriggerBranches);
  declareProperty("susytools_config_file", c_susytools_config_file);
  declareProperty("single_lepton_triggers", c_single_lepton_triggers);
  //declareProperty("susytools_extra_config_file", c_susytools_extra_config_file);
  declareProperty("amiTag", c_amiTag);


  // AthHistogramming properties
  declareProperty( "RootStreamName",   m_prefix  = "/",         "Name of the output ROOT stream (file) that the THistSvc uses");
  declareProperty( "RootDirName",      m_rootDir = "",          "Name of the ROOT directory inside the ROOT file where the histograms will go");
  declareProperty( "HistNamePrefix",   m_histNamePrefix  = "",  "The prefix for the histogram THx name" );
  declareProperty( "HistNamePostfix",  m_histNamePostfix = "",  "The postfix for the histogram THx name" );
  declareProperty( "HistTitlePrefix",  m_histTitlePrefix  = "", "The prefix for the histogram THx title" );
  declareProperty( "HistTitlePostfix", m_histTitlePostfix = "", "The postfix for the histogram THx title" );
}


StatusCode ConfigTreeTool::initialize() {
  ATH_MSG_INFO("Initializing " << name() << "...");

  // Make sure AthHistogramming collects our settings for things like the stream name  
  ATH_CHECK( AthHistogramming::configAthHistogramming( histSvc(), m_prefix, m_rootDir, 
                                                      m_histNamePrefix, m_histNamePostfix, 
                                                      m_histTitlePrefix, m_histTitlePostfix) );


  // Create the config tree (could be done in readConfig() )
  ATH_CHECK( book( TTree("ConfigurationTree", "ConfigurationTree") ));
  TTree* configTree = tree("ConfigurationTree");

  const EventStreamInfo* esi = 0;
  ATH_CHECK( inputMetaStore()->retrieve(esi) );


  // Define triggers used in this analysis
  m_muon_triggers[2015] = "HLT_2mu10";
  m_muon_triggers[2016] = "HLT_2mu14";
  m_muon_triggers[2017] = "HLT_2mu14";
  m_muon_triggers[2018] = "HLT_2mu14";

  m_electron_triggers[2015] = "HLT_2e12_lhloose_L12EM10VH";
  m_electron_triggers[2016] = "HLT_2e17_lhvloose_nod0";
  m_electron_triggers[2017] = "HLT_2e24_lhvloose_nod0"; //"2e17_lhvloose_nod0_L12EM15VHI"};
  m_electron_triggers[2018] = "HLT_2e24_lhvloose_nod0";//, "2e17_lhvloose_nod0_L12EM15VHI"}; // OR of these triggers is needed to compensate for the L1 isolation inefficiency 

  m_emu_triggers[2015] = "HLT_e17_lhloose_mu14";
  m_emu_triggers[2016] = "HLT_e17_lhloose_nod0_mu14";
  m_emu_triggers[2017] = "HLT_e17_lhloose_nod0_mu14";
  m_emu_triggers[2018] = "HLT_e17_lhloose_nod0_mu14";

  // single lepton triggers
  if(c_single_lepton_triggers){
    m_singlemuon_triggers[2015] = "HLT_mu20_iloose_L1MU15";
    m_singlemuon_triggers[2016] = "HLT_mu26_ivarmedium"; // need to OR with HLT_mu24_ivarmedium and HLT_mu24_ivarloose too 
    m_singlemuon_triggers[2017] = "HLT_mu26_ivarmedium";
    m_singlemuon_triggers[2018] = "HLT_mu26_ivarmedium";
    m_singleelectron_triggers[2015] = "HLT_e24_lhmedium_L1EM20VH";
    m_singleelectron_triggers[2016] = "HLT_e24_lhtight_nod0_ivarloose"; // need to OR with HLT_e26_lhtight_nod0_ivarloose and HLT_e24_lhmedium_nod0_L1EM20VH too
    m_singleelectron_triggers[2017] = "HLT_e26_lhtight_nod0_ivarloose"; //"2e17_lhvloose_nod0_L12EM15VHI"};
    m_singleelectron_triggers[2018] = "HLT_e26_lhtight_nod0_ivarloose";//, "2e17_lhvloose_nod0_L12EM15VHI"}; // OR of these triggers is needed to compensate for the L1 isolation inefficiency 
    }

  // Extract set name 
  m_treeBaseName = "";
  /************************
  unsigned int mcid = 0;
  if(c_isMC){
    ATH_MSG_DEBUG("Fetching MCID for tree name.");
    bool set = false;
    if(c_isTruth){
      for(auto& type : esi->getEventTypes()){ // should only be one of these
        if(!set){
          mcid = type.mc_channel_number();
          set = true;
          ATH_MSG_INFO("extracted MCID from truth: " << mcid);
        }
        else ATH_MSG_ERROR("More than one event type retrieved!");
      }
    }
    else{
      for(auto r : esi->getRunNumbers()) {  // Should only be one of these.
        std::cout << r << std::endl; // TODO: DOESN"T WORK !! 

        if (!set) {
          mcid = r;
          ATH_MSG_INFO("extracted MCID from non-truth MC: " << mcid);
          set = true;
        } else ATH_MSG_ERROR("More than one run number retrieved!");
      }
    }
    if (!set) ATH_MSG_ERROR("Didn't get any event type / run number!");
    m_treeBaseName = mcid_to_sample_category(mcid);
  }
  else{
    m_treeBaseName = "data"; 
  }
  if(m_treeBaseName != c_dsid){
    ATH_MSG_FATAL("number (" << c_dsid << ")  and mcid (" << mcid << ") not the same!");
    return StatusCode::FAILURE;
  }
  ***********************************/
  if(c_isMC){
    m_treeBaseName = mcid_to_sample_category(c_dsid);
  }
  else{
    m_treeBaseName = "data";
  }
  
  ATH_MSG_INFO("Extracted tree base name: " << m_treeBaseName);

  // Add branches
  configTree->Branch("git_revision_sha", new TObjString( c_git_revision_sha.c_str() )); 
  configTree->Branch("amiTag", new TObjString(c_amiTag.c_str() ));
  configTree->Branch("treeBaseName", new TObjString(m_treeBaseName.c_str() ));
  configTree->Branch("outputName", new TObjString(c_outputName.c_str() ));
  configTree->Branch("isData", &c_isData);
  configTree->Branch("DSID", &c_dsid); //new TObjString( std::to_string(c_dsid).c_str() ) );
  configTree->Branch("subCampaign", new TObjString( c_mcCampaign.c_str() ) ); // "data" or mc16a/d/e 
  configTree->Branch("hasTruth", &c_hasTruth);
  configTree->Branch("isFullSim", &c_isFullSim);
  configTree->Branch("isAF2", &c_isAF2);

  

  // read SUSYTools config files
  ATH_CHECK( readConfig() );


  tree("ConfigurationTree")->Fill(); // must be called last
  return StatusCode::SUCCESS;
}

// Functions to get triggers based on year. 
std::map<int, std::string> ConfigTreeTool::get_dielectron_triggers() const{
  return m_electron_triggers;
}
std::map<int, std::string> ConfigTreeTool::get_dimuon_triggers() const{
  return m_muon_triggers;
}
std::map<int, std::string> ConfigTreeTool::get_emu_triggers() const{
  return m_emu_triggers;
}
// single lepton triggers
std::map<int, std::string> ConfigTreeTool::get_singleelectron_triggers() const{
  return m_singleelectron_triggers;
}
std::map<int, std::string> ConfigTreeTool::get_singlemuon_triggers() const{
  return m_singlemuon_triggers;
}

// define, since override 
StatusCode ConfigTreeTool::beginInputFile() {
  std::cout << "hello world, beginInputFile" << std::endl;
  return StatusCode::SUCCESS;
}

// define, since override 
StatusCode ConfigTreeTool::beginEvent() {
  //std::cout << "hello world, beginEvent" << std::endl;
  return StatusCode::SUCCESS;
}


// define, since override 
StatusCode ConfigTreeTool::endInputFile() {
  std::cout << "hello world, endInputFile" << std::endl;
  return StatusCode::SUCCESS;
}

// TODO: Check this is needed (taken from old version of code ... ) [used only by readConfig()]
void ConfigTreeTool::configFromFile(double& property, const std::string& propname, TEnv& rEnv, double defaultValue) {
  property = rEnv.GetValue(propname.c_str(), defaultValue);
  ATH_MSG_DEBUG( "configFromFile(): Loaded property \"" << propname << "\" with value " << property );
}

// TODO: Check this is needed (taken from old version of code ... ) [used only by readConfig()]
void ConfigTreeTool::configFromFile(std::string& property, const std::string& propname, TEnv& rEnv, const std::string& defaultValue) {
  property = rEnv.GetValue(propname.c_str(), defaultValue.c_str());
  if (property.empty()) {
    ATH_MSG_FATAL("Read empty string property from text file (property name: " << propname << ")");
  }
  // post-processing to get rid of leading/trailing spaces and comments
  std::string tmp_prop = property.substr(0, property.find("#", 0));
  property = TString(tmp_prop).ReplaceAll(" ","").Data();
  ATH_MSG_INFO( "configFromFile(): Loaded property \"" << propname << "\" with value " << property );
}



StatusCode ConfigTreeTool::readConfig() {
  /**************************
   *
   * Read configuraton variables from the SUSYTools config (e.g. signal electron pT)
   *
   * ***********************/

  std::string ST_config_file = PathResolverFindCalibFile(c_susytools_config_file);
  ATH_MSG_INFO( "Configuring from file " << ST_config_file); //  << " and " << ST_extra_config_file );

  TEnv rEnv;
  int success = rEnv.ReadFile(ST_config_file.c_str(), kEnvAll);
  if (success != 0) {
    ATH_MSG_ERROR("Failed to read configuration from file " << ST_config_file );
    return StatusCode::FAILURE;
  }

  configFromFile(c_elePt, "Ele.Et", rEnv, 25000.);
  configFromFile(c_eleEta, "Ele.Eta", rEnv, 2.47);
  configFromFile(c_eled0sig, "Ele.d0sig", rEnv, 5.);
  configFromFile(c_elez0, "Ele.z0", rEnv, 0.5);
  configFromFile(c_muPt, "Muon.Pt", rEnv, 25000.);
  configFromFile(c_muEta, "Muon.Eta", rEnv, 2.7);
  configFromFile(c_mud0sig, "Muon.d0sig", rEnv, 3.);
  configFromFile(c_muz0, "Muon.z0", rEnv, 0.5);
  configFromFile(c_electronTriggerSFStringSingle, "Ele.TriggerSFStringSingle", rEnv, "");

  return StatusCode::SUCCESS;
}

std::string ConfigTreeTool::mcid_to_sample_category(unsigned int mcid) const {

  /************
   * Convert mcid to a named sample
   * Copied from Ben's old code ... would be better to do this elsewhere i.e. in the JOs 
   * TODO: remove 
   * Args: 
   *  mcid: int corresponding to the DSID of an mc sample
   * Returns:
   *  string, corresponding to the named MC sample, e.g. ttbar 
   * ********************/

  std::string category = "unknown";
  if ((mcid >= 364100 && mcid <= 364141) || (mcid >= 364198 && mcid <= 364221 )) {
    category = "zjets";
  } else if ((mcid>=410470 && mcid<=410472) || (mcid >=407340 && mcid <= 407350)) {
    category = "ttbar";
  } else if (((mcid >= 410640)&&(mcid <= 410660))) {
    category = "top";
  } else if ( ((mcid >= 361063)&&(mcid <= 361097)) || ((mcid >= 363350)&&(mcid <= 363360)) || (mcid == 363489) || ((mcid >= 364250)&&(mcid <= 364302)) || (mcid >=345705 && mcid <=345723)) {
    category = "diboson";
  } else if ( ((mcid >= 364240)&&(mcid <= 364249)) ) {
    category = "triboson";
  } else if ((mcid >= 364156 && mcid <= 364197) || (mcid >=364220 && mcid <=364230) ) {
    category = "wjets";
  } else if ((mcid >= 361020)&&(mcid <= 361032)) {
    category = "jetjet";
  } else if ((mcid >= 410154)&&(mcid <= 410278)) {
    category = "ttV";
  } else if (
      (mcid >= 364100 && mcid<= 364113) // Zmumu
      || (mcid >= 364114 && mcid<= 364127 ) // Zee
      || (mcid >= 364128 && mcid<= 364141 ) // Ztautat
      //|| (mcid >= 364198 && mcid<= 364203 ) // Zmm // syst?
      //|| (mcid >= 364204 && mcid<= 364209 ) // Zee // syst?
      //|| (mcid >= 364210 && mcid<= 364215 ) // ztt // syst?
      ){
    category = "Zll";
  } else if ((mcid >= 410080)&&(mcid <= 410090)) {
    category = "ttGamma";
  } else if (mcid >= 999000) {
    category = "signal"; // no longer true?
  } else {
    category = std::to_string(mcid);
  }
  return category;
}

