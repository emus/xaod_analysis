#include <map>

// ROOT include(s):
#include <TFile.h>
#include <TError.h>
#include <TString.h>
// Infrastructure include(s):
#ifdef ROOTCORE
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"
#endif  // ROOTCORE
// EDM include(s):
#include "xAODEgamma/ElectronContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "IFFTruthClassifier/IFFTruthClassifier.h"
#include "xAODEventInfo/EventInfo.h"

// For more advanced testing, we do some muon selection
#include "MuonSelectorTools/MuonSelectionTool.h"

// and some truth particle checks
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/xAODTruthHelpers.h"

#define CHECK(ARG)                                    \
  do {                                                \
    const bool result = ARG;                          \
    if (!result) {                                    \
      ::Error(APP_NAME, "Failed to execute: \"%s\"",  \
              #ARG);                                  \
      return 1;                                       \
    }                                                 \
  } while (false)


int main(int argc, char* argv[]) {
  // The application's name:
  const char* APP_NAME = argv[ 0 ];
  // Check if we received a file name:
  if (argc < 2) {
    Error(APP_NAME, "No file name received!");
    Error(APP_NAME, "  Usage: %s [xAOD file name]", APP_NAME);
    return 1;
  }

  // Initialise the application:
  CHECK(xAOD::Init(APP_NAME));

  // Open the input file:
  const TString fileName = argv[ 1 ];
  Info(APP_NAME, "Opening file: %s", fileName.Data());
  std::unique_ptr< TFile > ifile(TFile::Open(fileName, "READ"));
  CHECK(ifile.get());

  // Create a TEvent object:
  xAOD::TEvent event;
  CHECK(event.readFrom(ifile.get()));
  Info(APP_NAME, "Number of events in the file: %i",
       static_cast<int>(event.getEntries()));

  // Decide how many events to run over:
  Long64_t entries = event.getEntries();
  if (argc > 2) {
    const Long64_t e = atoll(argv[ 2 ]);
    if (e < entries) {
      entries = e;
    }
  }

  IFFTruthClassifier myClassifier("myClassifier");
  CHECK(myClassifier.initialize());

  static SG::AuxElement::Accessor<int> tT("truthType");
  static SG::AuxElement::Accessor<int> tO("truthOrigin");
  static SG::AuxElement::Accessor<int> mum_type("firstEgMotherTruthType");
  static SG::AuxElement::Accessor<int> mum_origin("firstEgMotherTruthOrigin");

  CP::MuonSelectionTool muon_selector("muon_selector");
  CHECK(asg::setProperty(muon_selector, "MuQuality", 1));
  CHECK(asg::setProperty(muon_selector, "MaxEta", 2.4));
  CHECK(muon_selector.initialize());

  std::map<int, int> el_counter;
  std::map<int, int> mu_counter;
  std::map<int, int> unknown_el_origin_counter;
  std::map<int, int> unknown_mu_origin_counter;
  std::map<int, std::map<int, int>> debug_map;
  std::map<int, std::map<int, std::map<int, std::map<int, int>>>> ultra_debug_map;

  // Loop over the events:
  for (Long64_t entry = 0; entry < entries; ++entry) {
    // Tell the object which entry to look at:
    event.getEntry(entry);

    // Info(APP_NAME, "=================  NEXT EVENT  =================");

    // Electrons
    const xAOD::ElectronContainer* electrons = 0;
    CHECK(event.retrieve(electrons, "Electrons"));

    // unsigned int i = 0;

    for (const auto& el : *electrons) {
      // Info(APP_NAME, "========================================");
      // Info(APP_NAME, "Electron #%d", i++);
      // Info(APP_NAME, "Electron %f GeV", el->pt() / 1000);

      // if (tT.isAvailable(*el)) {
      //   Info(APP_NAME, "Electron Type from  Reco returns %d ", tT(*el));
      // }
      // if (tO.isAvailable(*el)) {
      //   Info(APP_NAME, "Electron Origin from  Reco returns %d ", tO(*el));
      // }

      int iff_type = static_cast<int>(myClassifier.classify(*el));
      el_counter[iff_type]++;
      // Info(APP_NAME, "Electron from IFF Classifier = %d", iff_type);
      if (iff_type == 0) {
        unknown_el_origin_counter[tT(*el)]++;
        if (tT(*el) == 7) {
          // debug_map[tT(*el)][tO(*el)]++;
        }
	ultra_debug_map[tT(*el)][tO(*el)][mum_type(*el)][mum_origin(*el)]++;
      }
    }

    // Muons

    const xAOD::EventInfo* event_info = 0;
    CHECK(event.retrieve(event_info, "EventInfo"));

    const xAOD::MuonContainer* muons = 0;
    CHECK(event.retrieve(muons, "Muons"));

    for (const auto& mu : *muons) {
      // Info(APP_NAME, "========================================");
      // Info(APP_NAME, "Muon #%d", i++);
      // Info(APP_NAME, "Muon %f GeV", mu->pt() / 1000);

      // if (tT.isAvailable(*mu)) {
      //   Info(APP_NAME, "Muon Type from  Reco returns %d ", tT(*mu));
      // }
      // if (tO.isAvailable(*mu)) {
      //   Info(APP_NAME, "Muon Origin from  Reco returns %d ", tO(*mu));
      // }

      if (mu->pt() < 10000) continue;
      if (!muon_selector.accept(mu)) continue;

      int iff_type = static_cast<int>(myClassifier.classify(*mu));
      mu_counter[iff_type]++;
      // Info(APP_NAME, "Muon type from IFF Classifier = %d", iff_type);
      if (iff_type == 0 || iff_type == 1) {
        unknown_mu_origin_counter[tO(*mu)]++;
        debug_map[tT(*mu)][tO(*mu)]++;
        const xAOD::TruthParticle* truth_particle = xAOD::TruthHelpers::getTruthParticle(*mu);
        Info(APP_NAME, "EventInfo. MC Channel = %llu, Run Number = %u, Event Number = %llu",
             event_info->mcEventNumber(), event_info->runNumber(), event_info->eventNumber());
        if (!truth_particle) {
          Info(APP_NAME, "Unknown muon with NO truth match");
        }
      }
    }
    // Info(APP_NAME, "===>>>  done processing event #%lld ", entry);
  }


  Info(APP_NAME, "Electron summary");
  for (const auto& i: el_counter) {
    Info(APP_NAME, "%d : %d", i.first, i.second);
  }
  Info(APP_NAME, "Muon summary");
  for (const auto& i: mu_counter) {
    Info(APP_NAME, "%d : %d", i.first, i.second);
  }
  Info(APP_NAME, "Unknown electron origin");
  for (const auto& i: unknown_el_origin_counter) {
    Info(APP_NAME, "%d : %d", i.first, i.second);
  }
  Info(APP_NAME, "Unknown muon origin");
  for (const auto& i: unknown_mu_origin_counter) {
    Info(APP_NAME, "%d : %d", i.first, i.second);
  }
  Info(APP_NAME, "DEBUG MAP");
  for (const auto& i: debug_map) {
    for (const auto& j: i.second) {
      Info(APP_NAME, "%d | %d = %d", i.first, j.first, j.second);
    }
  }
  Info(APP_NAME, "ULTRA DEBUG MAP");
  for (const auto& i: ultra_debug_map) {
    for (const auto& j: i.second) {
      for (const auto& k: j.second) {
        for (const auto& m: k.second) {
          Info(APP_NAME, "%d | %d | %d | %d = %d",
               i.first, j.first, k.first, m.first, m.second);
        }
      }
    }
  }

  return 0;
}
