#include <set>
#include <string>

#include "IFFTruthClassifier/IFFTruthClassifier.h"
#include "MCTruthClassifier/MCTruthClassifierDefs.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/xAODTruthHelpers.h"


IFFTruthClassifier::IFFTruthClassifier(const std::string& type) :
  asg::AsgTool(type),
  truth_type("truthType"),
  truth_origin("truthOrigin"),
  mother_truth_type("firstEgMotherTruthType"),
  mother_truth_origin("firstEgMotherTruthOrigin"),
  mother_pdg_id("firstEgMotherPdgId") {}

StatusCode IFFTruthClassifier::initialize() {
  ATH_MSG_INFO("Initialize IFFTruthClassifier");
  return StatusCode::SUCCESS;
}

StatusCode IFFTruthClassifier::finalize() {
  ATH_MSG_INFO("Finalize IFFTruthClassifier");
  return StatusCode::SUCCESS;
}


IFF::Type IFFTruthClassifier::classify(const xAOD::Electron& electron) const {
  namespace MC = MCTruthPartClassifier;

  if (is_prompt_electron(electron)) {
    if (is_charge_flip(electron)) {
      return IFF::Type::ChargeFlipIsoElectron;
    } else {
      return IFF::Type::IsoElectron;
    }
  }

  int type = truth_type(electron);
  int origin = truth_origin(electron);
  int mum_type = mother_truth_type(electron);
  int mum_origin = mother_truth_origin(electron);

  // The following two cases are prompt electrons
  // 1)
  // Discrepancy between code snippet and slides with regards to including
  // mother type or not but it almost certainly isn't important as origin is
  // usually a subset of type...
  if (type == MC::BkgElectron &&
      (origin == MC::PhotonConv || origin == MC::ElMagProc) &&
      mum_origin == MC::FSRPhot) {
    return IFF::Type::IsoElectron;
  }

  // 2) TODO: This is a photon though?
  if (type == MC::NonIsoPhoton && origin == MC::FSRPhot) {
    return IFF::Type::IsoElectron;
  }

  // Tau decays
  // Non-isolated electron/photon from tau decay
  if ((type == MC::NonIsoElectron || type == MC::NonIsoPhoton) &&
      origin == MC::TauLep) {
    return IFF::Type::TauDecay;
  }

  // tau -> tau gamma, gamma -> e+ e-
  if (type == MC::BkgElectron && origin == MC::PhotonConv &&
      mum_type == MC::NonIsoPhoton && mum_origin == MC::TauLep) {
    return IFF::Type::TauDecay;
  }

  // Prompt Photon Conversions

  // gamma -> e+ e-
  if (type == MC::BkgElectron &&
      (origin == MC::PhotonConv || origin == MC::ElMagProc) &&
      mum_type == MC::IsoPhoton && mum_origin == MC::PromptPhot) {
    return IFF::Type::PromptPhotonConversion;
  }

  // H -> gamma gamma, gamma -> e+ e-
  if (type == MC::BkgElectron && origin == MC::PhotonConv &&
      mum_type == MC::IsoPhoton && mum_origin == MC::Higgs) {
    return IFF::Type::PromptPhotonConversion;
  }

  if (type == MC::BkgElectron && origin == MC::ElMagProc &&
      mum_type == MC::BkgPhoton && mum_origin == MC::UndrPhot) {
    return IFF::Type::PromptPhotonConversion;
  }

  // Is a photon?
  if (type == MC::IsoPhoton && origin == MC::PromptPhot) {
    return IFF::Type::PromptPhotonConversion;
  }

  // type = 16 and origin = 38 (again, this is a photon)
  if (type == MC::BkgPhoton && origin == MC::UndrPhot) {
    return IFF::Type::PromptPhotonConversion;
  }

  // TODO: Need to double check the meaning of chFlipElLep in
  // the example code. Perhaps remove the is_charge_flip?
  if (!is_charge_flip(electron) &&
      type == MC::BkgElectron && origin == MC::PhotonConv &&
      mum_type == MC::BkgPhoton && mum_origin == MC::UndrPhot) {
    return IFF::Type::PromptPhotonConversion;
  }

  // TODO: Message from Otilia: but it's not clear if these electrons are really
  // "fakes" or they should go in the real category (we don't know from where
  // this photon is coming...). I would say more truth studies should be done.
  // Hence the warning message...
  if (type == MC::BkgElectron && origin == MC::PhotonConv &&
      mum_type == MC::Unknown && mum_origin == MC::ZBoson) {
    ATH_MSG_WARNING("Electron identified as from a PromptPhotonConversion, "
                    "but this type of electron needs further study!");
    return IFF::Type::PromptPhotonConversion;
  }

  // Is muon reco as electron
  if ((type == MC::NonIsoElectron || type == MC::NonIsoPhoton) && origin == MC::Mu) {
    return IFF::Type::ElectronFromMuon;
  }

  if (type == MC::BkgElectron && mum_origin == MC::Mu) {
    if ((origin == MC::ElMagProc && mum_type == MC::NonIsoElectron) ||
        (origin == MC::PhotonConv && mum_type == MC::NonIsoPhoton)) {
      return IFF::Type::ElectronFromMuon;
    }
  }

  // Light hadron decays
  if (type == MC::Hadron) {
    return IFF::Type::LightFlavorDecay;
  }

  if (type == MC::BkgElectron) {
    if (origin == MC::DalitzDec || origin == MC::LightMeson || origin == MC::StrangeMeson) {
      return IFF::Type::LightFlavorDecay;
    }
    if (origin == MC::PhotonConv && mum_type == MC::BkgPhoton &&
        (mum_origin == MC::PiZero || mum_origin == MC::LightMeson || mum_origin == MC::StrangeMeson)) {
      return IFF::Type::LightFlavorDecay;
    }
    if (origin == MC::ElMagProc &&
        ((mum_type == MC::BkgElectron && mum_origin == MC::StrangeMeson) ||
         (mum_type == MC::BkgPhoton && (mum_origin == MC::LightMeson || mum_origin == MC::PiZero)))
        ) {
      return IFF::Type::LightFlavorDecay;
    }
  }

  // TODO: I am a bit confused here... in the header file I am working from this
  // comes with a comment "bkg electrons/photons from pi0, light meson, strange
  // meson or strange baryon" However, the background electron case is:
  // BkgElectron from BkgPhoton with origin PiZero, LightMeson or StrangeMeson whereas here
  // we have PiZero, LightMeson and StrangeMeson. Is this deliberate?
  if (type == MC::BkgPhoton && (origin == MC::PiZero || origin == MC::LightMeson || origin == MC::StrangeBaryon)) {
    return IFF::Type::LightFlavorDecay;
  }

  // From B hadron
  if (type == MC::NonIsoElectron && has_b_hadron_origin(origin)) {
    return IFF::Type::BHadronDecay;
  }

  // B -> B e
  if (type == MC::BkgElectron && origin == MC::ElMagProc &&
      (mum_type == MC::NonIsoElectron || mum_type == MC::BkgPhoton) &&
      mum_origin == MC::BottomMeson) {
    return IFF::Type::BHadronDecay;
  }

  if (type == MC::BkgElectron && origin == MC::PhotonConv &&
      mum_type == MC::BkgPhoton &&
      (mum_origin == MC::BottomMeson || mum_origin == MC::BottomBaryon)) {
    return IFF::Type::BHadronDecay;
  }

  // From C hadron
  if (type == MC::NonIsoElectron && has_c_hadron_origin(origin)) {
    return IFF::Type::CHadronDecay;
  }

  if (type == MC::BkgElectron && origin == MC::CCbarMeson) {
    return IFF::Type::CHadronDecay;
  }

  if (type == MC::BkgElectron && origin == MC::PhotonConv &&
      mum_type == MC::BkgPhoton && has_c_hadron_origin(mum_origin)) {
    return IFF::Type::CHadronDecay;
  }


  // TODO: See if we want this or not. Now we check if this is something we are
  // able to classify or not. Note that this might be a bit dangerous because
  // the reasons for not having origin and status codes might be complex. The
  // main idea is to weed out things we don't have a hope of classifying due to
  // missing or unknown information.
  const xAOD::TruthParticle* truth_particle = xAOD::TruthHelpers::getTruthParticle(electron);
  int status = truth_particle ? truth_particle->status() : 0;
  bool stable = (status == 1);

  if (!stable && origin == MC::NonDefined && type == MC::Unknown) {
    return IFF::Type::KnownUnknown;
  }
  if (stable && origin == MC::NonDefined && mum_origin == MC::NonDefined) {
    if ((type == MC::Unknown && mum_type == MC::Unknown) ||
        (type == MC::UnknownElectron && mum_type == MC::UnknownElectron)) {
      return IFF::Type::KnownUnknown;
    }
  }

  // TODO <tom.neep>: This an addition from me. Looking at a top sample we get a
  // decent number of electrons that are true isolation muons from tops.
  // MC::IsoMuon (type = 6), MC::top (origin = 10) (p.s. yes, really, top is
  // lowercase)...
  //
  // Anyway there is a question about what to call this, it is really a muon
  // that has been reconstructed as an electron. Overlap removal may kill this anyway.
  //
  // if (type == MC::IsoMuon && origin == MC::top) { return ... }
  //
  // TODO <tom.neep>: I also see a bunch of electrons with type = 7 origin = 25/26.
  // Type 7 is NonIsoMuon and origins 25 and 26 are Charmed/BottomMeson Again,
  // these might be removed through isolation/overlap etc but how do we treat
  // them here

  // ATH_MSG_WARNING("Electron type unknown");
  return IFF::Type::Unknown;
}


IFF::Type IFFTruthClassifier::classify(const xAOD::Muon& muon) const {
  namespace MC = MCTruthPartClassifier;

  int type = truth_type(muon);
  int origin = truth_origin(muon);

  // Check if the type of muon is IsoMuon(6) and whether the origin
  // of the muon is from a prompt source
  static const std::set<int> prompt_source({
      MC::SingleMuon,  // Single muon (origin = 2) from muon twiki
      MC::top,
      MC::WBoson,
      MC::ZBoson,
      MC::Higgs,
      MC::HiggsMSSM,
      MC::SUSY,
      MC::DiBoson,
    });
  if ((type == MC::IsoMuon) && is_in_set(origin, prompt_source)) {
    return IFF::Type::PromptMuon;
  }

  if (((type == MC::IsoMuon) || (type == MC::NonIsoMuon)) && has_b_hadron_origin(origin)) {
    return IFF::Type::BHadronDecay;
  }

  if (type == MC::NonIsoMuon && has_c_hadron_origin(origin)) {
    return IFF::Type::CHadronDecay;
  }

  // TODO:: There is a comment in the example code about J/psi but there is a
  // separate origin code for that: `MC::JPsi == 28.`
  if ((type == MC::IsoMuon || type == MC::BkgMuon) && origin == MC::CCbarMeson) {
    return IFF::Type::CHadronDecay;
  }

  // TODO: The muon twiki includes Light/Strange Baryons but these are not
  // included in the example code
  static const std::set<int> light_source({
      MC::LightMeson,
      MC::StrangeMeson,
      MC::LightBaryon,
      MC::StrangeBaryon,
      MC::PionDecay,
      MC::KaonDecay,
    });
  if (type == MC::BkgMuon && is_in_set(origin, light_source)) {
    return IFF::Type::LightFlavorDecay;
  }

  if (type == MC::Hadron) {
    return IFF::Type::LightFlavorDecay;
  }

  if (type == MC::NonIsoMuon && origin == MC::TauLep) {
    return IFF::Type::TauDecay;
  }

  // TODO:: See comment above about known unknowns for electrons
  if (type == MC::UnknownMuon && origin == MC::NonDefined) {
    return IFF::Type::KnownUnknown;
  }

  const xAOD::TruthParticle* truth_particle = xAOD::TruthHelpers::getTruthParticle(muon);
  int status = truth_particle ? truth_particle->status() : 0;
  bool stable = (status == 1);
  if (!stable && type == MC::Unknown && origin == MC::NonDefined) {
    // Data
    return IFF::Type::KnownUnknown;
  } else if (!stable && type == -99999 && origin == -99999) {
    // MC - no status=1 truth particle associated with the primary track
    return IFF::Type::KnownUnknown;
  }

  ATH_MSG_WARNING("Muon type unknown: type = " << type << ", origin = " << origin);
  return IFF::Type::Unknown;
}


bool IFFTruthClassifier::is_prompt_electron(const xAOD::Electron& electron) const {
  namespace MC = MCTruthPartClassifier;

  int type = truth_type(electron);
  if (type == MC::IsoElectron) {
    return true;
  }

  int origin = truth_origin(electron);
  bool has_electron_mum = std::abs(mother_pdg_id(electron)) == 11;
  if (type == MC::BkgElectron && origin == MC::PhotonConv && has_electron_mum) {
    return true;
  }

  int mum_type = mother_truth_type(electron);
  int mum_origin = mother_truth_origin(electron);
  static const std::set<int> sm_prompt_source({
      MC::top,
      MC::WBoson,
      MC::ZBoson,
      MC::Higgs,
      MC::DiBoson,
     });

  if (type == MC::BkgElectron && origin == MC::ElMagProc &&
      has_electron_mum && mum_type == MC::IsoElectron &&
      is_in_set(mum_origin, sm_prompt_source)) {
    return true;
  }

  // If we reach here then it is not a prompt electron
  return false;
}


bool IFFTruthClassifier::is_charge_flip(const xAOD::Electron& electron) const {
  return (mother_pdg_id(electron) * electron.charge()) > 0;
}


bool IFFTruthClassifier::has_b_hadron_origin(int origin) const {
  namespace MC = MCTruthPartClassifier;
  static const std::set<int> b_hadrons({
      MC::BottomMeson,
      MC::BBbarMeson,
      MC::BottomBaryon,
    });
  return is_in_set(origin, b_hadrons);
}

bool IFFTruthClassifier::has_c_hadron_origin(int origin) const {
  namespace MC = MCTruthPartClassifier;
  static const std::set<int> c_hadrons({
      MC::CharmedMeson,
      MC::CCbarMeson,
      MC::CharmedBaryon,
    });
  return is_in_set(origin, c_hadrons);
}
