export XAODANADIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

# setupATLAS
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
setupATLAS
lsetup git
lsetup cmake
mkdir source build run 
# Note the following 4 commands are only needed IF "--recurse-submodules" was not passed to the git clone command used to checkout this package
#cd source/IFFTruthClassifier
#git submodule init
#git submodule update
#cd ../../
cd build
export ACMHOME=~/acmDev
alias acmSetup='source ${ACMHOME}/atlasexternals/Build/AtlasACM/acmSetup.sh'
#lsetup "root 6.16.00-x86_64-centos7-gcc8-opt"
#lsetup "root 6.14.04-x86_64-slc6-gcc62-opt" # definitely doesn't compile with this version of ROOT
#acmSetup AthAnalysis,21.2.83,centos7 --sourcedir=../source
#acmSetup AthAnalysis,21.2.85,slc6 --sourcedir=../source
#acmSetup AthAnalysis,21.2.96,slc6 --sourcedir=../source
acmSetup AthAnalysis,21.2.151 --sourcedir=../source
cd .. 
acm compile 

