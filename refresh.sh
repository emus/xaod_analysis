export XAODANADIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

# setupATLAS
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
lsetup cmake
mkdir  build 
# Note the following 4 commands are only needed IF "--recurse-submodules" was not passed to the git clone command used to checkout this package
cd build
export ACMHOME=~/acmDev
alias acmSetup='source ${ACMHOME}/atlasexternals/Build/AtlasACM/acmSetup.sh'
#lsetup "root 6.16.00-x86_64-centos7-gcc8-opt"
#acmSetup AthAnalysis,21.2.83,centos7 --sourcedir=../source
#acmSetup AthAnalysis,21.2.98,slc6 --sourcedir=../source
acmSetup AthAnalysis,21.2.98 --sourcedir=../source
cd .. 
acm compile 
