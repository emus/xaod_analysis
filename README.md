# xaod_analysis

This package is used to create ntuples for the emu analysis. 


## Installation
xaod_analysis uses acm for its compilation and installation (which internally uses CMake). First, the package must be checked out from git:
```
git clone --recurse-submodules ssh://git@gitlab.cern.ch:7999/emus/xaod_analysis.git
```
Note that this package uses the Isolation and Fake Forum's [IFFTruthClassifier](https://gitlab.cern.ch/ATLAS-IFF/IFFTruthClassifier) tool as a submodule, hence the `--recurse-submodules` statement.

Move to the xaod_analysis directory and install:
```
cd xaod_analysis/
setupATLAS
mkdir build run
cd build
acmSetup AthAnalysis,21.2.133 --sourcedir=../source
cd ..
acm compile
```
Note that in the above `AthAnalysis,21.2.133` is used, but you should replace that with whatever version you want to use. 
Depending on the operating system, you may encounter some difficulties, espeically with different ROOT versions. 
The above works with centos7 (one can specify the the operating system with e.g. `acmSetup AthAnalysis,21.2.133,slc6`), and with root version `6.16.00-x86_64-centos7-gcc8-opt`. 

### Daily setup
When opening a new shell, to setup the code do the following:
```
cd xaod_analysis/
export XAODANADIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
setupATLAS
cd build
acmSetup
cd ..
```
There is a handy script, `daily_setup.sh` to facilitate this, so all you need to do is run that. 

### Updating AthAnalysis version
Periodically you will want to update the AthAnalysis version. To do, execute the following in a fresh shell:
```
cd xaod_analysis/
setupATLAS
acmSetup AthAnalysis,<new version number> --sourcedir=../source
acm clean 
acm find_packages
acm compile
```



## Running the code
### Locally
An example of the command used to run locally:
```
athena -c "outputFileName='testFileData'" ntupleGeneratorAlgJobOptions.py  --filesInput=/r02/atlas/emus/dxaod/data/p3704/data16_13TeV.00307732.physics_Main.deriv.DAOD_SUSY2.r9264_p3083_p3704/DAOD_SUSY2.16128248._000011.pool.root.1 --evtMax=100
```
Note that ".root" will be appended to the outputFileName variable. 

### On the grid 
A script is available in the `common` directory of the OSDFChargeFlavourAsymmCode package to submit jobs to the grid automatically, however for testing, the following command can be used:
```
pathena ntupleGenerator/ntupleGeneratorAlgJobOptions.py --inDS="mc16_13TeV.407342.PhPy8EG_A14_ttbarHT1k5_hdamp258p75_nonallhad.deriv.DAOD_SUSY2.e6414_s3126_r10201_p3703" --outDS="user.wfawcett.1901231751.mc16d.407342.PhPy8EG_A14_ttbarHT1k5_hdamp258p75_nonallhad.e6414_s3126_r10201_p3703" --forceStaged --mergeOutput
```
Of course, the `outDS` option must be unique, and have your CERN username. Note that the `forceStaged` is optional (this will "force files from primary DS to be staged to local disk, even if direct-access is possible"). `mergeOutput` will merge the output files (this may be more useful if a _list_ of files was submitted instead of a single dataset. A single dataset is input via the `inDS` command in the example above, a list of files can be passed with `--inDsTxt`. It is not currently planned to use `--inDsTxt` in this analysis. 

Note that scripts also exist to download the ntuples, also located in the  [OSDFChargeFlavourAsymmCode](https://gitlab.cern.ch/emus/OSDFChargeFlavourAsymmCode)  repository.

## Design overview 
### Tree structure 
The initial design for this code envisioned two main sets of TTrees for storing data, a common tree, and a systematic tree (one systematic tree for each variation). The common tree would store variables that are not affected by systematic variations, e.g. MC event weights, event number; and the systematic trees would store variables that are affected by the variations, e.g. Jet pT. The idea was to save space when running on systematics by not copying variables such as event number for each systematic variation. The systematic trees are also linked, so that a new entry is only stored if it is affected by that systematic (for example, running on an electron systematic might not affect the jet pT, and so this branch wouldn't be written twice). 

During development it was found to be superior to add cuts that depends on systematics, this broke the common tree aspect, as it would have a different number of events to the systematic trees, however the commonTree framework is still in place. [this is still in review] 

In addition, a configurationTree also exists, to store the SUSYTools and other configured variables inside the TTree. This allows for all metadata to be conviently carried around in the root file itself.

Lastly, there is a metadata tree, that stores luminosity information (for data only). 


### Algorithms and tools 
#### configTreeTool
This too is used as one place to store the configuration of the whole job. There are several configurable options in the JobOptions, and also some of the file metadata is extracted in the JOs. All of this is passed to the configTreeTool, which itself is passed to the algs (therefore not having to pass all of the configuration to each alg). This tool also creates the ConfigurationTree, which stores most of the configuration data inside the output root file. 

#### CutFlowTool
This tool is used to create cutflow histograms for any cuts executed in the ntuple maker. Some cuts are make by the `CommonWriter` alg (which apply to all events) and some cuts are made by the `ObjectWriter`, which only apply to a specific systematic variation. The tool acts as book keeping across the systematics. The histograms it creates are stored in the output root file for further book-keeping.

#### ntupleGeneratorAlg
The main athena algorithm (alg) is `ntupleGeneratorAlg` which has properties initialises several of the other tools used, and initializes several book-keeping histograms which are passed to the `CutFlowTool`.

Basic cuts are also applied here: Event cleaning (LAr, Tile, SCT errors) batman cleaning, primary vertex. 

#### CommonWriterAlg 
This gets event-level information, and other variables, that do not change across systematic variation (e.g. EventNumber, BCID), and writes them to the CommonTree.
[Note: this alg could be removed]

#### ObjectGetterAlg
This get the physics objects (jets, muons, electrons, MET), and applies the relevant calibrations and decorations. 

#### ObjectWriterAlg
This writes the relevant properties of the physics objects to an ntuple, e.g. JetPt. Cuts are also applied here, for example bad jet and bad muon. Note that only jets, electrons and muons that pass _baseline_ and _overlap removal_ are stored in the resulting ntuple. 

