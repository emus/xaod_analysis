echo "Will run test job with file $1"
echo "Will produce file $2"

COMMAND="athena -c \"doSyst=0;outputFileName='$2'\" ntupleGenerator/ntupleGeneratorAlgJobOptions.py --filesInput=$1"
echo $COMMAND
$COMMAND
