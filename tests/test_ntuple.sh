echo "Will run test job with file $1"

athena -c "doSyst=0;outputFileName='/analysis/tests/test_ntuple.root'" ntupleGenerator/ntupleGeneratorAlgJobOptions.py --evtMax=100 --filesInput=$1

echo "List files in this directory: $PWD"
ls 

echo "List files in home directory"
ls ~/ 

# print out the last command as a check
echo !! 
